﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.Contracts.Validator;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Services
{
    public class SprintService : ISprintService
    {
        private readonly IRepository<Project> _repositoryProject;
        private readonly IRepository<Sprint> _repositorySprint;
        private readonly IRepository<SprintStatus> _repositorySprintStatus;
        private readonly IValidator<Sprint> _validator;

        public SprintService(
            IRepository<Project> repositoryProgect,
            IRepository<Sprint> repositorySprint,
            IRepository<SprintStatus> repositorySprintStatus,
            IValidator<Sprint> validator)
        {
            _repositoryProject = repositoryProgect;
            _repositorySprint = repositorySprint;
            _repositorySprintStatus = repositorySprintStatus;
            _validator = validator;
        }

        public SprintViewModel[] FindByProject(int projectId)
        {
            var sprints = _repositorySprint.Find(sprint => sprint.ProjectId == projectId).ToList();
            if (sprints == null)
            {
                throw new ArgumentException();
            }
            for (int i = 0; i < sprints.Count; i++)
            {
                ChangeStatus(sprints[i]);
            }
            return Mapper.Map<IEnumerable<Sprint>, IEnumerable<SprintViewModel>>(sprints).ToArray();
        }

        public void Edit(SprintViewModel value)
        {
            var entity = _repositorySprint.FindById(value.Id);
            if (entity == null)
            {
                throw new ArgumentException();
            }
            entity = Mapper.Map(value, entity);
            if (!_validator.IsValid(entity))
            {
                throw new ValidationException(_validator.Results[0]);
            }
            _repositorySprint.Update(entity);
            _repositorySprint.SaveChanges();
        }

        public void Add(SprintViewModel value)
        {
            try
            {
                var project = _repositoryProject.Find(p => p.Id == value.ProjectId).First();
            }
            catch (InvalidOperationException)
            {
                throw new ValidationException("Invalid project Id, project does not exist");
            }
            Sprint sprint = new Sprint();
            Mapper.Map(value, sprint);
            sprint.ProjectId = value.ProjectId;
            sprint.SprintStatusId = value.StatusId;
            if (!_validator.IsValid(sprint))
            {
                throw new ValidationException(_validator.Results[0]);
            }
            ChangeStatus(sprint);
            _repositorySprint.Add(sprint);
            _repositorySprint.SaveChanges();
        }

        public void Delete(int entityId)
        {
            var sprints = _repositorySprint.FindById(entityId);
            if (sprints == null || DefineStatus(sprints.StartDate, sprints.EndDate) != "Future")
            {
                throw new ValidationException();
            }
            ReleseTasks(sprints);

            _repositorySprint.Delete(entityId);
            _repositorySprint.SaveChanges();
        }

        public IEnumerable<SprintViewModel> GetAll()
        {
            return _repositorySprint.GetAll().Select(sprint => Mapper.Map<SprintViewModel>(sprint));
        }

        public SprintViewModel FindById(int entityId)
        {
            if (!_validator.IsExist(entityId))
            {
                throw new ArgumentException("Sprint doesn't exist");
            }
            var entity = _repositorySprint.FindById(entityId);
            ChangeStatus(entity);
            return Mapper.Map<Sprint, SprintViewModel>(entity);
        }

        private static string DefineStatus(DateTime startTime, DateTime endTime)
        {
            var result = "Future";
            var start = startTime.CompareTo(DateTime.Now);
            var finish = endTime.CompareTo(DateTime.Now);
            if (finish < 0)
            {
                result = "Completed";
            }
            else if (start < 0 && finish >= 0)
            {
                result = "Current";
            }
            else if (start > 0)
            {
                result = "Future";
            }
            return result;
        }

        private void ChangeStatus(Sprint sprint)
        {
            var status = DefineStatus(sprint.StartDate, sprint.EndDate);
            var statusId = _repositorySprintStatus.Find(s => s.Status == status).First().Id;
            if (sprint.SprintStatusId != statusId)
            {
                sprint.SprintStatusId = statusId;
                _repositorySprint.Update(sprint);
            }
            if (status == "Completed")
            {
                ReleseTasks(sprint);
            }
        }

        private void ReleseTasks(Sprint sprint)
        {
            if (sprint.Tasks != null)
            {
                foreach (var task in sprint.Tasks)
                {
                    task.SprintId = null;
                    task.Sprint = null;
                }
                sprint.Tasks = null;
                _repositorySprint.Update(sprint);
            }
        }
    }
}