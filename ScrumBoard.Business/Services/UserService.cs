﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.Contracts.Validator;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _repositoryUser;
        private readonly IValidator<User> _validatorUser;

        public UserService(IRepository<User> repositoryUser,
            IValidator<User> validatorUser)
        {
            _repositoryUser = repositoryUser;
            _validatorUser = validatorUser;
        }

        public void Edit(UserViewModel value)
        {
            var entity = _repositoryUser.FindById(value.Id);
            if (entity == null)
            {
                throw new ArgumentException();
            }
            entity = Mapper.Map(value, entity);
            if (!_validatorUser.IsValid(entity))
            {
                throw new ValidationException(_validatorUser.Results[0]);
            }
            _repositoryUser.Update(entity);
        }

        public void Add(UserViewModel value)
        {
            var newUser = Mapper.Map<User>(value);
            if (!_validatorUser.IsValid(newUser)) { 
                throw new ValidationException(_validatorUser.Results[0]);
            }
            _repositoryUser.Add(newUser);
            _repositoryUser.SaveChanges();
        }

        public void Delete(int entityId)
        {
            var tmp = _repositoryUser.Find(x => x.AccountId == entityId).FirstOrDefault();
            if (tmp == null)
            {
                throw new ValidationException("User doesn't exist");
            }
            _repositoryUser.Delete(tmp.Id);
            _repositoryUser.SaveChanges();
        }

        public IEnumerable<UserViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(_repositoryUser.GetAll());
        }

        public UserViewModel FindById(int entityId)
        {
            var user = _repositoryUser.Find(x => x.AccountId == entityId).FirstOrDefault();
            if (user == null)
            {
                throw new ArgumentException("User not found");
            }
            return Mapper.Map<User, UserViewModel>(user);
        }

        public UserViewModel FindByEmail(string email)
        {
            var user = _repositoryUser.Find(u => u.Email == email).FirstOrDefault();
            if (user == null)
            {
                throw new InvalidOperationException("User not found");
            }
            return Mapper.Map<User, UserViewModel>(user);
        }
    }
}