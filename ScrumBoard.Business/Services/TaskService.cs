﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.Contracts.Validator;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Services
{
    public class TaskService : ITaskService
    {
        private readonly IRepository<Task> _taskRepository;
        private readonly IRepository<TaskStatus> _taskStatusRepository;
        private readonly IRepository<TaskType> _taskTypeRepository;
        private readonly IValidator<Task> _taskValidator;
        private readonly IValidator<Project> _projectValidator;
        private readonly IValidator<Teammate> _teammateValidator;
        private readonly IValidator<Sprint> _sprintValidator;

        public TaskService(IRepository<Task> repository, IValidator<Task> validator,
            IValidator<Project> projectValidator,
            IValidator<Teammate> teammateValidator, IValidator<Sprint> sprintValidator,
            IRepository<TaskStatus> taskStatusRepository, IRepository<TaskType> taskTypeRepository)
        {
            _taskRepository = repository;
            _taskValidator = validator;
            _projectValidator = projectValidator;
            _teammateValidator = teammateValidator;
            _sprintValidator = sprintValidator;
            _taskStatusRepository = taskStatusRepository;
            _taskTypeRepository = taskTypeRepository;
        }

        public void Add(TaskViewModel value)
        {
            var task = Mapper.Map<TaskViewModel, Task>(value);

            if (!_taskValidator.IsValid(task))
            {
                throw new ValidationException("Entity is not valid.");
            }

            _taskRepository.Add(task);
            _taskRepository.SaveChanges();
        }

        public void Edit(TaskViewModel value)
        {
            var oldTask = _taskRepository.FindById(value.Id);
            if (oldTask == null)
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }

            var task = Mapper.Map(value, oldTask);

            if (!_taskValidator.IsValid(task))
            {
                throw new ValidationException("Entity is not valid.");
            }

            _taskRepository.Update(task);
        }


        public void Delete(int entityId)
        {
            var task = _taskRepository.FindById(entityId);
            if (task == null)
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }

            if (task.ChildTasks != null)
            {
                var subtasks = task.ChildTasks.ToList();
                for (int i = 0; i < subtasks.Count; i++)
                {
                    _taskRepository.Delete(subtasks[i].Id);
                } 
            }

            _taskRepository.Delete(entityId);
            _taskRepository.SaveChanges();
        }

        public IEnumerable<TaskViewModel> GetAll()
        {
            var tasks = _taskRepository.GetAll();
            return Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);
        }

        public IEnumerable<TaskStatusViewModel> GetAllStatuses()
        {
            var statuses = _taskStatusRepository.GetAll();
            return Mapper.Map<IEnumerable<TaskStatus>, IEnumerable<TaskStatusViewModel>>(statuses);
        }

        public IEnumerable<TaskTypeViewModel> GetAllTypes()
        {
            var types = _taskTypeRepository.GetAll();
            return Mapper.Map<IEnumerable<TaskType>, IEnumerable<TaskTypeViewModel>>(types);
        }

        public IEnumerable<TaskViewModel> FindByParentTask(int parentId)
        {
            var subtasks = _taskRepository.Find(x => x.ParentId == parentId);
            return Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(subtasks);
        }

        public TaskViewModel FindById(int entityId)
        {
            var task = _taskRepository.FindById(entityId);
            if (task == null)
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }
            return Mapper.Map<Task, TaskViewModel>(task);
        }

        public IEnumerable<TaskViewModel> FindByProject(int projectId)
        {
            if (!_projectValidator.IsExist(projectId))
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }
            var tasks = _taskRepository.Find(m => m.ProjectId == projectId);
            return Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);
        }

        public IEnumerable<TaskViewModel> FindByTeammate(int teammateId)
        {
            if (!_teammateValidator.IsExist(teammateId))
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }
            var tasks = _taskRepository.Find(m => m.TeammateId == teammateId).AsEnumerable();
            return Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);
        }

        public IEnumerable<TaskViewModel> FindbySprint(int sprintId)
        {
            if (!_sprintValidator.IsExist(sprintId))
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }

            var tasks = _taskRepository.Find(m => m.SprintId == sprintId).AsEnumerable();
            return Mapper.Map<IEnumerable<Task>, IEnumerable<TaskViewModel>>(tasks);
        }
    }
}