﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using ScrumBoard.DAL.Entities;
using ScrumBoard.Contracts.Services;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Validator;
using System.Collections.Generic;

namespace ScrumBoard.Business.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly IRepository<ProjectDocument> _repositoryProjectDocument;
        private readonly IRepository<TaskDocument> _repositoryTaskDocument;
        private readonly IValidator<ProjectDocument> _projectDocumentValidator;
        private readonly IRepository<Project> _repositoryProject;
        private readonly IRepository<Task> _repositoryTask;

        public DocumentService(
            IRepository<ProjectDocument> repositoryProjectDocument,
            IRepository<Project> repositoryProject,
            IRepository<TaskDocument> repositoryTaskDocument,
            IRepository<Task> repositoryTask,
            IValidator<ProjectDocument> projectDocumentValidator)
        {
            _repositoryProjectDocument = repositoryProjectDocument;
            _repositoryProject = repositoryProject;
            _repositoryTaskDocument = repositoryTaskDocument;
            _repositoryTask = repositoryTask;
            _projectDocumentValidator = projectDocumentValidator;
        }

        public IEnumerable<DocumentViewModel> FindByProject(int projectId)
        {
            var documents = _repositoryProjectDocument.Find(document => document.ProjectId == projectId);
            return Mapper.Map<IEnumerable<ProjectDocument>, IEnumerable<DocumentViewModel>>(documents);
        }

        public IEnumerable<DocumentViewModel> FindByTask(int taskId)
        {
            var documents = _repositoryTaskDocument.Find(document => document.TaskId == taskId);
            return Mapper.Map<IEnumerable<TaskDocument>, IEnumerable<DocumentViewModel>>(documents);
        }

        public void Add(DocumentViewModel value)
        {
            switch (value.Type)
            {
                case "Project":
                    AddToProjectRepository(value);
                    _repositoryProjectDocument.SaveChanges();
                    break;
                case "Task":
                    AddToTaskRepository(value);
                    _repositoryTaskDocument.SaveChanges();
                    break;
                default:
                    throw new ValidationException("Invalid document type");
            }
        }

        private void AddToTaskRepository(DocumentViewModel value)
        {
            var document = Mapper.Map<TaskDocument>(value);
            var task = _repositoryTask.FindById(value.ParentId);
            if (task == null)
                throw new ArgumentException("TASK_DOES_NOT_EXIST");
            document.TaskId = task.Id;

            _repositoryTaskDocument.Add(document);
        }

        private void AddToProjectRepository(DocumentViewModel value)
        {
            var document = Mapper.Map<ProjectDocument>(value);

            if (!_projectDocumentValidator.IsValid(document))
                throw new ValidationException(_projectDocumentValidator.Results[0]);

            var project = _repositoryProject.FindById(value.ParentId);
            if (project == null)
                throw new ArgumentException("PROJECT_DOES_NOT_EXIST");
            document.ProjectId = project.Id;

            _repositoryProjectDocument.Add(document);
        }

        public IEnumerable<DocumentViewModel> GetAll()
        {
            var projectDocuments = _repositoryProjectDocument.GetAll();
            var taskDocuments = _repositoryTaskDocument.GetAll();
            var projectViewDocuments = Mapper.Map<IEnumerable<ProjectDocument>, IEnumerable<DocumentViewModel>>(projectDocuments);
            var taskViewDocuments = Mapper.Map<IEnumerable<TaskDocument>, IEnumerable<DocumentViewModel>>(taskDocuments);
            return projectViewDocuments.Concat(taskViewDocuments);
        }

        public DocumentViewModel FindProjectDocument(int documentId)
        {
            var dto = Mapper.Map<DocumentViewModel>(_repositoryProjectDocument.FindById(documentId));
            dto.Type = "Project";

            return dto;
        }

        public DocumentViewModel FindTaskDocument(int documentId)
        {
            var dto = Mapper.Map<DocumentViewModel>(_repositoryTaskDocument.FindById(documentId));
            dto.Type = "Task";

            return dto;
        }

        public void DeleteProjectDocument(int documentId)
        {
            _repositoryProjectDocument.Delete(documentId);
            _repositoryProjectDocument.SaveChanges();
        }

        public void DeleteTaskDocument(int documentId)
        {
            _repositoryTaskDocument.Delete(documentId);
            _repositoryTaskDocument.SaveChanges();
        }
    }
}