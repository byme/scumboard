﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.Contracts.Validator;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Services
{
    public class UserDetailService : IUserDetailService
    {
        private readonly IRepository<UserDetail> _repository;
        private readonly IRepository<UserDetailType> _repositoryUserDetailType;
        private readonly IValidator<UserDetail> _validator;
        private readonly IValidator<User> _userValidator;

        public UserDetailService(IRepository<UserDetail> repository,
            IRepository<UserDetailType> repositoryUserDetailType,
            IValidator<UserDetail> validator, IValidator<User> userValidator)
        {
            _repository = repository;
            _repositoryUserDetailType = repositoryUserDetailType;
            _validator = validator;
            _userValidator = userValidator;
        }

        public void Edit(UserDetailViewModel value)
        {
            var oldDetail = _repository.FindById(value.Id);
            if (oldDetail == null)
            {
                throw new ValidationException();
            }

            var detail = Mapper.Map(value, oldDetail);

            if (!_validator.IsValid(detail))
            {
                throw new ValidationException();
            }

            _repository.Update(detail);
        }

        public void Add(UserDetailViewModel value)
        {
            var detail = Mapper.Map<UserDetailViewModel, UserDetail>(value);

            if (!_validator.IsValid(detail))
            {
                throw new ValidationException();
            }

            _repository.Add(detail);
            _repository.SaveChanges();
        }

        public void Delete(int entityId)
        {
            var detail = _repository.FindById(entityId);

            if (!_validator.IsValid(detail))
            {
                throw new ValidationException();
            }
            _repository.Delete(entityId);
            _repository.SaveChanges();
        }

        public IEnumerable<UserDetailViewModel> GetAll()
        {
            var details = _repository.GetAll();
            return Mapper.Map<IEnumerable<UserDetail>, IEnumerable<UserDetailViewModel>>(details);
        }

        public UserDetailViewModel FindById(int entityId)
        {
            var detail = _repository.FindById(entityId);
            if (detail == null)
            {
                throw new ValidationException();
            }
            return Mapper.Map<UserDetail, UserDetailViewModel>(detail);
        }

        public IEnumerable<UserDetailViewModel> GetUserDetails(int userId)
        {
            if (!_userValidator.IsExist(userId))
            {
                throw new ValidationException();
            }
            var details = _repository.Find(n => n.UserId == userId);
            return Mapper.Map<IEnumerable<UserDetail>, IEnumerable<UserDetailViewModel>>(details);
        }

        public IEnumerable<UserDetailTypeViewModel> GetUserDetailsTypes()
        {
            var types = _repositoryUserDetailType.GetAll();
            if (types == null)
            {
                throw new ArgumentException("Types not found");
            }
            return Mapper.Map<IEnumerable<UserDetailType>, IEnumerable<UserDetailTypeViewModel>>(types);
        }
    }
}