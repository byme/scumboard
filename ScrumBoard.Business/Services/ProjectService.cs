﻿using System;
using System.Linq;
using AutoMapper;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.Contracts.Validator;
using ScrumBoard.DAL.Entities;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ScrumBoard.Business.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IRepository<Project> _repositoryProject;
        private readonly IRepository<Teammate> _repositoryTeammate;
        private readonly IValidator<Project> _projectValidator;
        private readonly IRepository<RoleInProject> _repositoryRoleInProject;
        private readonly IValidator<Teammate> _teammateValidator;

        public ProjectService(
            IRepository<Project> repositoryProject, 
            IRepository<Teammate> repositoryTeammate,
            IRepository<RoleInProject> repositoryRoleInProject,
            IValidator<Project> projectValidator,
            IValidator<Teammate> teammateValidator)
        {
            _repositoryProject = repositoryProject;
            _repositoryTeammate = repositoryTeammate;
            _repositoryRoleInProject = repositoryRoleInProject;
            _projectValidator = projectValidator;
            _teammateValidator = teammateValidator;
        }

        public IEnumerable<ProjectViewModel> FindByUser(int userId)
        {
            var projects = _repositoryTeammate.
                Find(teammate => teammate.Member.Id == userId && teammate.Role.Role == "Project Owner").
                Select(o => o.ProjectId).
                ToArray().
                Select(id => _repositoryProject.FindById(id)).
                Where(project => !project.IsDeleted);

            return Mapper.Map<IEnumerable<Project>, IEnumerable<ProjectViewModel>>(projects.AsEnumerable()).ToArray();
        }

        public IEnumerable<ProjectViewModel> FindByTeammate(int teammateId)
        {
            var projects = _repositoryTeammate.
                Find(teammate => teammate.MemberId == teammateId).
                Select(o => o.ProjectId).
                ToArray().
                Select(id => _repositoryProject.FindById(id)).
                Where(project => !project.IsDeleted);

            return Mapper.Map<IEnumerable<Project>, IEnumerable<ProjectViewModel>>(projects.AsEnumerable()).ToArray();
        }

        public void Add(ProjectViewModel value)
        {
            var newProject = Mapper.Map<Project>(value);
            if (!_projectValidator.IsValid(newProject))
                throw new ValidationException(_projectValidator.Results[0]);

            newProject = _repositoryProject.Add(newProject);
            _repositoryProject.SaveChanges();

            var teammate = new Teammate()  {
                MemberId = value.UserId,
                ProjectId = newProject.Id,
                RoleId = _repositoryRoleInProject.Find(role => role.Role == "Project Owner").FirstOrDefault().Id
            };

            if (!_teammateValidator.IsValid(teammate))
                throw new ValidationException(_teammateValidator.Results[0]);

            _repositoryTeammate.Add(teammate);
            _repositoryTeammate.SaveChanges();
        }

        public void Delete(int entityId)
        {
            var project = _repositoryProject.FindById(entityId);
            if (project == null)
            {
                throw new ArgumentException();
            }
            project.IsDeleted = true;
            _repositoryProject.Update(project);
            _repositoryProject.SaveChanges();
        }

        public IEnumerable<ProjectViewModel> GetAll()
        {
            return Mapper.Map<IEnumerable<Project>, IEnumerable<ProjectViewModel>>(_repositoryProject.GetAll());
        }

        public ProjectViewModel FindById(int entityId)
        {
            var entity = _repositoryProject.FindById(entityId);
            return Mapper.Map<Project, ProjectViewModel>(entity);
        }

        public void Edit(ProjectViewModel value)
        {
            var entity = _repositoryProject.FindById(value.Id);
            if (entity == null)
            {
                throw new ArgumentException();
            }
             entity = Mapper.Map(value, entity);
            if (!_projectValidator.IsValid(entity))
            {
                throw new ValidationException(_projectValidator.Results[0]);
            }
            _repositoryProject.Update(entity);
            _repositoryProject.SaveChanges();
        }
    }
}
