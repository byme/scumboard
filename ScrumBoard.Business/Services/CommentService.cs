﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.Contracts.Validator;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Services
{
    public class CommentService : ICommentService
    {
        private readonly IRepository<Comment> _repository;
        private readonly IValidator<Comment> _commentValidator;

        public CommentService(IRepository<Comment> repository, IValidator<Comment> commentValidator) {
            _repository = repository;
            _commentValidator = commentValidator;
        }

        public void Add(CommentViewModel value)
        {
            var comment = Mapper.Map<CommentViewModel, Comment>(value);
            if (!_commentValidator.IsValid(comment))
            {
                throw new ValidationException(_commentValidator.Results[0]);
            }

            _repository.Add(comment);
            _repository.SaveChanges();
        }

        public IEnumerable<CommentViewModel> FindByTask(int taskId)
        {
            var comments = _repository.Find(c => c.TaskId == taskId);
            return Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentViewModel>>(comments);
        }
    }
}