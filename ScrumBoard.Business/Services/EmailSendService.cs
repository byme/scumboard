﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Configuration;
using ScrumBoard.Contracts.Exceptions;
using ScrumBoard.Contracts.Services;

namespace ScrumBoard.Business.Services
{
    public class EmailSendService : ISender
    {
        private readonly string _email = WebConfigurationManager.AppSettings["emailSenderAdress"];
        private readonly string _password = WebConfigurationManager.AppSettings["emailSenderPassword"];
        private readonly string _host = WebConfigurationManager.AppSettings["smtpHost"];
        private readonly int _port = Convert.ToInt32(WebConfigurationManager.AppSettings["smtpPort"]);


        public void SendMessage(MailMessage message)
        {
            var client = new SmtpClient(_host, _port)
            {
                Credentials = new NetworkCredential(_email, _password),
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory,
                DeliveryFormat = SmtpDeliveryFormat.SevenBit
            };

            try
            {
                client.Send(message);
            }
            catch (Exception e)
            {
                throw new SendMailException(e.Message);
            }
        }

        public void SendRestorPasswordMessage(string address, string userName,
            string link)
        {
            var body = File.ReadAllText(
                HttpContext.Current.Server.MapPath(
                    WebConfigurationManager.AppSettings["restorePasswodFormPath"]));

            body = body.Replace("#UserName", userName);
            body = body.Replace("#RestoreLink", link);
            var msg = CreateMessage(address, "Restore Password at ScrumBoard", body);

            SendMessage(msg);
        }

        public void SendProfileConfirmationMessage(string address, string userName,
            string link)
        {
            var body = File.ReadAllText(
                HttpContext.Current.Server.MapPath(
                    WebConfigurationManager.AppSettings["profileConfirmationFormPath"]));

            body = body.Replace("#UserName", userName);
            body = body.Replace("#ConfirmationLink", link);
            var msg = CreateMessage(address, "Profile Confirmation Password", body);

            SendMessage(msg);
        }

        private MailMessage CreateMessage(string to, string subject, string body)
        {
            var message = new MailMessage(_email, to)
            {
                Subject = subject,
                IsBodyHtml = false
            };
            message.ReplyToList.Add(_email);
            message.BodyEncoding = Encoding.UTF8;
            message.HeadersEncoding = Encoding.UTF8;
            message.SubjectEncoding = Encoding.UTF8;
            message.Body = null;

            var plainView = AlternateView.CreateAlternateViewFromString(body, message.BodyEncoding, "text/plain");
            plainView.TransferEncoding = TransferEncoding.SevenBit;
            message.AlternateViews.Add(plainView);
            return message;
        }
    }
}