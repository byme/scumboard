﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.Contracts.Validator;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Services
{
    public class TeammateService : ITeammateService
    {
        private readonly IRepository<Teammate> _repositoryTeammate;
        private readonly IValidator<Teammate> _validatorTeammate;
        private readonly IValidator<Project> _validatorProject;
        private readonly IValidator<User> _validatorUser;

        public TeammateService(IRepository<Teammate> repositoryTeammate,
            IValidator<Teammate> validatorTeammate,
            IValidator<Project> validatorProject, IValidator<User> validatorUser)
        {
            _repositoryTeammate = repositoryTeammate;
            _validatorTeammate = validatorTeammate;
            _validatorProject = validatorProject;
            _validatorUser = validatorUser;
        }

        public void Add(TeammateViewModel value)
        {
            var teammate = Mapper.Map<TeammateViewModel, Teammate>(value);
            if (!_validatorTeammate.IsValid(teammate))
            {
                throw new ValidationException("Entity is not valid.");
            }
            _repositoryTeammate.Add(teammate);
            _repositoryTeammate.SaveChanges();
        }

        public void Delete(int entityId)
        {
            var tmp = _repositoryTeammate.FindById(entityId);
            if (tmp == null)
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }
            if (tmp.Role.Role == "Project Owner")
            {
                throw new ArgumentException("Cannot delete teammate, because of his role.");
            }
            if (tmp.Tasks != null)
            {
                foreach (var task in tmp.Tasks)
                {
                    task.TeammateId = null;
                    task.Teammate = null;
                }
            }
            tmp.Tasks = null;
            _repositoryTeammate.Delete(entityId);
            _repositoryTeammate.SaveChanges();
        }

        public IEnumerable<TeammateViewModel> GetAll()
        {
            return _repositoryTeammate.GetAll().Select(teammate => Mapper.Map<Teammate, TeammateViewModel>(teammate));
        }

        public TeammateViewModel FindById(int entityId)
        {
            var teammate = _repositoryTeammate.FindById(entityId);
            if (teammate == null)
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }
            return Mapper.Map<Teammate, TeammateViewModel>(teammate);
        }

        public IEnumerable<TeammateViewModel> FindByProject(int projectId)
        {
            if (!_validatorProject.IsExist(projectId))
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }
            var teammates = _repositoryTeammate.Find(teammate => teammate.ProjectId == projectId);
            return Mapper.Map<IEnumerable<Teammate>, IEnumerable<TeammateViewModel>>(teammates);
        }

        public IEnumerable<TeammateViewModel> FindByUserId(int userId)
        {
            if (!_validatorUser.IsExist(userId))
            {
                throw new ArgumentException("Cannot load entity with such id.");
            }
            var teammates = _repositoryTeammate.Find(o => o.MemberId == userId);
            return Mapper.Map<IEnumerable<Teammate>, IEnumerable<TeammateViewModel>>(teammates);
        }
    }
}