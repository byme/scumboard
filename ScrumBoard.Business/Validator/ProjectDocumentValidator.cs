﻿using FluentValidation;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Validator
{
    public class ProjectDocumentValidator : Validator<ProjectDocument>
    {
        public ProjectDocumentValidator(
            IRepository<ProjectDocument> projectDocumentRepository,
            Contracts.Validator.IValidator<Project> projectValidator)
            : base(projectDocumentRepository)
        {
            RuleFor(o => o.Name).
                Length(0, 100);

            RuleFor(o => o.Url).
                Length(20);

            RuleFor(o => o.ProjectId).
                Must(projectValidator.IsExist);
        }
    }
}