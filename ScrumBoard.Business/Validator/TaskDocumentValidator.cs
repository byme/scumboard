﻿using FluentValidation;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Validator
{
    public class TaskDocumentValidator : Validator<TaskDocument>
    {
        public TaskDocumentValidator(
            IRepository<TaskDocument> tastDocumentRepository, Contracts.Validator.IValidator<Task> taskValidator)
            : base(tastDocumentRepository)
        {
            RuleFor(o => o.Name).
                Length(0, 100);

            RuleFor(o => o.Url).
                Length(20);

            RuleFor(o => o.TaskId).
                Must(taskValidator.IsExist);
        }
    }
}