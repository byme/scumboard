﻿using System;
using FluentValidation;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Validator
{
    public class CommentValidator : Validator<Comment>
    {
        public CommentValidator(
            IRepository<Comment> commentRepository, Contracts.Validator.IValidator<Task> taskValidator,
            Contracts.Validator.IValidator<User> userValidator)
            : base(commentRepository)
        {
            RuleFor(o => o.Text).
                NotEmpty();

            RuleFor(o => o.TaskId).
                Must(taskValidator.IsExist);

            RuleFor(o => o.UserId).
                Must(userValidator.IsExist);

            RuleFor(o => o.Time).
                GreaterThanOrEqualTo(DateTime.Today);
        }
    }
}