﻿using FluentValidation;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Validator
{
    public class TeammateValidator : Validator<Teammate>
    {
        public TeammateValidator(
            IRepository<Teammate> teammateRepository, Contracts.Validator.IValidator<User> userValidator,
            Contracts.Validator.IValidator<Project> projectValidator)
            : base(teammateRepository)
        {
            RuleFor(o => o.MemberId).
                Must(userValidator.IsExist);

            RuleFor(o => o.ProjectId).
                Must(projectValidator.IsExist);
        }
    }
}