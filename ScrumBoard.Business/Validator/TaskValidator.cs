﻿using System;
using FluentValidation;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Validator
{
    public class TaskValidator : Validator<Task>
    {
        public TaskValidator(
            IRepository<Task> taskRepository,
            IRepository<Sprint> sprintRepository, 
            Contracts.Validator.IValidator<Project> projectValidator,
            Contracts.Validator.IValidator<TaskType> taskTypeValidator,
            Contracts.Validator.IValidator<TaskStatus> taskStatusValidator,
            Contracts.Validator.IValidator<Teammate> teammateValidator)
            : base(taskRepository)
        {
            RuleFor(o => o.Name).
                NotEmpty().
                Length(0, 255);

            RuleFor(o => o.Description).
                NotEmpty();

            RuleFor(o => o.ProjectId).
                Must(projectValidator.IsExist);

            RuleFor(o => o.TypeId).
                Must(taskTypeValidator.IsExist);

            RuleFor(o => o.StatusId).
                Must(taskStatusValidator.IsExist);

            RuleFor(o => o.Completed).
                GreaterThanOrEqualTo(0);

            RuleFor(o => o.OriginalEstimate).
                GreaterThanOrEqualTo(0);

            RuleFor(o => o.Remaining).
                GreaterThanOrEqualTo(0);

            RuleFor(o => o).
                Must(task => task.SprintId == null || (
                    sprintRepository.FindById((int)task.SprintId) != null 
                    && sprintRepository.FindById((int)task.SprintId).ProjectId == task.ProjectId));

            RuleFor(o => o.ParentId).
                Must((o, id) => id == null || (taskRepository.FindById((int) id) != null 
                    && taskRepository.FindById((int) id).ParentId == null 
                    && taskRepository.FindById((int) id).TypeId != o.TypeId));

            RuleFor(o => o.TeammateId).
                Must(id => id == null || teammateValidator.IsExist((int) id));
        }
    }
}