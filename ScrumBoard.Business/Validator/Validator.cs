﻿using System.Linq;
using FluentValidation.Results;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.Validator;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Validator
{
    public class Validator<TCore> : FluentValidation.AbstractValidator<TCore>, IValidator<TCore>
        where TCore : class
    {
        protected readonly IRepository<TCore> _coreRepository;

        public Validator(IRepository<TCore> coreRepository)
        {
            _coreRepository = coreRepository;
        }

        private ValidationResult _results;

        public string[] Results
        {
            get { return _results != null ? _results.Errors.Select(o => o.ToString()).ToArray() : new string[0]; }
        }

        public bool IsValid(TCore value)
        {
            _results = Validate(value);
            return _results.IsValid;
        }

        public bool IsExist(int id)
        {
            return _coreRepository.FindById(id) != null;
        }
    }
}