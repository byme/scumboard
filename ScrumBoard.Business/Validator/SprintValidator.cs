﻿using System;
using FluentValidation;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Validator
{
    public class SprintValidator : Validator<Sprint>
    {
        public SprintValidator(
            IRepository<Sprint> sprintRepository, Contracts.Validator.IValidator<Project> projectValidator,
            Contracts.Validator.IValidator<SprintStatus> sprintStatusValidator)
            : base(sprintRepository)
        {
            RuleFor(o => o.Name).
                NotEmpty().
                Length(0, 255);

            RuleFor(o => o.ProjectId).
                Must(projectValidator.IsExist);

            RuleFor(o => o.SprintStatusId).
                Must(sprintStatusValidator.IsExist);

            RuleFor(o => o.StartDate).
                GreaterThanOrEqualTo(DateTime.Today.AddDays(-1));

            RuleFor(o => o.EndDate).
                GreaterThan(it => it.StartDate);
        }
    }
}