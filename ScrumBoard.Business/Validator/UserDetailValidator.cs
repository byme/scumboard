﻿using FluentValidation;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Validator
{
    public class UserDetailValidator : Validator<UserDetail>
    {
        public UserDetailValidator(IRepository<UserDetail> coreRepository,
            Contracts.Validator.IValidator<User> userValidator,
            Contracts.Validator.IValidator<UserDetailType> userDetailTypeValidator)
            : base(coreRepository)
        {
            RuleFor(o => o.Name).NotEmpty();
            RuleFor(o => o.Value).NotEmpty();
            RuleFor(o => o.UserId).
                Must(userValidator.IsExist);
            RuleFor(o => o.DetailTypeId).
                Must(userDetailTypeValidator.IsExist);
        }
    }
}