﻿using FluentValidation;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Validator
{
    public class ProjectValidator : Validator<Project>
    {
        public ProjectValidator(IRepository<Project> projectRepository)
            : base(projectRepository)
        {
            RuleFor(o => o.Name).
                NotEmpty().
                Length(0, 255);
        }
    }
}