﻿using AutoMapper;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Business.Convertors
{
    public static class MapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<ProjectViewModel, Project>()
                .ForMember(n => n.Id, o => o.Ignore());
            Mapper.CreateMap<Project, ProjectViewModel>();
            Mapper.CreateMap<Comment, CommentViewModel>()
                .AfterMap((c, dto) => dto.UserId = c.UserId)
                .AfterMap((c, dto) => dto.TaskId = c.TaskId)
                .AfterMap((c, dto) => dto.UserName = c.User.FirstName + " " + c.User.LastName);
            Mapper.CreateMap<CommentViewModel, Comment>()
                .ForMember(n => n.Id, o => o.Ignore());

            Mapper.CreateMap<Task, TaskViewModel>().AfterMap((n, m) =>
            {
                m.Status = Mapper.Map<TaskStatus, TaskStatusViewModel>(n.Status);
                m.Teammate = Mapper.Map<Teammate, TeammateViewModel>(n.Teammate);
                m.Type = Mapper.Map<TaskType, TaskTypeViewModel>(n.Type);
            });

            Mapper.CreateMap<TaskViewModel, Task>()
                .ForMember(n => n.Id, o => { o.Ignore(); })
                .ForMember(n => n.Project, o => { o.Ignore(); })
                .ForMember(n => n.Type, o => { o.Ignore(); })
                .ForMember(n => n.Sprint, o => { o.Ignore(); })
                .ForMember(n => n.Teammate, o => { o.Ignore(); })
                .ForMember(n => n.Status, o => { o.Ignore(); })
                .ForMember(n => n.ProjectId, o => { o.Ignore(); })
                .BeforeMap((n, m) => {
                    if (m.ProjectId == 0) {
                        m.ProjectId = n.ProjectId;
                    }
                })
                .AfterMap((n, m) =>
                {
                    if (n.Sprint != null && n.Sprint.Id == 0)
                    {
                        m.SprintId = null;
                    }

                    if (n.Teammate != null && n.Teammate.Id == 0)
                    {
                        m.TeammateId = null;
                    }

                    if (m.ProjectId != 0 && n.ProjectId != m.ProjectId)
                    {
                        m.ProjectId = m.ProjectId;
                    }

                    m.ParentId = n.ParentId == 0 ? (int?) null : n.ParentId;
                });


            Mapper.CreateMap<SprintViewModel, Sprint>()
                .AfterMap((dto, s) => s.SprintStatusId = dto.StatusId)
                .ForMember(n => n.Id, o => o.Ignore());

            Mapper.CreateMap<Sprint, SprintViewModel>()
                .AfterMap((s, dto) =>
                {
                    dto.StatusId = s.SprintStatusId;
                    dto.Status = s.SprintStatus.Status;
                });

            Mapper.CreateMap<TeammateViewModel, Teammate>()
                .ForMember(n => n.Id, o => o.Ignore())
                .ForMember(n => n.Role, o => o.Ignore())
                .ForMember(n => n.Project, o => o.Ignore());

            Mapper.CreateMap<Teammate, TeammateViewModel>().AfterMap((t, dto) =>
            {
                dto.Id = t.Id;
                dto.Name = t.Member.LastName + " " + t.Member.FirstName;
                dto.Role = t.Role.Role;
            })
                .ForMember(n => n.Id, o => o.Ignore());

            Mapper.CreateMap<TaskStatusViewModel, TaskStatus>()
                .ForMember(n => n.Id, o => o.Ignore());

            Mapper.CreateMap<TaskStatus, TaskStatusViewModel>();
            Mapper.CreateMap<ProjectDocument, DocumentViewModel>()
                .AfterMap((p, dto) =>
                {
                    dto.Type = "Project";
                    dto.ParentId = p.ProjectId;
                });
            Mapper.CreateMap<DocumentViewModel, ProjectDocument>()
                .AfterMap((dto, p) => p.ProjectId = dto.ParentId);
            Mapper.CreateMap<TaskDocument, DocumentViewModel>()
                .AfterMap((t, dto) =>
                {
                    dto.Type = "Task";
                    dto.ParentId = t.TaskId;
                });
            Mapper.CreateMap<DocumentViewModel, TaskDocument>()
                .AfterMap((dto, t) => t.TaskId = dto.ParentId);
            Mapper.CreateMap<User, UserViewModel>()
                .ForMember(n => n.IsAdmin, o => o.Ignore());
            Mapper.CreateMap<UserViewModel, User>()
                .ForMember(n => n.Details, o => o.Ignore())
                .ForMember(n => n.Id, o => o.Ignore());
            Mapper.CreateMap<SprintStatus, SprintStatusViewModel>();
            Mapper.CreateMap<SprintStatusViewModel, SprintStatus>()
                .ForMember(n => n.Id, o => o.Ignore());
            Mapper.CreateMap<TaskType, TaskTypeViewModel>();
            Mapper.CreateMap<TaskTypeViewModel, TaskType>()
                .ForMember(n => n.Id, o => o.Ignore());
            Mapper.CreateMap<UserDetail, UserDetailViewModel>();
            Mapper.CreateMap<UserDetailViewModel, UserDetail>()
                .ForMember(n => n.DetailType, o => o.Ignore())
                .ForMember(n => n.User, o => o.Ignore())
                .ForMember(n => n.Id, o => o.Ignore());
            Mapper.CreateMap<UserDetailType, UserDetailTypeViewModel>();
            Mapper.CreateMap<UserDetailTypeViewModel, UserDetailType>()
                .ForMember(n => n.Id, o => o.Ignore());
        }
    }
}