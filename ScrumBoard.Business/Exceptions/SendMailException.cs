﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.Contracts.Exceptions
{
    class SendMailException : Exception
    {
        public SendMailException(string message) : base(message)
        {

        }
    }
}
