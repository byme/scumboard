﻿using ScrumBoard.Contracts.DTO;

namespace ScrumBoard.Contracts.Services
{
    public interface ISprintService : IAddable<SprintViewModel>, IEditable<SprintViewModel>, IAllGetable<SprintViewModel>, IByIdFindable<SprintViewModel>, IDeletable
    {
        SprintViewModel[] FindByProject(int projectId);
    }
}