﻿namespace ScrumBoard.Contracts.Services
{
    public interface IEditable<T>
    {
        /// <summary>
        /// Use for edit the entity
        /// </summary>
        /// <param name="value"></param>
        void Edit(T value);
    }
}