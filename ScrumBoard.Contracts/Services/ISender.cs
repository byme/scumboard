﻿using System.Net.Mail;

namespace ScrumBoard.Contracts.Services
{
    public interface ISender
    {
        void SendMessage(MailMessage message);

        void SendRestorPasswordMessage(string address, string userName,
            string link);

        void SendProfileConfirmationMessage(string address, string userName,
            string link);
    }
}