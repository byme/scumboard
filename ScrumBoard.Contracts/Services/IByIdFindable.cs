﻿namespace ScrumBoard.Contracts.Services
{
    public interface IByIdFindable<T>
    {
        /// <summary>
        /// Use for finding entity by id
        /// </summary>
        /// <param name="entityId">Id of entity</param>
        /// <returns>Return found entity. If entity does not exist return null.</returns>
        T FindById(int entityId);
    }
}