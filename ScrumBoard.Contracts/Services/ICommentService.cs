﻿using System.Collections.Generic;
using ScrumBoard.Contracts.DTO;

namespace ScrumBoard.Contracts.Services
{
    public interface ICommentService : IAddable<CommentViewModel>
    {
        IEnumerable<CommentViewModel> FindByTask(int taskId);
    }
}