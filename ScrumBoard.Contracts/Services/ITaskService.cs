﻿using System.Collections.Generic;
using ScrumBoard.Contracts.DTO;

namespace ScrumBoard.Contracts.Services
{
    public interface ITaskService : IAddable<TaskViewModel>, IEditable<TaskViewModel>, IAllGetable<TaskViewModel>, IByIdFindable<TaskViewModel>, IDeletable
    {
        IEnumerable<TaskViewModel> FindByProject(int projectId);
        IEnumerable<TaskViewModel> FindByTeammate(int teammateId);
        IEnumerable<TaskViewModel> FindbySprint(int sprintId);
        IEnumerable<TaskStatusViewModel> GetAllStatuses();
        IEnumerable<TaskViewModel> FindByParentTask(int parentId);

        IEnumerable<TaskTypeViewModel> GetAllTypes();
    }
}