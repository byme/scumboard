﻿using System.Collections.Generic;
using ScrumBoard.Contracts.DTO;

namespace ScrumBoard.Contracts.Services
{
    public interface IProjectService : IAddable<ProjectViewModel>, IAllGetable<ProjectViewModel>, 
        IByIdFindable<ProjectViewModel>, IEditable<ProjectViewModel>, IDeletable
    {
        IEnumerable<ProjectViewModel> FindByUser(int userId);
        IEnumerable<ProjectViewModel> FindByTeammate(int teammateId);
    }
}