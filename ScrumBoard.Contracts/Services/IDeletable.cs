﻿namespace ScrumBoard.Contracts.Services
{
    public interface IDeletable
    {
        /// <summary>
        /// Use for delete the entity
        /// </summary>
        /// <param name="entityId">Id of entity</param>
        void Delete(int entityId); 
    }
}