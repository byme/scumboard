﻿using System.Collections.Generic;
using ScrumBoard.Contracts.DTO;

namespace ScrumBoard.Contracts.Services
{
    public interface ITeammateService : IAddable<TeammateViewModel>, IAllGetable<TeammateViewModel>, IByIdFindable<TeammateViewModel>, IDeletable
    {
        IEnumerable<TeammateViewModel> FindByProject(int projectId);
        IEnumerable<TeammateViewModel> FindByUserId(int userId);
    }
}