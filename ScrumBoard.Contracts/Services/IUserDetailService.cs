﻿using System.Collections.Generic;
using ScrumBoard.Contracts.DTO;

namespace ScrumBoard.Contracts.Services
{
    public interface IUserDetailService : IAddable<UserDetailViewModel>, IEditable<UserDetailViewModel>, IAllGetable<UserDetailViewModel>, IByIdFindable<UserDetailViewModel>, IDeletable
    {
        IEnumerable<UserDetailViewModel> GetUserDetails(int userId);
        IEnumerable<UserDetailTypeViewModel> GetUserDetailsTypes();
    }
}