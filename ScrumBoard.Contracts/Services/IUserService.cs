﻿using ScrumBoard.Contracts.DTO;

namespace ScrumBoard.Contracts.Services
{
    public interface IUserService : IAddable<UserViewModel>, IEditable<UserViewModel>, IAllGetable<UserViewModel>, IByIdFindable<UserViewModel>, IDeletable
    {
        UserViewModel FindByEmail(string email);
    }
}