﻿using System.Collections.Generic;
using ScrumBoard.Contracts.DTO;

namespace ScrumBoard.Contracts.Services
{
    public interface IDocumentService : IAddable<DocumentViewModel>, IAllGetable<DocumentViewModel>
    {
        DocumentViewModel FindProjectDocument(int documentId);
        DocumentViewModel FindTaskDocument(int documentId);

        void DeleteProjectDocument(int documentId);
        void DeleteTaskDocument(int documentId);

        IEnumerable<DocumentViewModel> FindByProject(int projectId);
        IEnumerable<DocumentViewModel> FindByTask(int taskId);
    }
}