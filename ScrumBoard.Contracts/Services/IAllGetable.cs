﻿using System.Collections.Generic;

namespace ScrumBoard.Contracts.Services
{
    public interface IAllGetable<T>
    {
        /// <summary>
        /// Use for getting all entities
        /// </summary>
        /// <returns>Array of existing entity</returns>
        IEnumerable<T> GetAll(); 
    }
}