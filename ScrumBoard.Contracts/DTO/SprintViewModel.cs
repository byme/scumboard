﻿using System;

namespace ScrumBoard.Contracts.DTO
{
    public class SprintViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public int ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
    }
}