﻿namespace ScrumBoard.Contracts.DTO
{
    public class UserDetailTypeViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}