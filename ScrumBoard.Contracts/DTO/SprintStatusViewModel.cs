﻿namespace ScrumBoard.Contracts.DTO
{
    public class SprintStatusViewModel
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}