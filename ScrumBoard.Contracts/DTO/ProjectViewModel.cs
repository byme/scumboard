﻿namespace ScrumBoard.Contracts.DTO
{
    public class ProjectViewModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsOwner { get; set; }


    }
}