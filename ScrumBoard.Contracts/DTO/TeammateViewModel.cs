﻿namespace ScrumBoard.Contracts.DTO
{
    public class TeammateViewModel
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public string Name { get; set; }
        public int ProjectId { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }
    }
}
