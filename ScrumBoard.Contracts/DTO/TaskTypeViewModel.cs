﻿namespace ScrumBoard.Contracts.DTO
{
    public class TaskTypeViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}