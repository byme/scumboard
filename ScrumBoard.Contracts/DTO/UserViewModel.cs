﻿namespace ScrumBoard.Contracts.DTO
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Photo { get; set; }
        public bool IsAdmin { get; set; }
        public int AccountId { get; set; }
        public UserDetailViewModel[] Details { get; set; }
    }
}