﻿namespace ScrumBoard.Contracts.DTO
{
    public class UserDetailViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int UserId { get; set; }
        public int DetailTypeId { get; set; }
        public UserDetailTypeViewModel DetailType { get; set; }
    }
}