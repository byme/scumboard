﻿namespace ScrumBoard.Contracts.DTO
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OriginalEstimate { get; set; }
        public int Remaining { get; set; }
        public int Completed { get; set; }

        public int ProjectId { get; set; }
        public string ProjectName { get; set; }

        public int ParentId { get; set; }
        public string ParentName { get; set; }

        public TaskStatusViewModel Status { get; set; }

        public TeammateViewModel Teammate { get; set; }

        public TaskTypeViewModel Type { get; set; }

        public SprintViewModel Sprint { get; set; }
    }
}