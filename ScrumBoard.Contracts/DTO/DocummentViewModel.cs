﻿namespace ScrumBoard.Contracts.DTO
{
    public class DocumentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        public int ParentId { get; set; }
    }
}