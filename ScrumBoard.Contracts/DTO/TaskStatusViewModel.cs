﻿namespace ScrumBoard.Contracts.DTO
{
    public class TaskStatusViewModel
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}