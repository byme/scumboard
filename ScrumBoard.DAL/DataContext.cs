﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using ScrumBoard.DAL.Entities;
using ScrumBoard.DAL.Mappings;

namespace ScrumBoard.DAL
{
    public class DataContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserDetail> UserDetails { get; set; }
        public DbSet<Sprint> Sprints { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<ProjectDocument> ProjectDocuments { get; set; }
        public DbSet<RoleInProject> RolesInProject { get; set; }
        public DbSet<SprintStatus> SprintStatuses { get; set; }
        public DbSet<TaskDocument> TaskDocuments { get; set; }
        public DbSet<TaskStatus> TaskStatuses { get; set; }
        public DbSet<TaskType> TaskTypes { get; set; }
        public DbSet<Teammate> Teammates { get; set; }
        public DbSet<UserDetailType> UserDetailTypes { get; set; }

        public DataContext() : base("DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = true;
            Database.SetInitializer(new BoardInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new ProjectMap());
            modelBuilder.Configurations.Add(new SprintMap());
            modelBuilder.Configurations.Add(new TaskMap());
            modelBuilder.Configurations.Add(new UserDetailMap());
            modelBuilder.Configurations.Add(new TeammateMap());
            modelBuilder.Configurations.Add(new CommentMap());
            modelBuilder.Configurations.Add(new ProjectDocumentMap());
            modelBuilder.Configurations.Add(new RoleInProjectMap());
            modelBuilder.Configurations.Add(new SprintStatusMap());
            modelBuilder.Configurations.Add(new TaskDocumentMap());
            modelBuilder.Configurations.Add(new TaskStatusMap());
            modelBuilder.Configurations.Add(new TaskTypeMap());
            modelBuilder.Configurations.Add(new UserDetailTypeMap());
            base.OnModelCreating(modelBuilder);
        }

        private class BoardInitializer : CreateDatabaseIfNotExists<DataContext>
        {
            protected override void Seed(DataContext context)
            {
                var roles = new List<RoleInProject> {
                    new RoleInProject { Role = "Project Owner" },
                    new RoleInProject { Role = "Developer" },
                    new RoleInProject { Role = "QA" }
                };
                roles.ForEach(m => context.RolesInProject.AddOrUpdate(n => n.Role, m));

                var types = new List<UserDetailType> {
                    new UserDetailType { Type = "Text" },
                    new UserDetailType { Type = "Document" },
                    new UserDetailType { Type = "Photo" }
                };
                types.ForEach(m => context.UserDetailTypes.AddOrUpdate(n => n.Type, m));

                var sprintStatuses = new List<SprintStatus> {
                    new SprintStatus { Status = "Completed" },
                    new SprintStatus { Status = "Current" },
                    new SprintStatus { Status = "Future" }
                };
                sprintStatuses.ForEach(m => context.SprintStatuses.AddOrUpdate(n => n.Status, m));

                var taskStatuses = new List<TaskStatus> {
                    new TaskStatus { Status = "To Do" },
                    new TaskStatus { Status = "In progress" },
                    new TaskStatus { Status = "Testing" },
                    new TaskStatus { Status = "Done" }
                };
                taskStatuses.ForEach(m => context.TaskStatuses.AddOrUpdate(n => n.Status, m));

                var taskTypes = new List<TaskType> {
                    new TaskType { Type = "Feature" },
                    new TaskType { Type = "Task" },
                    new TaskType { Type = "Bug" }
                };
                taskTypes.ForEach(m => context.TaskTypes.AddOrUpdate(n => n.Type, m));
                context.SaveChanges();

                base.Seed(context);
            }
        }
    }
}
