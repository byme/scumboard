﻿using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class Task : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int OriginalEstimate { get; set; }
        public int Remaining { get; set; }
        public int Completed { get; set; }
        public int StatusId { get; set; }
        public int TypeId { get; set; }
        public int ProjectId { get; set; }
        public int? SprintId { get; set; }
        public int? ParentId { get; set; }
        public int? TeammateId { get; set; }

        public virtual TaskStatus Status { get; set; }
        public virtual TaskType Type { get; set; }
        public virtual Project Project { get; set; }
        public virtual Task ParentTask { get; set; }
        public virtual Teammate Teammate { get; set; }
        public virtual Sprint Sprint { get; set; }

        public virtual ICollection<TaskDocument> Documents { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Task> ChildTasks{ get; set; }
    }
}