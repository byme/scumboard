﻿using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class User : Entity
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Photo { get; set; }
        public int AccountId { get; set; }

        public virtual ICollection<Comment> Comments { get; set; } 
        public virtual ICollection<UserDetail> Details { get; set; }
        public virtual ICollection<Teammate> UserInProjects { get; set; }
    }
}