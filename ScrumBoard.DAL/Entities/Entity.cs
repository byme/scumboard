﻿namespace ScrumBoard.DAL.Entities
{
    public abstract class Entity
    {
        public int Id { get; private set; }
    }
}