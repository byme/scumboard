﻿namespace ScrumBoard.DAL.Entities
{
    public class UserDetail : Entity
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int UserId { get; set; }
        public int DetailTypeId { get; set; }

        public virtual User User { get; set; }
        public virtual UserDetailType DetailType { get; set; }
    }
}