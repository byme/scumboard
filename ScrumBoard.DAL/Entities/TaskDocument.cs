﻿namespace ScrumBoard.DAL.Entities
{
    public class TaskDocument : Entity
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public int TaskId { get; set; }

        public virtual Task Task { get; set; }
    }
}