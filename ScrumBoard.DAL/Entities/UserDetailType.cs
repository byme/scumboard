﻿using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class UserDetailType : Entity
    {
        public string Type { get; set; }

        public virtual ICollection<UserDetail> UserDetails { get; set; }
    }
}
