﻿using System;

namespace ScrumBoard.DAL.Entities
{
    public class Comment : Entity
    {
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public int TaskId { get; set; }
        public int UserId { get; set; }

        public virtual Task Task { get; set; }
        public virtual User User { get; set; }
    }
}