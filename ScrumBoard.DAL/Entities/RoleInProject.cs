﻿using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class RoleInProject : Entity
    {
        public string Role { get; set; }

        public virtual ICollection<Teammate> Teammates { get; set; }
    }
}