﻿using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class TaskType : Entity
    {
        public string Type { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }
    }
}