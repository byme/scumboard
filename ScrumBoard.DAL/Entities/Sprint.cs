﻿using System;
using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class Sprint : Entity
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ProjectId { get; set; }
        public int SprintStatusId { get; set; }

        public virtual Project Project { get; set; }
        public virtual SprintStatus SprintStatus { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
    }
}