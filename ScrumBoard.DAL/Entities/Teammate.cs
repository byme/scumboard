﻿using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class Teammate : Entity
    {
        public int MemberId { get; set; }
        public int ProjectId { get; set; }
        public int RoleId { get; set; }

        public virtual User Member { get; set; }
        public virtual Project Project { get; set; }
        public virtual RoleInProject Role { get; set; }

        public virtual ICollection<Task> Tasks { get; set; } 
    }
}