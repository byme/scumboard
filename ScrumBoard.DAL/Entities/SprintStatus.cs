﻿using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class SprintStatus : Entity
    {
        public string Status { get; set; }

        public virtual ICollection<Sprint> Sprints { get; set; }
    }
}