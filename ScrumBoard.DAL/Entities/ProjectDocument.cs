﻿namespace ScrumBoard.DAL.Entities
{
    public class ProjectDocument : Entity
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public int ProjectId { get; set; }

        public virtual Project Project { get; set; }
    }
}