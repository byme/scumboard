﻿using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class Project : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<Teammate> Teammates { get; set; } 
        public virtual ICollection<Sprint> Sprints { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<ProjectDocument> Documents { get; set; }
    }
}