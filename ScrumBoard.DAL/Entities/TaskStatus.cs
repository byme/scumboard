﻿using System.Collections.Generic;

namespace ScrumBoard.DAL.Entities
{
    public class TaskStatus : Entity
    {
        public string Status { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }
    }
}