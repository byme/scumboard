﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using ScrumBoard.Contracts.DAL;

namespace ScrumBoard.DAL.Repository
{
    public static class LockContainer
    {
        public static readonly object LockObject = new object();
    }

    public class EFRepository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _context;
        private readonly IDbSet<T> _entities;

        public EFRepository(DbContext context)
        {
            _context = context;
            _entities = context.Set<T>();
        }

        public T Add(T entity)
        {
            try
            {
                Monitor.Enter(LockContainer.LockObject);
                var newEntity = _entities.Create();
                _entities.Add(newEntity);
                _context.Entry(newEntity).CurrentValues.SetValues(entity);
                return newEntity;
            }
            finally
            {
                Monitor.Exit(LockContainer.LockObject);
            }
        }

        public void Update(T entity)
        {
            SaveChanges();
        }

        public void Delete(int id)
        {
            try
            {
                Monitor.Enter(LockContainer.LockObject);
                var entity = FindById(id);
                _entities.Remove(entity);
            }
            finally
            {
                Monitor.Exit(LockContainer.LockObject);
            }
        }

        public IQueryable<T> GetAll()
        {
            IQueryable<T> data;
            try
            {
                Monitor.Enter(LockContainer.LockObject);
                data = _entities.Where(o => true);
            }
            finally
            {
                Monitor.Exit(LockContainer.LockObject);
            }
            return data;
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> expression)
        {
            IQueryable<T> data;
            try
            {
                Monitor.Enter(LockContainer.LockObject);
                data = _entities.Where(expression);
            }
            finally
            {
                Monitor.Exit(LockContainer.LockObject);
            }
            return data;
        }

        public T FindById(int id)
        {
            T data;
            try
            {
                Monitor.Enter(LockContainer.LockObject);
                data = _entities.Find(id);
            }
            finally
            {
                Monitor.Exit(LockContainer.LockObject);
            }

            return data;
        }

        public void SaveChanges()
        {
            try
            {
                Monitor.Enter(LockContainer.LockObject);
                _context.SaveChanges();
            }
            finally
            {
                Monitor.Exit(LockContainer.LockObject);
            }
        }
    }
}