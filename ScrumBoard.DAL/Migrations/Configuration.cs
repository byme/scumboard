using System.Collections.Generic;
using System.Data.Entity.Migrations;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
    }
}
