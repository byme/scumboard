﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class TaskDocumentMap : EntityTypeConfiguration<TaskDocument>
    {
        public TaskDocumentMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Name).HasMaxLength(100).IsRequired();
            Property(m => m.Url).HasMaxLength(20).IsRequired();

            HasRequired(m => m.Task).WithMany(m => m.Documents).HasForeignKey(m => m.TaskId).WillCascadeOnDelete(true);
        }
    }
}