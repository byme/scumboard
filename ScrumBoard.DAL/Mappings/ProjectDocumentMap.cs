﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class ProjectDocumentMap : EntityTypeConfiguration<ProjectDocument>
    {
        public ProjectDocumentMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Name).HasMaxLength(100).IsRequired();
            Property(m => m.Url).HasMaxLength(20).IsRequired();

            HasRequired(m => m.Project)
                .WithMany(m => m.Documents)
                .HasForeignKey(m => m.ProjectId)
                .WillCascadeOnDelete(false);
        }
    }
}