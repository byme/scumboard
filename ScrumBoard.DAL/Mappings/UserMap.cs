﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            Property(m => m.Id).IsRequired();
            Property(m => m.Email).IsRequired();
            Property(m => m.FirstName).IsRequired().HasMaxLength(40);
            Property(m => m.LastName).IsRequired().HasMaxLength(40);
            Property(m => m.Phone).IsRequired().HasMaxLength(20);
            Property(m => m.Photo).IsRequired();

            ToTable("User");
        }
    }
}