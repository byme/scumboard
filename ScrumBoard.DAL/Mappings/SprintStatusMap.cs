﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class SprintStatusMap : EntityTypeConfiguration<SprintStatus>
    {
        public SprintStatusMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Status).IsRequired().HasMaxLength(15);
        }
    }
}