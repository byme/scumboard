﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class UserDetailTypeMap : EntityTypeConfiguration<UserDetailType>
    {
        public UserDetailTypeMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Type).IsRequired().HasMaxLength(15);
        }
    }
}