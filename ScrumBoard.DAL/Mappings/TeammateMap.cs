﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class TeammateMap : EntityTypeConfiguration<Teammate>
    {
        public TeammateMap()
        {
            HasKey(m => m.Id);

            HasRequired(m => m.Member)
                .WithMany(m => m.UserInProjects)
                .HasForeignKey(m => m.MemberId)
                .WillCascadeOnDelete(true);
            HasRequired(m => m.Project)
                .WithMany(m => m.Teammates)
                .HasForeignKey(m => m.ProjectId)
                .WillCascadeOnDelete(false);
            HasRequired(m => m.Role).WithMany(m => m.Teammates).HasForeignKey(m => m.RoleId).WillCascadeOnDelete(false);
        }
    }
}