﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class UserDetailMap : EntityTypeConfiguration<UserDetail>
    {
        public UserDetailMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Name).IsRequired();
            Property(m => m.Value).IsRequired();

            HasRequired(m => m.User).WithMany(m => m.Details).HasForeignKey(m => m.UserId).WillCascadeOnDelete(true);
        }
    }
}