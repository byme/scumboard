﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class TaskStatusMap : EntityTypeConfiguration<TaskStatus>
    {
        public TaskStatusMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Status).IsRequired().HasMaxLength(15);
        }
    }
}