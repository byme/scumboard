﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class TaskMap : EntityTypeConfiguration<Task>
    {
        public TaskMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Name).IsRequired().HasMaxLength(255);
            Property(m => m.Description).IsRequired();
            Property(m => m.OriginalEstimate).IsRequired();
            Property(m => m.Remaining).IsRequired();
            Property(m => m.Completed).IsRequired();

            HasRequired(m => m.Status).WithMany(m => m.Tasks).HasForeignKey(m => m.StatusId).WillCascadeOnDelete(false);
            HasRequired(m => m.Type).WithMany(m => m.Tasks).HasForeignKey(m => m.TypeId).WillCascadeOnDelete(false);
            HasRequired(m => m.Project)
                .WithMany(m => m.Tasks)
                .HasForeignKey(m => m.ProjectId)
                .WillCascadeOnDelete(false);
            HasOptional(m => m.ParentTask)
                .WithMany(m => m.ChildTasks)
                .HasForeignKey(m => m.ParentId)
                .WillCascadeOnDelete(false);
            HasOptional(m => m.Teammate)
                .WithMany(m => m.Tasks)
                .HasForeignKey(m => m.TeammateId)
                .WillCascadeOnDelete(false);
            HasOptional(m => m.Sprint).WithMany(m => m.Tasks).HasForeignKey(m => m.SprintId).WillCascadeOnDelete(false);
        }
    }
}