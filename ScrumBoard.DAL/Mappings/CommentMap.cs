﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class CommentMap : EntityTypeConfiguration<Comment>
    {
        public CommentMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Text).IsRequired();
            Property(m => m.Time).IsRequired().HasColumnType("date");

            HasRequired(m => m.Task).WithMany(m => m.Comments).HasForeignKey(m => m.TaskId).WillCascadeOnDelete(true);
            HasRequired(m => m.User).WithMany(m => m.Comments).HasForeignKey(m => m.UserId).WillCascadeOnDelete(true);
        }
    }
}