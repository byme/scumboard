﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class SprintMap : EntityTypeConfiguration<Sprint>
    {
        public SprintMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Name).HasMaxLength(255).IsRequired();
            Property(m => m.StartDate).IsRequired().HasColumnType("date");
            Property(m => m.EndDate).IsRequired().HasColumnType("date");

            HasRequired(m => m.Project)
                .WithMany(m => m.Sprints)
                .HasForeignKey(m => m.ProjectId)
                .WillCascadeOnDelete(false);
            HasRequired(m => m.SprintStatus)
                .WithMany(m => m.Sprints)
                .HasForeignKey(m => m.SprintStatusId)
                .WillCascadeOnDelete(false);
        }
    }
}