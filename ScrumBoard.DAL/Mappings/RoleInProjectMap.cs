﻿using System.Data.Entity.ModelConfiguration;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.DAL.Mappings
{
    internal class RoleInProjectMap : EntityTypeConfiguration<RoleInProject>
    {
        public RoleInProjectMap()
        {
            HasKey(m => m.Id);
            Property(m => m.Role).IsRequired().HasMaxLength(20);
        }
    }
}