﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumBoard.Business.Services;
using ScrumBoard.DAL.Entities;
using ScrumBoard.Tests.TestHelpers;
using ScrumBoard.Business.Convertors;
using ScrumBoard.Business.Validator;
using ScrumBoard.Contracts.DTO;

namespace ScrumBoard.Tests.Services
{
    /// <summary>
    /// Summary description for ProjectServiceTest
    /// </summary>
    [TestClass]
    public class ProjectServiceTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        #region Additional test attributes

        private ProjectService _projectService;
        private FakeRepository<Project> _projectRepository;
        private FakeRepository<Teammate> _teammateRepository;
        private FakeRepository<RoleInProject> _repositoryRoleInProject;

        [TestInitialize()]
        public void MyTestInitialize()
        {
            _projectRepository = new FakeRepository<Project>();
            _teammateRepository = new FakeRepository<Teammate>();
            _repositoryRoleInProject = new FakeRepository<RoleInProject>();
            _projectService = new ProjectService(
                _projectRepository,
                _teammateRepository,
                _repositoryRoleInProject,
                new ProjectValidator(_projectRepository),
                new Validator<Teammate>(_teammateRepository));

            _projectRepository.Add(new Project() {Name = "TestName1", Description = "Test1"});
            _projectRepository.Add(new Project() {Name = "TestName2", Description = "Test2"});
            _projectRepository.Add(new Project() {Name = "TestName3", Description = "Test3"});
            _projectRepository.Add(new Project() {Name = "TestName4", Description = "Test4"});
            _repositoryRoleInProject.Add(new RoleInProject() {Role = "Project Owner"});
            _repositoryRoleInProject.Add(new RoleInProject() {Role = "Developer"});

            MapperConfig.RegisterMappings();
        }

        #endregion

        [TestMethod]
        public void AddNewProject_AddValidProject_MustBeOk()
        {
            var description = "New GTAVI";
            var name = "GTAVI";

            _projectService.Add(new ProjectViewModel()
            {
                Description = description,
                Name = name
            });

            Assert.IsNotNull(
                _projectRepository.Find(o => o.Description == description).FirstOrDefault(),
                "Must be okey, because add correct Project entity.");
        }

        [TestMethod]
        public void GetAllProjects_GettingAllProjects_MustReturnAllProject()
        {
            Assert.IsTrue(
                _projectService.GetAll().Count() == _projectRepository.GetAll().Count(),
                "Must be equal, because ProjectService get all data from projectRepository");
        }

        [TestMethod]
        public void FindProjectById_GetExistProject_ProjectMustBeEqvaulToRepository()
        {
            int id = 1;
            Assert.IsTrue(_projectService.FindById(id).Name == _projectRepository.FindById(id).Name,
                "Must be equal, because ProjectService get all data from projectRepository");
        }

        [TestMethod]
        public void FindProjectById_GetNotExistProject_MustBeNull()
        {
            int id = 100500;

            ProjectViewModel result = _projectService.FindById(id);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void DeleteProjectById_SendExistId_MustBeOk()
        {
            int id = 1;

            Assert.IsTrue(_projectRepository.Find(o => o.Id == id).Count() == 1);

            _projectService.Delete(id);

            Assert.IsTrue(_projectRepository.Find(o => o.Id == id).FirstOrDefault().IsDeleted);
        }
    }
}