﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumBoard.Business.Convertors;
using ScrumBoard.Business.Services;
using ScrumBoard.Business.Validator;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.DAL.Entities;
using ScrumBoard.Tests.TestHelpers;

namespace ScrumBoard.Tests.Business.Services
{
    [TestClass]
    public class TeammateServiceTest
    {
        private TeammateService _teammateService;
        private FakeRepository<Teammate> _teammateRepository;
        private FakeRepository<User> _userRepository;
        private FakeRepository<Project> _projectRepository;
        private FakeRepository<RoleInProject> _roleRepository;

        [TestInitialize]
        public void TeammateServiceTestInitialize()
        {
            MapperConfig.RegisterMappings();
            _teammateRepository = new FakeRepository<Teammate>();
            _userRepository = new FakeRepository<User>();
            _projectRepository = new FakeRepository<Project>();
            _roleRepository = new FakeRepository<RoleInProject>();
            _teammateService = new TeammateService(
                _teammateRepository,
                new TeammateValidator(
                    _teammateRepository,
                    new Validator<User>(_userRepository), 
                    new Validator<Project>(_projectRepository)
                    ),
                new Validator<Project>(_projectRepository),
                new Validator<User>(_userRepository));

            for (int i = 0; i < 3; i++)
            {
                _userRepository.Add(new User()
                {
                    Email = String.Format("email{0}@email.email", i),
                    FirstName = String.Format("Name{0}", i),
                });
            }
            _roleRepository.Add(new RoleInProject() {Role = "Project Owner"});
            _roleRepository.Add(new RoleInProject() {Role = "Developer"});
            _projectRepository.Add(new Project()
            {
                Name = "Name",
                Description = "Test description",
                IsDeleted = false
            });
            for (int i = 1; i <= 3; i++)
            {
                _teammateRepository.Add(new Teammate()
                {
                    Member = _userRepository.FindById(i),
                    Project = _projectRepository.GetAll().FirstOrDefault(),
                    Role = _roleRepository.Find(x => x.Role == "Developer").FirstOrDefault(),
                });
            }
        }

        [TestMethod]
        public void AddTest_AddValidTeammate_TeammateAdded()
        {
            var dto = new TeammateViewModel()
            {
                MemberId = _userRepository.GetAll().FirstOrDefault().Id,
                ProjectId = _projectRepository.GetAll().FirstOrDefault().Id,
                RoleId = _roleRepository.Find(x => x.Role == "Developer").FirstOrDefault().Id
            };

            _teammateService.Add(dto);

            Assert.IsNotNull(_teammateRepository.Find(x => x.Member.Id == dto.MemberId));
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void AddTest_AddWithUnexpectedProjectId_MustBeException()
        {
            var dto = new TeammateViewModel()
            {
                MemberId = 0,
                ProjectId = 20
            };

            _teammateService.Add(dto);

            Assert.Fail("Must be ValidationException");
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void AddTest_AddWithUnexpectedUserId_MustBeException()
        {
            var dto = new TeammateViewModel()
            {
                MemberId = 20,
                ProjectId = 0
            };

            _teammateService.Add(dto);

            Assert.Fail("Must be ValidationException");
        }

        [TestMethod]
        public void DeleteTest_DeleteWithValidId_EntityIsDeleted()
        {
            int id = 1;

            _teammateService.Delete(id);

            Assert.IsNull(_teammateRepository.FindById(id));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DeleteTest_DeleteWithInvalid_MustBeException()
        {
            int id = 20;

            _teammateService.Delete(id);

            Assert.Fail("Must be ArgumentException");
        }

        [TestMethod]
        public void FindByIdTest_FindWithValidId_EntityIsFound()
        {
            int id = 1;

            var dto = _teammateService.FindById(id);

            Assert.IsNotNull(dto);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FindByIdTest_FindWithInvalidId_MustBeException()
        {
            int id = 20;

            _teammateService.FindById(id);

            Assert.Fail("Must be ArgumentException");
        }

        [TestMethod]
        public void FindByProjectIdTest_FindWithValidId_EntityIsFound()
        {
            int projectId = 1;

            var dto = _teammateService.FindByProject(projectId);

            Assert.IsNotNull(dto);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FindByProjectIdTest_FindWithInvalidId_MustBeException()
        {
            int projectId = 20;

            _teammateService.FindByProject(projectId);

            Assert.Fail("Must be ArgumentException");
        }

        [TestMethod]
        public void GetAllTest_GetAllTeammatesAndConvertToDTO_AllTeammatesReceived()
        {
            var dto = _teammateService.GetAll();

            Assert.IsNotNull(dto);
        }
    }
}