﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumBoard.Business.Convertors;
using ScrumBoard.Business.Services;
using ScrumBoard.Business.Validator;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.DAL.Entities;
using ScrumBoard.Tests.TestHelpers;

namespace ScrumBoard.Tests.Business.Services
{
    [TestClass]
    public class UserServiceTest
    {
        private UserService _userService;
        private FakeRepository<User> _userRepository;


        [TestInitialize]
        public void UserServiceTestInitialize()
        {
            MapperConfig.RegisterMappings();

            _userRepository = new FakeRepository<User>();
            _userService = new UserService(_userRepository,
                new Validator<User>(_userRepository));

            for (int i = 0; i < 3; i++)
            {
                _userRepository.Add(new User()
                {
                    Email = String.Format("email{0}@email.email", i),
                    FirstName = String.Format("Name{0}", i),
                    AccountId = i + 1
                });
            }
        }


        [TestMethod]
        public void FindByIdTest_FindWithValidId_EntityIsFound()
        {
            int id = 1;

            var dto = _userService.FindById(id);

            Assert.IsNotNull(dto);
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentException))]
        public void FindByIdTest_FindWithInvalidId_MustBeException()
        {
            int id = 20;

            _userService.FindById(id);

            Assert.Fail("Must be ArgumentException");
        }

        [TestMethod]
        public void FindByEmailTest_FindWithValidEmail_EntityIsFound()
        {
            string email = String.Format("email1@email.email");
            UserViewModel dto = _userService.FindByEmail(email);
            Assert.IsNotNull(dto);
        }


        [TestMethod]
        public void GetAllTest_GetAllUsersAndConvertToDTO_AllUsersReceived()
        {
            var dto = _userService.GetAll();

            Assert.IsNotNull(dto);
        }
    }
}