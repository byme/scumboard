﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumBoard.Business.Convertors;
using ScrumBoard.Business.Services;
using ScrumBoard.Business.Validator;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.DAL.Entities;
using ScrumBoard.Tests.TestHelpers;

namespace ScrumBoard.Tests.Business.Services
{
    /// <summary>
    ///     Summary description for ProjectServiceTest
    /// </summary>
    [TestClass]
    public class SprintServiceTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void AddNewSprint_AddValidSprint_MustBeOk()
        {
            var name = "Iteration";
            var startDateTime = DateTime.Today;
            var endDateTime = DateTime.Parse("6.11.2030");

            _sprintService.Add(new SprintViewModel
            {
                Name = name,
                StartDate = startDateTime,
                EndDate = endDateTime,
                Id = 1,
                ProjectId = _projectRepository.data[0].Id
            });

            Assert.IsNotNull(
                _sprintRepository.Find(o => o.StartDate == startDateTime).FirstOrDefault(),
                "Must be okey, because add correct Sprint entity.");
        }

        [TestMethod]
        [ExpectedException(typeof (ValidationException))]
        public void AddNewSprint_AddSprintWithNotDateEndValue_MustBeValidationException()
        {
            var name = "";
            var startDateTime = DateTime.Today;

            _sprintService.Add(new SprintViewModel
            {
                Name = name,
                StartDate = startDateTime
            });
        }

        [TestMethod]
        [ExpectedException(typeof (ValidationException))]
        public void AddNewSprint_AddSprintWithNotExistingProgectId_MustBeValidationException()
        {
            var name = "Iteration2";
            var startDateTime = DateTime.Today;
            var endDateTime = DateTime.Parse("7.11.2030");

            _sprintService.Add(new SprintViewModel
            {
                Name = name,
                StartDate = startDateTime,
                EndDate = endDateTime,
                Id = 1,
                ProjectId = 100500
            });
        }

        [TestMethod]
        [ExpectedException(typeof (ValidationException))]
        public void AddNewSprint_AddSprintWithNotValidName_MustBeValidationException()
        {
            var name = "";
            var startDateTime = DateTime.Today;

            _sprintService.Add(new SprintViewModel
            {
                Name = name,
                EndDate = DateTime.Parse("6.11.2030"),
                StartDate = startDateTime,
            });
        }

        [TestMethod]
        public void GetAllSprints_GetingAllSprints_MustReturnAllSprints()
        {
            Assert.IsTrue(
                _sprintRepository.GetAll().Count() == _sprintRepository.GetAll().Count(),
                "Must be equal, because SprintService get all data from sprintRepository");
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentException))]
        public void FindSprintById_GetNotExistSprint_MustBeInvalidOperationException()
        {
            var id = 999999999;
            _sprintService.FindById(id);
        }

        public void FindSprintByProjectId_GetExistSprint_MustBeOk()
        {
            var projectId = _projectRepository.data[0].Id;
            _projectRepository.data[0].Sprints.Add(_sprintRepository.data[0]);

            Assert.IsTrue(_sprintService.FindByProject(projectId).First().Name == _sprintService.GetAll().First().Name,
                "Must be equal, because SprintService get all data from sprintRepository");
        }

        #region Additional test attributes

        private SprintService _sprintService;
        private FakeRepository<Sprint> _sprintRepository;
        private FakeRepository<Project> _projectRepository;
        private FakeRepository<SprintStatus> _sprintStatusRepository;
        
        [TestInitialize]
        public void SprintTestInitialize()
        {
            _sprintRepository = new FakeRepository<Sprint>();
            _projectRepository = new FakeRepository<Project>();
            _sprintStatusRepository = new FakeRepository<SprintStatus>();
            _sprintService = new SprintService(_projectRepository, 
                _sprintRepository,
                _sprintStatusRepository,
                new Validator<Sprint>(_sprintRepository));

            _projectRepository.Add(new Project() {Name = "TestName1", Description = "Test1"});

            _sprintRepository.Add(new Sprint
            {
                Name = "TestName1",
                EndDate = DateTime.Today,
                StartDate = DateTime.Parse("9.10.2030"),
                Project = _projectRepository.data[0]
            });
            _sprintRepository.Add(new Sprint
            {
                Name = "TestName2",
                EndDate = DateTime.Today,
                StartDate = DateTime.Parse("10.3.2030"),
                Project = _projectRepository.data[0]
            });
            _sprintStatusRepository.Add(new SprintStatus()
            {
                Status = "Completed"
            });
            _sprintStatusRepository.Add(new SprintStatus()
            {
                Status = "Current"
            });
            _sprintStatusRepository.Add(new SprintStatus()
            {
                Status = "Future"
            });
            MapperConfig.RegisterMappings();
        }

        [TestCleanup]
        public void MyTestClear()
        {
        }

        #endregion
    }
}