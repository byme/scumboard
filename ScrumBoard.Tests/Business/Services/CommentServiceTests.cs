﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumBoard.Business.Convertors;
using ScrumBoard.Business.Services;
using ScrumBoard.Business.Validator;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.DAL.Entities;
using ScrumBoard.Tests.TestHelpers;
using Task = ScrumBoard.DAL.Entities.Task;

namespace ScrumBoard.Tests.Business.Services
{
    [TestClass]
    public class CommentServiceTests
    {
        private CommentService _commentService;
        private FakeRepository<Comment> _fakeRepositoryComment;
        private FakeRepository<User> _userRepository;
        private FakeRepository<Task> _taskRepository;

        [TestInitialize]
        public void MyTestInitialize()
        {
            MapperConfig.RegisterMappings();

            var projectRepository = new FakeRepository<Project>();

            _taskRepository = new FakeRepository<Task>();
            _taskRepository.Add(new Task
            {
                Name = "Task 1",
                Description = "Description 1",
                OriginalEstimate = 1,
                Remaining = 2,
                ProjectId = 1,
                SprintId = 1,
                StatusId = 2,
                TeammateId = 1,
                TypeId = 2
            });

            var taskTypeRepository = new FakeRepository<TaskType>();

            var taskStatusRepository = new FakeRepository<TaskStatus>();

            var sprintStatusRepository = new FakeRepository<SprintStatus>();

            var sprintRepository = new FakeRepository<Sprint>();

            _userRepository = new FakeRepository<User>();
            _userRepository.Add(new User
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "Email@mail.ru",
                Phone = "123"
            });

            var teammateRepository = new FakeRepository<Teammate>();

            var projectValidator = new ProjectValidator(projectRepository);
            var taskTypeValidator = new Validator<TaskType>(taskTypeRepository);
            var taskStatusValidator = new Validator<TaskStatus>(taskStatusRepository);
            var sprintStatusValidator = new Validator<SprintStatus>(sprintStatusRepository);
            var sprintValidator = new SprintValidator(sprintRepository, projectValidator, sprintStatusValidator);
            var userValidator = new Validator<User>(_userRepository);
            var teammateValidator = new TeammateValidator(teammateRepository, userValidator, projectValidator);
            var taskValidator = new TaskValidator(_taskRepository, sprintRepository, projectValidator, taskTypeValidator,
                taskStatusValidator, teammateValidator);
            var commentValidator = new CommentValidator(_fakeRepositoryComment, taskValidator, userValidator);

            _fakeRepositoryComment = new FakeRepository<Comment>();
            for (var i = 0; i < 10; i++)
            {
                _fakeRepositoryComment.data.Add(
                    new Comment()
                    {
                        Text = "Text" + i,
                        Time = DateTime.Now,
                        UserId = _userRepository.data.First().Id,
                        User = _userRepository.data.First(),
                        TaskId = _taskRepository.data.First().Id
                    });
            }
            _commentService = new CommentService(
                _fakeRepositoryComment, commentValidator);
        }

        [TestMethod]
        public void AddComment_CorrectDTO_AddNewComment()
        {
            var newDto = new CommentViewModel {
                Text = "sdfsd",
                Time = DateTime.Now,
                UserId = 1,
                TaskId = 1,
            };
            _commentService.Add(newDto);

            Assert.IsNotNull(_fakeRepositoryComment.Find(m => m.Text == newDto.Text && m.UserId == newDto.UserId && m.TaskId == newDto.TaskId).FirstOrDefault(),
                "Must be ok as added correct entity");
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void AddComment_IncorrectDTO_ValidationException()
        {
            var newIncorrectDto = new CommentViewModel
            {
                Text = null,
                UserId = 1,
                TaskId = 0
            };
            _commentService.Add(newIncorrectDto);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void AddComment_IncorrectTextDTO_ValidationException()
        {
            var newIncorrectDto = new CommentViewModel
            {
                Text = "   ",
                UserId = 1,
                TaskId = 0
            };
            _commentService.Add(newIncorrectDto);
        }

        [TestMethod]
        public void GetCommentByTask_CorrectTaskId_Comments()
        {
            var taskId = 1;
            var actuallyFindDate = _commentService.FindByTask(taskId);
            Assert.AreEqual(10, actuallyFindDate.Count());
        }

        [TestMethod]
        public void GetCommentByTask_IncorrectTaskId_EmptyArray()
        {
            var taskId = 123;
            var actuallyFindDate = _commentService.FindByTask(taskId);
            Assert.IsTrue(!actuallyFindDate.Any());
        }
    }
}