﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScrumBoard.Business.Convertors;
using ScrumBoard.Business.Services;
using ScrumBoard.Business.Validator;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.DAL.Entities;
using ScrumBoard.Tests.TestHelpers;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ScrumBoard.Tests.Business.Services
{
    [TestClass]
    public class TaskServiceTest
    {
        #region Additional test attributes

        private TaskService _service;
        private FakeRepository<Task> _taskRepository;

        [TestInitialize()]
        public void MyTestInitialize()
        {
            _taskRepository = new FakeRepository<Task>();
            _taskRepository.Add(new Task
            {
                Name = "Task 1",
                Description = "Description 1",
                OriginalEstimate = 1,
                Remaining = 2,
                ProjectId = 1,
                SprintId = 1,
                StatusId = 2,
                TeammateId = 1,
                TypeId = 2
            });
            _taskRepository.Add(new Task
            {
                Name = "Task 2",
                Description = "Description 2",
                OriginalEstimate = 1,
                Remaining = 2,
                ProjectId = 1,
                SprintId = 1,
                StatusId = 2,
                TeammateId = 1,
                TypeId = 2
            });

            var projectRepository = new FakeRepository<Project>();
            projectRepository.Add(new Project {Name = "Project 1"});

            var taskTypeRepository = new FakeRepository<TaskType>();
            taskTypeRepository.Add(new TaskType {Type = "Feature"});
            taskTypeRepository.Add(new TaskType {Type = "Task"});
            taskTypeRepository.Add(new TaskType {Type = "Bug"});

            var taskStatusRepository = new FakeRepository<TaskStatus>();
            taskStatusRepository.Add(new TaskStatus {Status = "To Do"});
            taskStatusRepository.Add(new TaskStatus {Status = "In Progress"});
            taskStatusRepository.Add(new TaskStatus {Status = "Testing"});
            taskStatusRepository.Add(new TaskStatus {Status = "Done"});

            var sprintStatusRepository = new FakeRepository<SprintStatus>();
            sprintStatusRepository.Add(new SprintStatus {Status = "Completed"});
            sprintStatusRepository.Add(new SprintStatus {Status = "Current"});
            sprintStatusRepository.Add(new SprintStatus {Status = "Future"});

            var sprintRepository = new FakeRepository<Sprint>();
            sprintRepository.Add(new Sprint {Name = "Sprint 1", SprintStatusId = 2, ProjectId = 1});

            var userRepository = new FakeRepository<User>();
            userRepository.Add(new User
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "Email@mail.ru",
                Phone = "123"
            });

            var teammateRepository = new FakeRepository<Teammate>();
            teammateRepository.Add(new Teammate {MemberId = 1, ProjectId = 1, RoleId = 1});

            var projectValidator = new ProjectValidator(projectRepository);
            var taskTypeValidator = new Validator<TaskType>(taskTypeRepository);
            var taskStatusValidator = new Validator<TaskStatus>(taskStatusRepository);
            var sprintStatusValidator = new Validator<SprintStatus>(sprintStatusRepository);
            var sprintValidator = new SprintValidator(sprintRepository, projectValidator, sprintStatusValidator);
            var userValidator = new Validator<User>(userRepository);
            var teammateValidator = new TeammateValidator(teammateRepository, userValidator, projectValidator);
            var taskValidator = new TaskValidator(_taskRepository, sprintRepository, projectValidator, taskTypeValidator,
                taskStatusValidator, teammateValidator);


            _service = new TaskService(_taskRepository, taskValidator, projectValidator, teammateValidator,
                sprintValidator, taskStatusRepository, taskTypeRepository);

            MapperConfig.RegisterMappings();
        }

        #endregion

        [TestMethod]
        public void TestTaskService_AddValidTask_Ok()
        {
            var dto = new TaskViewModel()
            {
                Name = "New Task",
                Description = "New Description",
                OriginalEstimate = 10,
                Remaining = 0,
                Completed = 0,
                ProjectId = 1,
                ParentId = 0,
                Status = new TaskStatusViewModel {Id = 1},
                Teammate = new TeammateViewModel {Id = 1},
                Type = new TaskTypeViewModel {Id = 1},
                Sprint = new SprintViewModel {Id = 1}
            };

            _service.Add(dto);

            Assert.IsNotNull(_taskRepository.Find(m => m.Name == dto.Name && m.Description == dto.Description && m.OriginalEstimate == dto.OriginalEstimate),
                "Must be ok as adding correct object");
        }

        [TestMethod]
        [ExpectedException(typeof (ValidationException))]
        public void TestTaskService_RequiredFieldNotSet_ValidationException()
        {
            var dto = new TaskViewModel()
            {
                Description = "New Description",
                OriginalEstimate = 10,
                Remaining = 0,
                Completed = 0,
                ProjectId = 1,
                ParentId = 0,
                Status = new TaskStatusViewModel {Id = 1},
                Teammate = new TeammateViewModel {Id = 1},
                Type = new TaskTypeViewModel {Id = 1},
                Sprint = new SprintViewModel {Id = 1}
            };

            _service.Add(dto);
        }

        [TestMethod]
        [ExpectedException(typeof (ValidationException))]
        public void TestTaskService_FieldTooBigValue_ValidationException()
        {
            var dto = new TaskViewModel()
            {
                Name = "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
                       + "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq",
                Description = "New Description",
                OriginalEstimate = 10,
                Remaining = 0,
                Completed = 0,
                ProjectId = 1,
                ParentId = 0,
                Status = new TaskStatusViewModel {Id = 20},
                Teammate = new TeammateViewModel {Id = 20},
                Type = new TaskTypeViewModel {Id = 20},
                Sprint = new SprintViewModel {Id = 20}
            };

            _service.Add(dto);
        }

        [TestMethod]
        public void TestTaskService_EditValidTask_Ok()
        {
            var dto = new TaskViewModel()
            {
                Id = 1,
                Name = "New Name",
                Description = "New Description",
                OriginalEstimate = 10,
                Remaining = 0,
                Completed = 0,
                ProjectId = 1,
                ParentId = 0,
                Status = new TaskStatusViewModel {Id = 1},
                Teammate = new TeammateViewModel {Id = 1},
                Type = new TaskTypeViewModel {Id = 1},
                Sprint = new SprintViewModel {Id = 1}
            };

            _service.Edit(dto);

            Assert.AreEqual(_taskRepository.FindById(1).Name, dto.Name,
                "Must be ok as editing correct object");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestTaskService_EditNotExistingTask_ArgumentException()
        {
            var dto = new TaskViewModel()
            {
                Id = 20,
                Name = "New Name",
                Description = "New Description",
                OriginalEstimate = 10,
                Remaining = 0,
                Completed = 0,
                ProjectId = 1,
                ParentId = 0,
                Status = new TaskStatusViewModel {Id = 1},
                Teammate = new TeammateViewModel {Id = 1},
                Type = new TaskTypeViewModel {Id = 1},
                Sprint = new SprintViewModel {Id = 1}
            };

            _service.Edit(dto);
        }

        [TestMethod]
        [ExpectedException(typeof (ValidationException))]
        public void TestTaskService_EditInvalidTask_ValidationException()
        {
            var dto = new TaskViewModel()
            {
                Id = 1,
                Description = "New Description",
                OriginalEstimate = 10,
                Remaining = 0,
                Completed = 0,
                ProjectId = 1,
                ParentId = 0,
                Status = new TaskStatusViewModel {Id = 1},
                Teammate = new TeammateViewModel {Id = 1},
                Type = new TaskTypeViewModel {Id = 1},
                Sprint = new SprintViewModel {Id = 1}
            };

            _service.Edit(dto);
        }

        [TestMethod]
        public void TestTaskService_DeleteValidTask_Ok()
        {
            _service.Delete(1);

            Assert.IsNull(_taskRepository.FindById(1),
                "Must be ok as delete existing object");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestTaskService_DeleteNotExistingTask_ArgumentException()
        {
            _service.Delete(10);
        }

        [TestMethod]
        public void TestTaskService_GetAllTasks_Ok()
        {
            Assert.IsTrue(_service.GetAll().Any(), "Must be ok as data is stored.");
        }

        [TestMethod]
        public void TestTaskService_FindByIdTask_Ok()
        {
            Assert.IsNotNull(_service.FindById(1),
                "Must be ok as looking for existing object");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestTaskService_FindByIdNotExistingTask_ArgumentException()
        {
            _service.FindById(10);
        }

        [TestMethod]
        public void TestTaskService_FindByProjectIdTask_Ok()
        {
            Assert.IsNotNull(_service.FindByProject(1),
                "Must be ok as looking for existing object");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestTaskService_FindByProjectIdNotExistingTask_ArgumentException()
        {
            _service.FindByProject(10);
        }

        [TestMethod]
        public void TestTaskService_FindByTeammateIdTask_Ok()
        {
            Assert.IsNotNull(_service.FindByTeammate(1),
                "Must be ok as looking for existing object");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestTaskService_FindByTeammateIdNotExistingTask_ArgumentException()
        {
            _service.FindByTeammate(10);
        }

        [TestMethod]
        public void TestTaskService_FindBySprintIdTask_Ok()
        {
            Assert.IsNotNull(_service.FindbySprint(1),
                "Must be ok as looking for existing object");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestTaskService_FindBySprintIdNotExistingTask_ArgumentException()
        {
            _service.FindbySprint(10);
        }
    }
}