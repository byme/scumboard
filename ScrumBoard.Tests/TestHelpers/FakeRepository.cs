﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.DAL.Entities;

namespace ScrumBoard.Tests.TestHelpers
{
    internal class FakeRepository<Core> : IRepository<Core> where Core : Entity
    {
        public List<Core> data = new List<Core>();

        public Core Add(Core entity)
        {
            if (data.Count > 0)
                entity.GetType().BaseType.GetProperty("Id").SetValue(entity, data.Max(o => o.Id) + 1);
            else
                entity.GetType().BaseType.GetProperty("Id").SetValue(entity, 1);

            data.Add(entity);
            return entity;
        }

        public void Update(Core entity)
        {
            try
            {
                data[data.IndexOf(entity)] = entity;
            }
            catch {}
        }

        public void Delete(int id)
        {
            data.Remove(data.Find(o => o.Id == id));
        }

        public Core FindById(int id)
        {
            return data.Find(o => o.Id == id);
        }

        public IQueryable<Core> GetAll()
        {
            return data.AsQueryable();
        }

        public IQueryable<Core> Find(Expression<Func<Core, bool>> expression)
        {
            return data.Where(expression.Compile()).AsQueryable();
        }

        public void SaveChanges()
        {
        }
    }
}