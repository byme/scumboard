﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel;
using System.Web.Http.Dependencies;
using Castle.Windsor;
using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

namespace ScrumBoard.CastleWindsor
{
    public class CastleDependencyResolver : IDependencyResolver
    {
        private bool _disposed;

        static CastleDependencyResolver _instanse = new CastleDependencyResolver(new WindsorContainer().Install(new AdminInstaller()));

        static public CastleDependencyResolver Instanse { get { return _instanse; } }


        private CastleDependencyResolver(IWindsorContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container", @"The instance of the container cannot be null.");
            }

            Container = container;
        }

        ~CastleDependencyResolver()
        {
            Dispose(false);
        }


        public IWindsorContainer Container { get; protected set; }

        public IDependencyScope BeginScope()
        {
            return new CastleDependencyScope(Container);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return Container.Resolve(serviceType);
            }
            catch (ComponentNotFoundException)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            // Not expected to trow an exception, rather return an empty IEnumerable.
            return Container.ResolveAll(serviceType).Cast<object>();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                if (Container != null)
                {
                    Container.Dispose();
                    Container = null;
                }
            }
            _disposed = true;
        }
    }
}