﻿using System.Data.Entity;
using System.Web.Http;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ScrumBoard.Business.Services;
using ScrumBoard.Business.Validator;
using ScrumBoard.Contracts.DAL;
using ScrumBoard.Contracts.Services;
using ScrumBoard.Contracts.Validator;
using ScrumBoard.DAL;
using ScrumBoard.DAL.Entities;
using ScrumBoard.DAL.Repository;

namespace ScrumBoard.CastleWindsor
{
    public class AdminInstaller : IWindsorInstaller
    {
        private const string WebAssemblyName = "ScrumBoard.WebApi";

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyNamed(WebAssemblyName)
                .BasedOn<ApiController>()
                .LifestyleScoped());

            container.Register(
                Component.For(typeof (DbContext))
                    .ImplementedBy(typeof (DataContext))
                    .LifestyleSingleton());

            container.Register(
                Component.For(typeof (IRepository<>))
                    .ImplementedBy(typeof (EFRepository<>))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IValidator<>))
                    .ImplementedBy(typeof (Validator<>))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IValidator<Project>))
                    .ImplementedBy(typeof (ProjectValidator))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IValidator<UserDetail>))
                    .ImplementedBy(typeof (UserDetailValidator))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IValidator<Comment>))
                    .ImplementedBy(typeof (CommentValidator))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IValidator<ProjectDocument>))
                    .ImplementedBy(typeof (ProjectDocumentValidator))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IValidator<TaskDocument>))
                    .ImplementedBy(typeof (TaskDocumentValidator))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IValidator<Task>))
                    .ImplementedBy(typeof (TaskValidator))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IValidator<Sprint>))
                    .ImplementedBy(typeof (SprintValidator))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IValidator<Teammate>))
                    .ImplementedBy(typeof (TeammateValidator))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (ICommentService))
                    .ImplementedBy(typeof (CommentService))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IUserDetailService))
                    .ImplementedBy(typeof (UserDetailService))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IDocumentService))
                    .ImplementedBy(typeof (DocumentService))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IProjectService))
                    .ImplementedBy(typeof (ProjectService))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (ISprintService))
                    .ImplementedBy(typeof (SprintService))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (ITaskService))
                    .ImplementedBy(typeof (TaskService))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (ITeammateService))
                    .ImplementedBy(typeof (TeammateService))
                    .LifestyleScoped());

            container.Register(
                Component.For(typeof (IUserService))
                    .ImplementedBy(typeof (UserService))
                    .LifestyleScoped());
            container.Register(
                Component.For(typeof (ISender))
                    .ImplementedBy(typeof (EmailSendService))
                    .LifestyleScoped());
        }
    }
}