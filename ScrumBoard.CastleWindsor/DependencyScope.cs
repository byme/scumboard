﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;
using Castle.MicroKernel;
using Castle.MicroKernel.Lifestyle;
using Castle.Windsor;

namespace ScrumBoard.CastleWindsor
{
    public class CastleDependencyScope : IDependencyScope
    {
        private bool _disposed;

        private IDisposable _scope;

        public CastleDependencyScope(IWindsorContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            Container = container;
            _scope = Container.BeginScope();
        }

        ~CastleDependencyScope()
        {
            Dispose(false);
        }

        public IWindsorContainer Container { get; protected set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return Container.Resolve(serviceType);
            }
            catch (ComponentNotFoundException)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return Container.ResolveAll(serviceType).Cast<object>();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                if (_scope != null)
                {
                    _scope.Dispose();
                    _scope = null;
                }
            }

            _disposed = true;
        }
    }
}