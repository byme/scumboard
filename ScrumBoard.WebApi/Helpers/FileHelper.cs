﻿using System.IO;
using System.Web;
using ScrumBoard.WebAPI.Helpers;

namespace ScrumBoard.WebApi.Helpers
{
    public static class FileHelper
    {
        public static string Save(HttpRequest request, string path, ref string originalName)
        {
            if (request.Files.Count <= 0) return null;

            var file = request.Files[0];
            var newName = RandomStringGenerator.Next(20) + file.FileName.Substring(file.FileName.LastIndexOf('.'));
            var filePath = HttpContext.Current.Server.MapPath(path + newName);
            file.SaveAs(filePath);

            if (request.ApplicationPath != null)
            {
                originalName = file.FileName;
                return newName;
            }
            File.Delete(filePath);
            return null;
        }

        public static string GetUsersFilesVirtualPath(HttpRequest request)
        {
            return request.Url.Scheme + "://" + request.Url.Authority + request.ApplicationPath + "Content/UsersFiles/";
        }
    }
}