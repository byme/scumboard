﻿using System;
using System.Text;

namespace ScrumBoard.WebAPI.Helpers
{
    public static class RandomStringGenerator
    {
        private static readonly Random Rng = new Random();
        private static readonly string Valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        public static string Next(int length)
        {
            StringBuilder sb = new StringBuilder("");
            for (int i = 0; i < length; i++)
                sb.Append(Valid[Rng.Next(Valid.Length)]);

            return sb.ToString();
        }
    }
}