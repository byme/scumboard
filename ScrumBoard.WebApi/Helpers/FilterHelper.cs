﻿using ScrumBoard.CastleWindsor;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScrumBoard.WebApi.Helpers
{
    public enum FindBy
    {
        ProjectId,
        ProjectDTO,
        TeammateDTO,
        TeammateId,
        SprintDTO,
        SprintId,
        TaskDTO,
        TaskId
    }

    public class FilterHelper
    {
        static readonly IUserService _userService = CastleDependencyResolver.Instanse.BeginScope().GetService(typeof(IUserService)) as IUserService;
        static readonly ITeammateService _teammateService = CastleDependencyResolver.Instanse.BeginScope().GetService(typeof(ITeammateService)) as ITeammateService;
        static readonly ISprintService _sprintService = CastleDependencyResolver.Instanse.BeginScope().GetService(typeof(ISprintService)) as ISprintService;
        static readonly ITaskService _taskService = CastleDependencyResolver.Instanse.BeginScope().GetService(typeof(ITaskService)) as ITaskService;


        public static int GetProjectId(FindBy CheckBy, Dictionary<string, object> arguments)
        {
            switch (CheckBy)
            {
                case FindBy.ProjectDTO:
                    return (arguments.First(o => o.Key == "project").Value as ProjectViewModel).Id;

                case FindBy.TeammateDTO:
                    return (arguments.First(o => o.Key == "teammate").Value as TeammateViewModel).ProjectId;

                case FindBy.SprintDTO:
                    return (arguments.First(o => o.Key == "sprint").Value as SprintViewModel).ProjectId;

                case FindBy.TaskDTO:
                    return (arguments.First(o => o.Key == "task").Value as TaskViewModel).ProjectId;

                case FindBy.TeammateId:
                    return _teammateService.FindById((int)arguments.First(o => o.Key == "teammateId").Value).ProjectId;

                case FindBy.SprintId:
                    return _sprintService.FindById((int)arguments.First(o => o.Key == "sprintId").Value).ProjectId;

                case FindBy.ProjectId:
                    return (int)arguments.First(o => o.Key == "projectId").Value;

                case FindBy.TaskId:
                    return _taskService.FindById((int)arguments.First(o => o.Key == "taskId").Value).ProjectId;

                default:
                    break;
            }

            return 0;
        }
    }
}