namespace ScrumBoard.WebApi.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ScrumBoard.WebApi.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ScrumBoard.WebApi.Models.ApplicationDbContext";
        }

        protected override void Seed(ScrumBoard.WebApi.Models.ApplicationDbContext context)
        {
        }
    }
}