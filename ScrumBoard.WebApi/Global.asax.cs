﻿using System.Data.Entity;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Windsor;
using ScrumBoard.Business.Convertors;
using ScrumBoard.CastleWindsor;
using ScrumBoard.Contracts.Services;
using ScrumBoard.WebApi.Models;

namespace ScrumBoard.WebApi
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configuration.DependencyResolver = CastleDependencyResolver.Instanse;

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            

            // Configure WebApi to use a new CastleDependencyResolver as its dependency resolver
            
            
            // Configure WebApi to use the newly configured GlobalConfiguration complete with Castle dependency resolver
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            MapperConfig.RegisterMappings();
        }
    }
}
