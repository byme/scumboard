﻿using ScrumBoard.CastleWindsor;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Microsoft.AspNet.Identity;
using ScrumBoard.WebApi.Helpers;

namespace ScrumBoard.WebApi.Filter
{

    public class ProjectOwnerAttribute : ActionFilterAttribute
    {

        readonly ITeammateService _teammateService = CastleDependencyResolver.Instanse.BeginScope().GetService(typeof(ITeammateService)) as ITeammateService;

        public FindBy CheckBy { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            int currentUserId = HttpContext.Current.User.Identity.GetUserId<int>();
            int projectId = FilterHelper.GetProjectId(CheckBy, actionContext.ActionArguments);

            if (!_teammateService.FindByProject(projectId).Any(o => o.Role == "Project Owner" && o.MemberId == currentUserId) && !HttpContext.Current.User.IsInRole("Admin"))
            {
                actionContext.ModelState.AddModelError("Access", "Access fail!");
            };

            base.OnActionExecuting(actionContext);
        }
    }
}