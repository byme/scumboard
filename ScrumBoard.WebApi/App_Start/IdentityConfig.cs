﻿using System.Threading.Tasks;
using System.Web;
using Castle.MicroKernel.Lifestyle;
using Castle.Windsor;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using ScrumBoard.CastleWindsor;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.WebApi.Models;

namespace ScrumBoard.WebApi
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

    public class ApplicationUserManager : UserManager<ApplicationUser, int>
    {
        private static IUserService _userService;

        public ApplicationUserManager(IUserStore<ApplicationUser, int> store, IUserService userService)
            : base(store)
        {
            _userService = userService;
        }

        public static ApplicationUserManager Create(
            IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var container = new WindsorContainer().Install(new AdminInstaller());
            container.BeginScope();
            var manager = new ApplicationUserManager(
                new UserStore(context.Get<ApplicationDbContext>()), container.Resolve<IUserService>());
            manager.RegisterTwoFactorProvider("PhoneCode",
                new PhoneNumberTokenProvider<ApplicationUser, int>
                {
                    MessageFormat = "Your security code is: {0}"
                });
            manager.RegisterTwoFactorProvider("EmailCode",
                new EmailTokenProvider<ApplicationUser, int>
                {
                    Subject = "Security Code",
                    BodyFormat = "Your security code is: {0}"
                });
            //manager.EmailService = new EmailService(); 
            //manager.SmsService = new SmsService(); 
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser, int>(
                        dataProtectionProvider.Create("ASP.NET Identity"));
            }

            var roleManager = new RoleManager<Role, int>(new RoleStore(context.Get<ApplicationDbContext>()));

            if (!roleManager.RoleExists(SecurityResource.AdminRole))
            {
                roleManager.Create(new Role(SecurityResource.AdminRole));
            }
            if (manager.FindByEmail(SecurityResource.AdminRole) == null)
            {
                var newAdmin = new ApplicationUser
                {
                    UserName = SecurityResource.AdminEmail,
                    Email = SecurityResource.AdminEmail
                };
                IdentityResult result = manager.Create(newAdmin, SecurityResource.AdminPassword);
                if (result.Succeeded)
                {
                    _userService.Add(new UserViewModel
                    {
                        AccountId = newAdmin.Id,
                        Email = SecurityResource.AdminEmail,
                        FirstName = SecurityResource.AdminName,
                        LastName = SecurityResource.AdminLastName,
                        Phone = SecurityResource.AdminPhone,
                        Photo = SecurityResource.DefaultPhoto
                    });
                    manager.AddToRole(newAdmin.Id, SecurityResource.AdminRole);
                    string code = manager.GenerateEmailConfirmationToken(newAdmin.Id);
                    manager.ConfirmEmail(newAdmin.Id, code);
                }
            }
            else
            {
                var currentAdmin = manager.FindByEmail(SecurityResource.AdminEmail);
                manager.AddToRole(currentAdmin.Id, SecurityResource.AdminRole);
            }

            manager.UserValidator = new UserValidator<ApplicationUser, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords 
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
            };
            // Register two factor authentication providers. This application uses Phone 
            // and Emails as a step of receiving a code for verifying the user 
            // You can write your own provider and plug in here. 
            

            return manager;
        }
    }
}