﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using ScrumBoard.WebAPI.Resourse;
using WebMatrix.WebData;

namespace ScrumBoard.WebAPI.App_Start
{
    public class SimpleMembershipInitializer
    {
        public static void MembershipInitializer()
        {
            try
            {
                if (!WebSecurity.Initialized)
                {
                    WebSecurity.InitializeDatabaseConnection(SecurityResourse.ConnectionString, SecurityResourse.Table,
                        SecurityResourse.Id, SecurityResourse.Email,
                        autoCreateTables:
                            true);
                }

                var roles = (SimpleRoleProvider)Roles.Provider;
                var membership = (SimpleMembershipProvider)Membership.Provider;

                if (!roles.RoleExists(SecurityResourse.RoleAdmin))
                {
                    roles.CreateRole(SecurityResourse.RoleAdmin);
                }
                if (membership.GetUser(SecurityResourse.DefaultAdminEmail, false) == null)
                {
                    var adminToken = WebSecurity.CreateUserAndAccount(SecurityResourse.DefaultAdminEmail, SecurityResourse.DefaultAdminPassword,
                    new
                    {
                        FirstName = SecurityResourse.DefaultAdminName,
                        LastName = SecurityResourse.DefaultAdminLastName,
                        Phone = SecurityResourse.DefaultAdminPhone,
                        Photo = SecurityResourse.DefaultAdminAvatar
                    }, true);
                    WebSecurity.ConfirmAccount(adminToken);
                }
                if (!roles.GetRolesForUser(SecurityResourse.DefaultAdminEmail).Contains(SecurityResourse.RoleAdmin))
                {
                    roles.AddUsersToRoles(new[] { SecurityResourse.DefaultAdminEmail }, new[] { SecurityResourse.RoleAdmin });
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    "The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588",
                    ex);
            }
        }
    }
}