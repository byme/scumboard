﻿using System.Collections.Generic;
using ScrumBoard.Contracts.Services;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.WebApi.Filter;
using ScrumBoard.WebApi.Helpers;
using Microsoft.AspNet.Identity;

namespace ScrumBoard.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api")]
    public class ProjectController : ApiController
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [Route("projects")]
        [Authorize(Roles = "Admin")]
        public IEnumerable<ProjectViewModel> GetAll()
        {

            return _projectService.GetAll();
        }

        [Route("project/{projectId}")]
        [Teammate(CheckBy = FindBy.ProjectId)]
        public IHttpActionResult Get(int projectId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var project = _projectService.FindById(projectId);
            var isAdmin = User.IsInRole("Admin");
            var isError = (project == null);
            if(project != null)
            {
                isError = project.IsDeleted && !isAdmin;
            }
            if (isError)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(string.Format("No project with ID = {0}", projectId)),
                    ReasonPhrase = "Project ID Not Found"
                };
                throw new HttpResponseException(resp);
            }

            project.IsOwner = _projectService.FindByUser(User.Identity.GetUserId<int>()).Any(o => o.Id == project.Id);

            return Ok(project);
        }

        [Route("user/{userId}/projects")]
        public IEnumerable<ProjectViewModel> GetByUser(int userId)
        {
            return _projectService.FindByUser(userId);
        }

        [Route("teammate/{teammateId}/projects")]
        public IEnumerable<ProjectViewModel> GetByTeammate(int teammateId)
        {
            var projects = _projectService.FindByTeammate(teammateId);
            var projectsOwner = _projectService.FindByUser(teammateId);

            foreach (var project in projects)
                project.IsOwner = _projectService.FindByUser(teammateId).Any(o => o.Id == project.Id);
            
            return projects;
        }

        [Route("project")]
        [ProjectOwner(CheckBy = FindBy.ProjectDTO)]
        public IHttpActionResult Put(ProjectViewModel project)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _projectService.Edit(project);

            return Ok();
        }

        [Route("project/{projectId}")]
        [ProjectOwner(CheckBy = FindBy.ProjectId)]
        public IHttpActionResult Delete(int projectId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _projectService.Delete(projectId);
            return Ok();
        }

        [Route("project")]
        public void Post(ProjectViewModel project)
        {
            _projectService.Add(project);
        }
    }
}
