﻿using System.Web.Http;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.WebApi.Filter;
using ScrumBoard.WebApi.Helpers;

namespace ScrumBoard.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api")]
    public class SprintController : ApiController
    {
        private readonly ISprintService _sprintService;

        public SprintController(ISprintService sprintService)
        {
            _sprintService = sprintService;
        }

        [HttpGet]
        [Teammate(CheckBy = FindBy.ProjectId)]
        [Route("project/{projectId}/sprints")]
        public IHttpActionResult GetByProject(int projectId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            return Ok(_sprintService.FindByProject(projectId));
        }

        [ProjectOwner(CheckBy = FindBy.SprintDTO)]
        [Route("sprint")]
        public IHttpActionResult Post(SprintViewModel sprint)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _sprintService.Add(sprint);

            return Ok();
        }

        [ProjectOwner(CheckBy = FindBy.SprintId)]
        [Route("sprint/{sprintId}")]
        public IHttpActionResult Delete(int sprintId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _sprintService.Delete(sprintId);

            return Ok();

        }
    }
}
