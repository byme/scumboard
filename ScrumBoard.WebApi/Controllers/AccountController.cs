﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using System.Web.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.WebApi.Helpers;
using ScrumBoard.WebApi.Models;
using ScrumBoard.WebAPI.Helpers;


namespace ScrumBoard.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private ApplicationUserManager _userManager;
        private readonly IUserService _userService;
        private readonly ISender _mailSender;

        public AccountController(IUserService userService, ISender mailSender)
        {
            _userService = userService;
            _mailSender = mailSender;
        }


        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat, IUserService userService, ISender mailSender)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
            _userService = userService;
            _mailSender = mailSender;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }


        [Route("User")]
        public UserViewModel GetUser()
        {
            var id = Authentication.User.Identity.GetUserId<int>();
            var user = _userService.FindById(id);
            user.IsAdmin = User.IsInRole("Admin");
            return user;
        }

        // POST api/Account/Logout

        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId<int>(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IEnumerable<string>> Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return null;
            }
            
            var user = new ApplicationUser { UserName = model.Email , Email = model.Email};
            IdentityResult result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                _userService.Add(new UserViewModel
                {
                    AccountId = user.Id,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.Phone,
                    Photo = SecurityResource.DefaultPhoto
                });
                var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callbackUrl =  Url.Link("CreateUrlMap", 
                    new { controller = "Account", routename = "ConfirmEmail", userId = user.Id, token = code });
                try
                {
                    _mailSender.SendProfileConfirmationMessage(user.Email, user.UserName, callbackUrl);
                }
                catch (Exception ex)
                {
                    return new[] {ex.Message};
                }
                return null;
            }
            return result.Errors;
        }

        [AllowAnonymous]
        [Route("ConfirmEmail")]
        [HttpGet]
        public async Task<IHttpActionResult> ConfirmEmail(int userId, string token)
        {
            IdentityResult result = await UserManager.ConfirmEmailAsync(userId, token.Replace(" ", "+"));
            if (result.Succeeded)
            {
                return Ok();
            }
            return BadRequest();
        }

        [AllowAnonymous]
        [Route("RessetPassword")]
        [HttpGet]
        public async Task<IEnumerable<string>> RessetPassword(int userId, string token)
        {
            ApplicationUser user = await UserManager.FindByIdAsync(userId);
            string newPassword = RandomStringGenerator.Next(6);
            var result = await UserManager.ResetPasswordAsync(userId, token, newPassword);
            if (result.Succeeded)
            {
                try
                {
                    _mailSender.SendRestorPasswordMessage(user.Email, user.UserName,
                                        newPassword + "Your new passworrd, change it");
                }
                catch (Exception ex)
                {
                    return new[] { ex.Message };
                }
                return null;
            }
            return result.Errors;
        }

        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<string> ForgotPassword(ForgotPasswordModel forgotPasswordModel)
        {
            try
            {
                ApplicationUser user = await UserManager.FindByEmailAsync(forgotPasswordModel.Email);
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Link("CreateUrlMap",
                    new { controller = "Account", routename = "RessetPassword", userId = user.Id, token = code });
                try
                {
                    _mailSender.SendProfileConfirmationMessage(user.Email, user.UserName, callbackUrl);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
            return "";
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }



        private IAuthenticationManager Authentication
        {
            get
            {
                //var user = UserManager.FindByEmail(Request.GetOwinContext().Request.User.Identity.Name);
                return /*user.EmailConfirmed ? */Request.GetOwinContext().Authentication /*: null*/;
            }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}
