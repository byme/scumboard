﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.WebApi.Helpers;

namespace ScrumBoard.WebApi.Controllers
{
    [Authorize]
    public class UserDetailController : ApiController
    {
        private readonly IUserDetailService _service;

        public UserDetailController(IUserDetailService service)
        {
            _service = service;
        }

        // GET: api/UserDetail?userid=1
        public IEnumerable<UserDetailViewModel> Get(int userId)
        {
            return _service.GetUserDetails(userId);
        }

        [HttpGet]
        public IHttpActionResult GetDocument(int id)
        {
            var detail = _service.FindById(id);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            var mediaType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content = new StreamContent(new FileStream(
                HttpContext.Current.Server.MapPath("~/Content/UsersFiles/" + detail.Value.Substring(detail.Value.LastIndexOf('/'))), FileMode.Open, FileAccess.Read));
            response.Content.Headers.ContentType = mediaType;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") {
                FileName = detail.Name
            };

            return ResponseMessage(response);
        }

        // GET: api/UserDetail/AllTypes
        public IEnumerable<UserDetailTypeViewModel> Get()
        {
            return _service.GetUserDetailsTypes();
        }

        // POST: api/UserDetail
        public void Post([FromBody]UserDetailViewModel value)
        {
            _service.Add(value);
        }

        // PUT: api/UserDetail/5
        public void Put([FromBody]UserDetailViewModel[] details)
        {
            foreach (var detail in details) {
                if (detail.DetailType.Type == "Photo") {
                    detail.Value = detail.Value.Substring(detail.Value.LastIndexOf("/") + 1);
                }
                _service.Edit(detail);
            }
        }

        // DELETE: api/UserDetail/5
        public void Delete(int detailId)
        {
            _service.Delete(detailId);
        }

        public HttpResponseMessage PostDocument(int userId, int typeId)
        {
            var httpRequest = HttpContext.Current.Request;
            var originalName = String.Empty;
            var imageName = FileHelper.Save(httpRequest, "~/Content/UsersFiles/", ref originalName);
            if (imageName == null) {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            var detail = new UserDetailViewModel {
                DetailTypeId = typeId,
                Name = originalName,
                UserId = userId,
                Value = imageName
            };
            _service.Add(detail);
            return Request.CreateResponse(HttpStatusCode.Created);
        }
    }
}
