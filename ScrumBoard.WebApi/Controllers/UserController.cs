﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.WebApi.Helpers;

namespace ScrumBoard.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api")]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;


        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [Route("user/{userId}")]
        public UserViewModel Get(int userId)
        {
            try {
                var user = _userService.FindById(userId);
                user.Photo = FileHelper.GetUsersFilesVirtualPath(HttpContext.Current.Request) + user.Photo;
                foreach (var detail in user.Details.Where(m => m.DetailTypeId == 3)) {
                    detail.Value = FileHelper.GetUsersFilesVirtualPath(HttpContext.Current.Request) + detail.Value;
                }
                return user;
            }
            catch (InvalidOperationException) {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound) {
                    Content = new StringContent(string.Format("No user with ID = {0}", userId)),
                    ReasonPhrase = "User ID Not Found"
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("username/{namePart}/users")]
        public IEnumerable<UserViewModel> Get(string namePart)
        {
            return _userService.GetAll()
                .Where(m => (m.LastName + " " + m.FirstName).ToLower().Contains(namePart.ToLower()));
        }

        [Route("useremail/{emailPart}/users")]
        public IEnumerable<UserViewModel> GetByEmail(string emailPart)
        {
            return _userService.GetAll()
                .Where(m => m.Email.Contains(emailPart));
        }

        [Route("users")]
        public IEnumerable<UserViewModel> GetAll()
        {
            var users = _userService.GetAll();
            foreach (var user in users) {
                user.Photo = FileHelper.GetUsersFilesVirtualPath(HttpContext.Current.Request) + user.Photo;
            }
            return users;
        }

        [Route("user")]
        public void Put([FromBody]UserViewModel user)
        {
            if(user != null)
        {
            user.Photo = user.Photo.Substring(user.Photo.LastIndexOf("/", StringComparison.Ordinal) + 1);
            _userService.Edit(user);
        }
        }

        [Route("user/{userId}/document")]
        public HttpResponseMessage PostDocument(int userId)
        {
            var httpRequest = HttpContext.Current.Request;
            var originalName = String.Empty;
            var imageName = FileHelper.Save(httpRequest, "~/Content/UsersFiles/", ref originalName);
            if (imageName == null) {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            var user = _userService.FindById(userId);
            user.Photo = imageName;
            _userService.Edit(user);
            return Request.CreateResponse(HttpStatusCode.Created);
        }
    }
}
