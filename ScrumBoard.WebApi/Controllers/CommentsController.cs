﻿using System.Web.Http;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.WebApi.Filter;
using ScrumBoard.WebApi.Helpers;

namespace ScrumBoard.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api")]
    public class CommentsController : ApiController
    {
        private readonly ICommentService _commentService;

        public CommentsController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [Route("task/{taskId}/comments")]
        [Teammate(CheckBy = FindBy.TaskId)]
        public IHttpActionResult Get(int taskId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            return Ok(_commentService.FindByTask(taskId));
        }

        [Route("comment")]
        public void Post([FromBody]CommentViewModel comment)
        {
            _commentService.Add(comment);
        }
    }
}
