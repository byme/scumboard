﻿using System.Collections.Generic;
using ScrumBoard.Contracts.Services;
using System.Web.Http;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.WebApi.Filter;
using ScrumBoard.WebApi.Helpers;

namespace ScrumBoard.WebAPI.Controllers
{
    [RoutePrefix("api")]
    [Authorize]
    public class TeammateController : ApiController
    {
        private readonly ITeammateService _teammateService;

        public TeammateController(ITeammateService teammateService)
        {
            _teammateService = teammateService;
        }

        [Route("teammates")]
        public IEnumerable<TeammateViewModel> GetAll()
        {
            return _teammateService.GetAll();
        }

        [Route("teammate/{teammateId}")]
        public TeammateViewModel Get(int teammateId)
        {
            return _teammateService.FindById(teammateId);
        }

        [Teammate(CheckBy = FindBy.ProjectId)]
        [Route("project/{projectId}/teammates")]
        public IHttpActionResult GetByProject(int projectId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            return Ok(_teammateService.FindByProject(projectId));
        }

        //[ProjectOwner(CheckBy = FindBy.TeammateDTO)]
        //public IHttpActionResult Put(TeammateViewModel teammate)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest();

        //    _teammateService.Edit(teammate); 

        //    return Ok();
        //}
        [Route("teammate")]
        public void Post([FromBody]TeammateViewModel[] newTeammates)
        {
            foreach (var teammate in newTeammates)
            {
                _teammateService.Add(teammate);
            }
        }

        [ProjectOwner(CheckBy = FindBy.TeammateId)]
        [Route("teammate/{teammateId}")]
        public IHttpActionResult Delete(int teammateId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _teammateService.Delete(teammateId);

            return Ok();
        }

    }
}
