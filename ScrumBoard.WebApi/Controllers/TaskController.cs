﻿using System.Collections.Generic;
using System.Web.Http;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.WebApi.Filter;
using ScrumBoard.WebApi.Helpers;

namespace ScrumBoard.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api")]
    public class TaskController : ApiController
    {
        private readonly ITaskService _taskService;
        
        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [Route("tasks")]
        public IEnumerable<TaskViewModel> GetAllTasks()
        {
            return _taskService.GetAll();
        }

        [Route("task/statuses")]
        public IEnumerable<TaskStatusViewModel> GetAllStatuses()
        {
            return _taskService.GetAllStatuses();
        }

        [Route("tasks/types")]
        public IEnumerable<TaskTypeViewModel> GetAllTypes()
        {
            return _taskService.GetAllTypes();
        }

        [Route("project/{projectId}/tasks")]
        [Teammate(CheckBy = FindBy.ProjectId)]
        public IHttpActionResult GetByProject(int projectId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            return Ok(_taskService.FindByProject(projectId));
        }

        [Route("task/{parentId}/tasks")]
        public IEnumerable<TaskViewModel> GetByParentTask(int parentId)
        {
            return _taskService.FindByParentTask(parentId);
        }

        [Route("task/{taskId}")]
        [Teammate(CheckBy = FindBy.TaskId)]
        public IHttpActionResult Get(int taskId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            return Ok(_taskService.FindById(taskId));
        }

        [Route("task")]
        [Teammate(CheckBy = FindBy.TaskDTO)]
        public IHttpActionResult Post([FromBody]TaskViewModel task)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _taskService.Add(task);

            return Ok();
        }


        [Route("task")]
        [Teammate(CheckBy = FindBy.TaskDTO)]
        public IHttpActionResult Put([FromBody]TaskViewModel task)
        {
            _taskService.Edit(task);

            return Ok();
        }

        [Route("task/{taskId}")]
        [Teammate(CheckBy = FindBy.TaskId)]
        public IHttpActionResult Delete(int taskId)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _taskService.Delete(taskId);

            return Ok();
        }
    }
}