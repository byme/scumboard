﻿using System.Collections.Generic;
using ScrumBoard.Contracts.DTO;
using ScrumBoard.Contracts.Services;
using ScrumBoard.WebAPI.Helpers;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace ScrumBoard.WebAPI.Controllers
{
    [RoutePrefix("api/Document")]
    [Authorize]
    public class DocumentController : ApiController
    {
        private readonly IDocumentService _documentService;

        public DocumentController(IDocumentService docummentService)
        {
            _documentService = docummentService;
        }

        
        public IEnumerable<DocumentViewModel> GetByProject(int projectId)
        {
            return _documentService.FindByProject(projectId);
        }

        
        public IEnumerable<DocumentViewModel> GetByTask(int taskId)
        {
            return _documentService.FindByTask(taskId);
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetDocument(int documentId, string documentType)
        {
            DocumentViewModel document;

            switch (documentType)
            {
                case "Project":
                    document = _documentService.FindProjectDocument(documentId);
                    break;
                case "Task":
                    document = _documentService.FindTaskDocument(documentId);
                    break;
                default:
                    return InternalServerError();
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);
            var mediaType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content = new StreamContent(new FileStream(HttpContext.Current.Server.MapPath("~/App_Data/Documents/" + document.Url), FileMode.Open, FileAccess.Read));
            response.Content.Headers.ContentType = mediaType;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") 
            {
                FileName = document.Name
            };

            return ResponseMessage(response);
        }

        public HttpResponseMessage PostDocument(int ParentId, string DocumentType)
        {
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count <= 0) return Request.CreateResponse(HttpStatusCode.BadRequest);
            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[file];
                var document = new DocumentViewModel {
                    ParentId = ParentId,
                    Type = DocumentType,
                    Name = postedFile.FileName,
                    Url = RandomStringGenerator.Next(20)
                };

                var filePath = HttpContext.Current.Server.MapPath("~/App_Data/Documents/" + document.Url);
                postedFile.SaveAs(filePath);

                _documentService.Add(document);
            }

            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpDelete]
        public void Delete(int documentId, string documentType)
        {
            switch (documentType)
            {
                case "Project":
                    _documentService.DeleteProjectDocument(documentId);
                    break;
                case "Task":
                    _documentService.DeleteTaskDocument(documentId);
                    break;
                default:
                    break;
            }
        }
    }
}
