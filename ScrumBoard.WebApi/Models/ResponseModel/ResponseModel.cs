﻿namespace ScrumBoard.WebApi.Models.ResponseModel
{
    public class ResponseModel
    {
        public bool Status;
        public string Message;
        public string UserName;

        public ResponseModel(bool status, string message)
        {
            Status = status;
            Message = message;
        }

        public ResponseModel(bool status, string message, string userName)
        {
            Status = status;
            Message = message;
            UserName = userName;
        }
    }
}