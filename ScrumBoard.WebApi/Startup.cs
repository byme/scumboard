﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using System.Web.Http.Cors;
using Castle.Windsor;
using ScrumBoard.CastleWindsor;
using ScrumBoard.Business.Convertors;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Optimization;

[assembly: OwinStartup(typeof(ScrumBoard.WebApi.Startup))]

namespace ScrumBoard.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
 
            ConfigureAuth(app);
 
            WebApiConfig.Register(config);
            config.EnableCors(new EnableCorsAttribute("*", "*", "GET, POST, OPTIONS, PUT, DELETE"));
            app.UseWebApi(config);


            config.DependencyResolver = CastleDependencyResolver.Instanse;

            MapperConfig.RegisterMappings();
        }
    }
}
