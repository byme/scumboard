﻿(function() {
    var taskService = function ($http) {
        var service = {};
        var url = $$ServiceUrl;

        service.getTaskByProject = function(projectId) {
            return $http({ method: "GET", url: url + "/project/" + projectId + "/tasks" });
        };

        service.getTaskByParent = function(taskId) {
            return $http({ method: "GET", url: url + "/task/" + taskId + "/tasks" });
        }

        service.getTask = function(taskId) {
            return $http({ method: "GET", url: url + "/task/" + taskId });
        }

        service.getTaskStatuses = function () {
            return $http({ method: "GET", url: url + "/task/statuses" });
        }

        service.getTaskTypes = function() {
            return $http({ method: "GET", url: url + "/tasks/types" });;
        }

        service.getAllTasks = function() {
            return $http({ method: "GET", url: url + "/tasks" });
        }

        service.updateTask = function(task) {
            return $http({ method: "PUT", url: url + "/task", data: task });
        };

        service.deleteTask = function(taskId) {
            return $http({ method: "DELETE", url: url + "/task/" + taskId });
        }
        service.post = function(task) {
            return $http({ method: "POST", url: url + "/task", data: task });
        };
        return service;
    };

    scrumBoardApp.factory("taskService", ["$http", taskService]);

}());