﻿(function() {
    var teamService = function($http) {
        var service = {};
        var url = $$ServiceUrl;

        service.getTeammatesByProject = function(projectId) {
            return $http.get(url + "/project/" + projectId + "/teammates");
        }

        service.deleteTeammateFromProject = function(teammateId) {
            return $http.delete(url + "/teammate/" + teammateId);
        }

        service.post = function(newTeammates) {
            return $http.post(url + "/teammate", newTeammates);
        };

        return service;
    };


    scrumBoardApp.service("teamService", ["$http", teamService]);
})();