﻿(function() {
    var sprintService = function($http) {
        var service = {};
        var url = $$ServiceUrl + "/sprint";

        service.getSprintsByProject = function(projectId) {
            return $http.get($$ServiceUrl + "/project/" + projectId + "/sprints");
        }
        service.post = function(sprint) {
            return $http({ method: "POST", url: url, data: sprint });
        };

        service.deleteSprint = function(sprintId) {
            return $http({ method: "DELETE", url: url + "/" + sprintId });
        }

        return service;

    };

    scrumBoardApp.service("sprintService", ["$http", sprintService]);
})();