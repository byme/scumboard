﻿(function() {
    var commentService = function($http) {
        var service = {};
        var url = $$ServiceUrl;

        service.getComments = function(taskId) {
            return $http({ method: "GET", url: url + "/task/" + taskId + "/comments" });
        }

        service.addComment = function(newComment) {
            return $http({ method: "POST", url: url + "/comment", data: newComment });
        }

        return service;
    }

    scrumBoardApp.service("commentService", ["$http", commentService]);
})();