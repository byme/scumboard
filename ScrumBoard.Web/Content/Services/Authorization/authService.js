﻿(function() {
    var authService = function($http, $q, $location, $rootScope, localStorageService) {

        var serviceBase = $$ServiceUrlNoApi;
        var service = {};


        service.saveRegistration = function(registration) {

            service.logOut();

            return $http.post(serviceBase + "api/account/register", registration).then(function(response) {
                return response;
            });
        };

        service.login = function(loginData) {

            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

            var deferred = $q.defer();

            $http.post(serviceBase + "token", data, { headers: { "Content-Type": "application/x-www-form-urlencoded" } }).success(function(response) {

                localStorageService.set("isAuth", true);
                localStorageService.set("userName", loginData.userName);
                localStorageService.set("token", response.access_token);
                service.fillAuthData();

                $http({ method: "GET", url: $$ServiceUrl + "/Account/User" }).success(function (response) {
                    user = {
                        Id: response.Id,
                        Email: response.Email,
                        IsAdmin: response.IsAdmin
                    }
                    localStorageService.set("user", user);
                    $location.path("/ProjectList");
                });

                deferred.resolve(response);

            }).error(function(err, status) {
                service.logOut();
                deferred.reject(err);
            });

            return deferred.promise;

        };

        service.logOut = function() {

            localStorageService.remove("authorizationData");
            localStorageService.remove("isAuth");
            localStorageService.remove("userName");
            localStorageService.remove("token");
            localStorageService.remove("user");

            $http.defaults.headers.common.Authorization = "";
        };

        service.fillAuthData = function() {
            $rootScope.isAuth = localStorageService.get("isAuth");
            if ($rootScope.isAuth) {
                $http.defaults.headers.common.Authorization = "Bearer " + localStorageService.get("token");
            }

        };


        service.token = function() {
            return localStorageService.get("token");
        };

        return service;
    };

    scrumBoardApp.service("authService", ["$http", "$q", "$location", "$rootScope", "localStorageService", authService]);
})();