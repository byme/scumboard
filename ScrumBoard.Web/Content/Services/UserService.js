﻿/// <reference path="UserService.js" />
(function () {
    var userService = function ($http, $window, $location, localStorageService) {
        var service = {};
        var url = $$ServiceUrl;

        service.getAll = function () {
            return $http({ method: "GET", url: url + "/users" });
        }

        service.searchUserByName = function (namePart) {
            return $http({ method: "GET", url: url + "/username/" + namePart + "/users" });
        };

        service.searchUserByEmail = function (emailPart) {
            return $http({ method: "GET", url: url + "/useremail/" + emailPart + "/users" });
        }

        service.getUserById = function (userId) {
            return $http({ method: "GET", url: url + "/user/" + userId });
        };

        service.updateUser = function (user) {
            return $http({ method: "PUT", url: url + "/user", data: user });
        };

        service.getUserDetailsById = function (id) {
            return $http({ method: "GET", url: $$ServiceUrl + "/UserDetail", params: { "userId": id } });
        };

        service.getUserDetailTypes = function () {
            return $http({ method: "GET", url: $$ServiceUrl + "/UserDetail/AllTypes" });
        };

        service.addUserDetail = function (detail) {
            return $http({ method: "POST", url: $$ServiceUrl + "/UserDetail", data: detail });
        };

        service.updateUserDetails = function (details) {
            return $http({ method: "PUT", url: $$ServiceUrl + "/UserDetail", data: details });
        };

        service.deleteDetail = function (detailId) {
            return $http({ method: "DELETE", url: $$ServiceUrl + "/UserDetail", params: { "detailId": detailId } });
        };

        service.getUserDocument = function (detailId) {
            $window.open($$ServiceUrl + "/UserDetail?id=" + detailId, '_blank');
        };

        service.Registration = function (email, password, confirmPassword, firstName, lastName, phone, avatar) {
            var user = {
                Email: email,
                Password: password,
                ConfirmPassword: confirmPassword,
                FirstName: firstName,
                LastName: lastName,
                Phone: phone,
                Avatar: avatar
            };
            return $http.post(url, user);
        }

        service.currentUser = function () {
            var user = localStorageService.get("user");
            if (user)
                return user;
        };

        return service;
    };


    scrumBoardApp.service("userService", ["$http", "$window", "$location", "localStorageService", userService]);
})();