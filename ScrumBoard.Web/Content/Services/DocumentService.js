﻿(function() {
    var documentService = function($http, $window) {
        var service = {};
        var url = $$ServiceUrl + "/Document";

        service.getByProject = function(projectId) {
            return $http({ method: "GET", url: url, params: { "projectId": projectId } });
        };

        service.getByTask = function(taskId) {
            return $http({ method: "GET", url: url, params: { "taskId": taskId } });
        };

        service.getProjectDocument = function(documentId) {
            $window.open(url + "?documentId=" + documentId + "&" + "documentType=Project", '_blank');
        };

        service.getTaskDocument = function(doc) {
            $window.open(url + "?documentId=" + documentId + "&" + "documentType=Task", '_blank');
        };

        service.deleteDocument = function(doc) {
            return $http({ method: "DELETE", url: url + "?documentId=" + doc.Id + "&" + "documentType=" + doc.Type });
        };

        service.post = function(doc) {
            return $http({ method: "POST", url: url, data: doc });
        };

        return service;
    };

    scrumBoardApp.service("documentService", ["$http", "$window", documentService]);
})();