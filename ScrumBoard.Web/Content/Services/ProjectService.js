﻿(function() {
    var projectService = function($http, userService) {

        var service = {};
        var url = $$ServiceUrl;

        service.getAll = function() {
            return $http({ method: "GET", url: url + "/projects" });
        };

        service.get = function(projectId) {
            return $http({ method: "GET", url: url + "/project/" + projectId});
        };

        service.getByUser = function(userId) {
            return $http({ method: "GET", url: url + "/user/" + userId + "/projects"});
        };

        service.getByTeammate = function(teammateId) {
            return $http({ method: "GET", url: url + "/teammate/" + teammateId + "/projects" });
        };

        service.put = function(project) {
            return $http({ method: "PUT", url: url + "/project", data: project });
        };

        service.delete = function(projectId) {
            return $http({ method: "DELETE", url: url + "/project/" + projectId});
        };

        service.post = function(name, description) {
            var user = userService.currentUser();
            if (user)
                return $http({ method: "POST", url: url + "/project", data: { UserId: user.Id, Name: name, Description: description } });
        };

        return service;
    };
    scrumBoardApp.service("projectService", ["$http", "userService", projectService]);
})();