﻿(function() {
    var registrationController = function($scope, $http, $location) {
        var $$ServiceUrl = 'http://localhost:54935/api/Account/Register';
        $scope.registration = function() {
            var user = {
                Email: $scope.email,
                Password: $scope.password,
                ConfirmPassword: $scope.confirmPassword,
                FirstName: $scope.firstName,
                LastName: $scope.secondName,
                Phone: $scope.phone,
            };
            $http.post($$ServiceUrl, user)
                .success(function(data) {
                    $scope.registerResult = data;
                    if (!data)
                        $location.path("/");
                });
        }

        $scope.passwordEquales = function() {
            if ($scope.password === $scope.confirmPassword)
                return true;
            else {
                return false;
            }
        }
    };
    scrumBoardApp.controller("RegistrationController", ["$scope", "$http", "$location", "FileUploader", registrationController]);
})();