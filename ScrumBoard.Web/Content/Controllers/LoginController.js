﻿(function () {
    var loginController = function ($scope, $http, $location, $rootScope, $route, authService, projectService) {
        var $$ServiceUrl = 'http://localhost:54935/api/Account/Login';
        $scope.login = function () {

            var user = {
                userName: $scope.email,
                password: $scope.password,
                RememberMe: $scope.rememberMe
            };
            authService.login(user).then(
                function (response) {
                    $rootScope.isAuth = true;
                    $location.path("/");
                },
                function (err) {
                    $scope.message = err.error_description;
                });
        };
        $scope.toForgotPassword = function () {
            $location.path("/Login/ForgotPassword");
        };
    };
    scrumBoardApp.controller("LoginController", ["$scope", "$http", "$location", "$rootScope", "$route", "authService", "projectService", loginController]);
})();