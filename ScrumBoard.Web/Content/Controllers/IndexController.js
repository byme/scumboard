﻿(function() {
    var IndexController = function($scope, $location, authService) {
        $scope.ProjectList = function() {
            $location.path("/ProjectList");
        };
        $scope.CreateProject = function() {
            $location.path("/Project/Create");
        };
        $scope.CreateSprint = function() {
            $location.path("/Sprint/Create");
        }
        $scope.Registration = function() {
            $location.path("/Registration");
        }
        $scope.Login = function() {
            $location.path("/Login");
        }
        $scope.CreateTask = function() {
            $location.path("/Task/Create");
        }
        $scope.TaskInfo = function() {
            $location.path("/Task/1");
        }
        $scope.LogOut = function() {
            authService.logOut();
        }
    };
    scrumBoardApp.controller("IndexController", ["$scope", "$location", "authService", IndexController]);
})();