﻿(function() {
    var projectDocumentsController = function($scope, $routeParams, documentService, $modal, fileUploader, authService, userService) {

        $scope.searchInput = "";
        $scope.user = userService.currentUser();
        var projectId = $routeParams.projectId;

        $scope.uploader = new fileUploader({
            url: $$ServiceUrl + "/Document?ParentId=" + projectId + "&DocumentType=Project",
            headers: {
                "Authorization": "Bearer " + authService.token()
            }
        });

        $scope.uploader.formData = { ParentId: projectId, DocumentType: "Project" };
        $scope.uploader.autoUpload = true;
        $scope.uploader.autoRemove = true;
        $scope.documents = [];

        $scope.uploader.onSuccessItem = function() {
            documentService.getByProject(projectId).success(function(response) {
                $scope.documentPreview = response;
            });
        };

        $scope.search = function (arg) {
            arg = arg.toLowerCase();
            documentService.getByProject(projectId).then(function(responce) {
                $scope.documentPreview = [];
                var data = responce.data;
                if (arg == "") {
                    $scope.documentPreview = data;
                    return;
                }
                for (var i in data) {
                    if (data.hasOwnProperty(i)) {
                        if (data[i].Name.indexOf(arg) > -1) {
                            $scope.documentPreview.push(data[i]);
                        }
                    }
                }
            });
        };

        var getDocuments = function(projectId) {
            documentService.getByProject(projectId).then(function(responce) {
                $scope.documentPreview = responce.data;
            });
        }
        getDocuments(projectId);

        $scope.addDocument = function() {
            $("#file").click();
        };

        $scope.downloadFile = function(documentId) {
            documentService.getProjectDocument(documentId);
        };

        $scope.animationsEnabled = true;
        $scope.deleteDocument = function(doc) {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: "/Content/Views/Modals/DeleteDocument.html",
                controller: "modalDeleteController",
                resolve: {
                    data: function() {
                        return doc;
                    }
                }

            });

            modalInstance.result.then(function(data) {
                documentService.deleteDocument(doc).success(function() {
                    getDocuments(projectId);
                });
            });
        };

    };
    scrumBoardApp.controller("projectDocumentsController", ["$scope", "$routeParams", "documentService", "$modal", "FileUploader", "authService", "userService", projectDocumentsController]);
})();