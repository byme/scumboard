﻿(function() {
    var modalConfirmProjectController = function ($scope, $location, $modalInstance, projectService, data) {
        $scope.data = data;

        $scope.cancel = function() {
            $modalInstance.dismiss("cancel");
        };
        $scope.ok = function () {
            if ($location.$$path != "/ProjectList") {
                $location.path("/ProjectList");
            }
            $modalInstance.close(data);
        };

    };

    scrumBoardApp.controller("modalConfirmProjectController",
    ["$scope", "$location", "$modalInstance", "projectService", "data", modalConfirmProjectController]);
})();