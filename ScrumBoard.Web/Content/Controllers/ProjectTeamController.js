﻿(function() {
    var projectTeamController = function($scope, $location, $routeParams, $modal, teamService, userService) {

        var getProjectTeam = function(projectId) {
            teamService.getTeammatesByProject(projectId).then(function(responce) {
                $scope.teammatePreview = responce.data;
            });
        }

        $scope.searchInput = "";
        $scope.user = userService.currentUser();

        $scope.goToUser = function(userId) {
            $location.path("/User/" + userId);
        }

        $scope.search = function(arg) {
            arg = arg.toLowerCase();
            teamService.getTeammatesByProject($routeParams.projectId).then(function (responce) {
                $scope.teammatePreview = [];
                var data = responce.data;
                if (arg == "") {
                    $scope.teammatePreview = data;
                    return;
                }
                for (var i in data) {
                    var name = (data[i].Name).toLowerCase();
                    if (name.indexOf(arg) > -1) {
                        $scope.teammatePreview.push(data[i]);
                    }
                }
            });
        }

        $scope.deleteMember = function(memberId) {
            teamService.deleteTeammateFromProject(memberId, $routeParams.projectId).then(function () {
                getProjectTeam($routeParams.projectId);
            });
        }

        $scope.animationsEnabled = true;
        $scope.openModal = function() {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: "/Content/Views/Modals/AddTeammate.html",
                controller: "modalAddTeammateController",
                resolve: {
                    team: function() {
                        return $scope.teammatePreview;
                    }
                }
            });

            modalInstance.result.then(function(users) {
                var teammates = [];
                for (var i in users) {
                    var teammate = {
                        MemberId: users[i].Id,
                        Name: "",
                        ProjectId: $routeParams.projectId,
                        RoleId: 2,
                        Role: ""
                    };
                    teammates.push(teammate);
                }
                teamService.post(teammates).success(function() {
                    getProjectTeam($routeParams.projectId);
                });
            });
        };

        getProjectTeam($routeParams.projectId);

    };

    scrumBoardApp.controller("projectTeamController", ["$scope", "$location", "$routeParams", "$modal", "teamService", "userService", projectTeamController]);
})();