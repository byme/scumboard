﻿(function() {
    var projectController = function ($scope, $location, $routeParams, $rootScope, projectService) {

        var pageName = $routeParams.pageName;
        var url = "/Project/" + $routeParams.projectId + "/";

        $scope.pages =
        [
            { name: 'Info', url: "/Content/Views/ProjectInfo.html" },
            { name: 'Team', url: "/Content/Views/ProjectTeam.html" },
            { name: 'Backlog', url: "/Content/Views/ProjectBacklog.html" },
            { name: 'Sprints', url: "/Content/Views/ProjectSprints.html" },
            { name: 'Documents', url: "/Content/Views/ProjectDocuments.html" },
            { name: 'Board', url: "/Content/Views/Board.html" }
        ];

        var initPage = function() {
            for (var i = 0; i < $scope.pages.length; i++) {
                if ($scope.pages[i].name.toUpperCase() == pageName.toUpperCase()) {
                    $scope.currentPage = $scope.pages[i];
                    return;
                }
            }
        }
        initPage();

        var projectId = $routeParams.projectId;
        $scope.setCurrentPage = function (page) {
            if (page.name.toLowerCase() === "board") {
                $scope.toBoard(projectId);
                return;
            }
            $location.path(url + page.name);
        };

        $scope.isSelected = function(page) {
            return $scope.currentPage === page;
        };

        var getProjectById = function(projectId) {
            projectService.get(projectId)
                .then(function(response) {
                    $scope.project = response.data;
                    $rootScope.projectId = $scope.project.Id;
                    $scope.$broadcast("projectData", $scope.project);
                });
        }

        getProjectById(projectId);

    };

    scrumBoardApp.controller("projectController", ["$scope", "$location", "$routeParams", "$rootScope", "projectService", projectController]);
})();