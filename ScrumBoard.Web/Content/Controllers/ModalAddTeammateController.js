﻿(function() {
    var modalAddTeammateController = function($scope, $routeParams, $modalInstance, userService, teamService) {

        $scope.emailPart = "";

        $scope.users = [];

        $scope.userPreview = [];

        var getTeam = function() {
            teamService.getTeammatesByProject($routeParams.projectId).then(function(responce) {
                $scope.team = responce.data;
            });
        }

        getTeam();

        var checkUsers = function(user) {
            for (var i in $scope.users) {
                if (user.Id == $scope.users[i].Id) {
                    return false;
                }
            }
            for (var i in $scope.team) {
                if (user.Id == $scope.team[i].MemberId) {
                    return false;
                }
            }
            return true;
        }

        $scope.cancel = function() {
            $modalInstance.dismiss("cancel");
        };

        $scope.ok = function() {
            $modalInstance.close($scope.users);
        };

        $scope.search = function(emailPart) {
            $scope.userPreview = [];

            if (emailPart == "") return;

            userService.searchUserByEmail(emailPart).then(function(responce) {

                $scope.userPreview = [];
                for (var i in responce.data) {
                    if (checkUsers(responce.data[i])) {
                        $scope.userPreview.push(responce.data[i]);
                    }
                }

            });
        }

        $scope.addUser = function(user) {
            $scope.userPreview = [];
            $scope.users.push(user);
        }

        $scope.deleteTeammate = function(user) {
            for (var i in $scope.users) {
                if (user.Id == $scope.users[i].Id) {
                    $scope.users.splice(i, 1);
                    break;
                }
            }
        }

    }

    scrumBoardApp.controller("modalAddTeammateController", ["$scope", "$routeParams", "$modalInstance", "userService", "teamService", modalAddTeammateController]);

})();