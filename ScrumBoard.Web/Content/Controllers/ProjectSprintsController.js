﻿(function () {
    var projectSprintsController = function ($scope, $location, $routeParams, $rootScope, sprintService,userService) {
        var projectId = $routeParams.projectId;
        var getSprints = function (projectId) {
            sprintService.getSprintsByProject(projectId).then(function (responce) {
                $scope.sprintPreview = responce.data;
            });
        }

        $scope.user = userService.currentUser();
        $scope.searchInput = "";

        $scope.search = function(arg) {
            arg = arg.toLowerCase();
            sprintService.getSprintsByProject(projectId).then(function (responce) {
                $scope.sprintPreview = [];
                var data = responce.data;
                if (arg == "") {
                    $scope.sprintPreview = data;
                    return;
                }
                for (var i in data) {
                    if (data[i].Name.toLowerCase().indexOf(arg) > -1) {
                        $scope.sprintPreview.push(data[i]);
                    }
                }
            });
        };
        $scope.goToBoard = function (sprintId) {
            $scope.toBoard(projectId, sprintId);
        }
        $scope.addNew = function() {
            $location.path("/Project/" + $rootScope.projectId + "/Sprint/Create");
        }

        $scope.deleteSprint = function(sprintId) {
            sprintService.deleteSprint(sprintId).then(function() {
                getSprints(projectId);
            });
        }

        getSprints(projectId);

    };
    scrumBoardApp.controller("projectSprintsController", ["$scope", "$location", "$routeParams", "$rootScope", "sprintService", "userService", projectSprintsController]);
})();