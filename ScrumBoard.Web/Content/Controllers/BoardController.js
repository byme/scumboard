﻿(function() {
    var boardController = function($scope, $location, $routeParams, $filter, $translate, taskService, teamService, projectService, sprintService) {
        var projectId = $routeParams.projectId;
        var sprintId = $routeParams.sprintId;
        var getProjectName = function() {
            projectService.get(projectId).then(function(response) {
                $scope.projectName = response.data.Name;
            });
        };
        

        $scope.models = {
            SELECT_ALL: 'ALL',
            NOT_ASSIGNED: 'NOT_ASSIGNED',
            WITHOUT_SPRINT: 'WITHOUT_SPRINT',
            groupTasks: [],
            teammates: [],
            sprints: [],
            selectTeammate: null,
            selectSprint: null,
        };

        getProjectName();
        //Add to dropdown Teammate: "All", "Not assigned"
        $scope.models.teammates.push({ Id: $scope.models.SELECT_ALL, Name: $filter('translate')($scope.models.SELECT_ALL) },
                    { Id: $scope.models.NOT_ASSIGNED, Name: $filter('translate')($scope.models.NOT_ASSIGNED) });
        $scope.models.selectTeammate = $scope.models.teammates[0];
        //Add to dropdown: "Without sprint"
        $scope.models.sprints.push({ Id: $scope.models.WITHOUT_SPRINT, Name: $filter('translate')($scope.models.WITHOUT_SPRINT) });
        $scope.models.selectSprint = $scope.models.sprints[0];


        var sortTaskByStatus = function(items, statuses) {
            var group;
            for (var i = 0; i < statuses.length; i++) {
                group = {
                    Status: statuses[i],
                    Items: $filter('filter')(items, statuses[i].Status),
                    Label: statuses[i].Status.replace(' ', '_').toUpperCase(),
                    Class: statuses[i].Status.replace(' ', '_').toLowerCase()
                };
                $scope.models.groupTasks.push(group);
            };
        };
        $scope.changeTaskStatus = function(task) {
            for (var i = 0; i < $scope.models.groupTasks.length; i++) {
                var items = $scope.models.groupTasks[i].Items;
                var newStatus = $scope.models.groupTasks[i].Status;
                var findTask = $filter('filter')(items, { Id: task.Id })[0];
                if (findTask) {
                    if (findTask.Status !== newStatus) {
                        findTask.Status = newStatus;
                        taskService.updateTask(findTask);
                    }
                    return;
                }
            }
        };


        $scope.goToTask = function(id) {
            $location.path("/Project/" + projectId + "/Task/" + id + "/Info");
        };

        $scope.goToUser = function (id) {
            if (!id) return;
            $location.path("/User/" + id);
        }

        teamService.getTeammatesByProject(projectId)
            .then(function (response) {
                $scope.models.teammates = $scope.models.teammates.concat(response.data);
            });

        sprintService.getSprintsByProject(projectId)
           .then(function (response) {
               $scope.models.sprints = $scope.models.sprints.concat(response.data);
               if(sprintId)
               {
                   findSprint = $filter('filter')($scope.models.sprints, sprintId)[0];
                   if (findSprint) {
                       $scope.models.selectSprint = findSprint;
                   }
               }
           });

        taskService.getTaskStatuses()
            .then(function(response) {
                taskService.getTaskByProject(projectId)
                    .then(function(responseTask) {
                        sortTaskByStatus(responseTask.data, response.data);
                    });
            });

    };

    scrumBoardApp.controller("boardController", ["$scope", "$location", "$routeParams", "$filter", "$translate",
        "taskService", "teamService", "projectService","sprintService", boardController]);
})();