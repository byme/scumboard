﻿(function() {
    var projectListController = function ($scope, $location, $modal, projectService, userService) {
        $scope.user = userService.currentUser();


        $scope.searchInput = "";

        $scope.search = function (arg) {
            arg = arg.toLowerCase();
            projectService.getByTeammate($scope.user.Id).then(function (responce) {
                $scope.projectPreview = [];
                var data = responce.data;
                if (arg == "") {
                    $scope.projectPreview = data;
                    return;
                }
                for (var i in data) {
                    if (data[i].Name.toLowerCase().indexOf(arg) > -1) {
                        $scope.projectPreview.push(data[i]);
                    }
                }
            });
        };

        $scope.addProject = function () {
            $location.path("/Project/Create");
        };

        $scope.openProject = function(projectId) {
            $location.path("/Project/" + projectId + "/Info");
        };

        var getProjects = function () {
            if ($scope.user) {
                if (!$scope.user.IsAdmin) {
                    projectService.getByTeammate($scope.user.Id).then(function (responce) {
                        $scope.projectPreview = responce.data;
                    });
                }
                else {
                    projectService.getAll().then(function (responce) {
                        $scope.projectPreview = responce.data;
                    });
                }
            }
        };
        $scope.restoreMessage =
            {
                title: 'RESTORING_PROJECT',
                message: 'CONFIRM_RESTORE_PRJ_MESSAGE'
            }
        $scope.deleteMessage =
           {
               title: 'DELETING_PROJECT',
               message: "CONFIRM_DELETING_PRJ_MESSAGE"
           }
            $scope.animationsEnabled = true;
            $scope.openModal = function (size, message,project) {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                    templateUrl: "/Content/Views/Modals/ModalConfirmProject.html",
                    controller: "modalConfirmProjectController",
                    size: size,
                resolve: {
                        data: function () {
                            var data =
                                {
                                project: project,
                                message: message.message,
                                title: message.title
                            }
                            return data;
                    }
                }
            });

                modalInstance.result.then(function (data) {
                    var project = data.project;
                    if (project.IsDeleted) {
                        project.IsDeleted = false;
                        projectService.put(project);
                    }
                    else {
                        projectService.delete(project.Id).success(function () {
                            getProjects();
                       });
                    }
            });
        };

        getProjects();
    }
    scrumBoardApp.controller("projectListController", ["$scope", "$location", "$modal", "projectService", "userService", projectListController]);
})();