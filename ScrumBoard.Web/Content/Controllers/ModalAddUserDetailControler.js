﻿(function() {
    var modalAddUserDetailController = function ($scope, $routeParams, $modalInstance, userService, FileUploader, authService) {

        var userId = $routeParams.userId;

        var getDetailTypes = function() {
            userService.getUserDetailTypes()
                .then(function(response) {
                    $scope.options = response.data;
                    $scope.selectedOption = $scope.options[0];
                });
        }

        getDetailTypes();

        $scope.addUserDetail = function() {
            if ($scope.selectedOption.Id !== 1) {
                window.setTimeout(function() {
                    $("#file").click();
                }, 10);
                return;
            }
            var detail = {
                Name: $scope.detailTitle,
                Value: $scope.detailValue,
                UserId: userId,
                DetailTypeId: $scope.selectedOption.Id
            }
            userService.addUserDetail(detail)
                .then(function () {
                    $scope.ok();
                });
        }

        $scope.cancel = function() {
            $modalInstance.dismiss("cancel");
        };

        $scope.ok = function() {
            $modalInstance.close();
        };

        /* File Upload */
        $scope.updateUploader = function(typeId) {
            $scope.uploadOption = {
                url: $$ServiceUrl + "/UserDetail?userId=" + userId + "&typeId=" + typeId
            }
            
            if (typeId === 3) {
                $scope.uploader.filters.push({
                    name: 'imageFilter',
                    fn: function(item /*{File|FileLikeObject}*/, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                    }
                });
            }
        }

        $scope.uploader = new FileUploader({
            headers: {
                "Authorization": "Bearer " + authService.token()
            }
        });
        $scope.uploader.autoUpload = true;
        $scope.uploader.autoRemove = true;
        $scope.uploader.removeAfterUpload = true;
        $scope.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            $scope.uploaderError = true;
        };
        $scope.uploader.onAfterAddingFile = function(item /*{File|FileLikeObject}*/, filter, options) {
            $scope.uploaderError = false;
        };
        $scope.uploader.onCompleteAll = function (item /*{File|FileLikeObject}*/, filter, options) {
            $scope.ok();
        };
    };
    scrumBoardApp.controller("modalAddUserDetailController", ["$scope", "$routeParams", "$modalInstance", "userService", "FileUploader", "authService", modalAddUserDetailController]);
})();