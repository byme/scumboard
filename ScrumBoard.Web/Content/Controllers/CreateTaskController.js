﻿(function() {
    var createTaskController = function ($scope, $routeParams, $location, $window, taskService) {
        $scope.task =
        {
            Name: "",
            Description: "",
            ParentId: $routeParams.parentId,
            OriginalEstimate: 0,
            ProjectId: $routeParams.projectId,
            Status: { Id: 1 },
            Type: { Id: 0, Type: "" }
        };

        if ($routeParams.parentId) {
            $scope.isFeature = false;
        } else {
            $scope.isFeature = true;
            $scope.task.Type.Id = 1;
        }

        taskService.getTaskTypes().then(function (response) {
            $scope.types = response.data;
            if (!$scope.isFeature) {
                $scope.types = jQuery.grep($scope.types, function (n, i) {
                    return (n.Type !== "Feature");
                });
                $scope.task.Type = $scope.types[0];
            }
        });

        $scope.createTask = function() {
            taskService.post($scope.task).success(function () {
                if (!$routeParams.parentId) {
                    $location.path("/Project/" + $routeParams.projectId + "/Backlog");
                } else {
                    $location.path("/Project/" + $routeParams.projectId + "/Task/" + $routeParams.parentId + "/Subtasks");
                }
            });
        };

        $scope.cancel = function() {
            $window.history.back();
        };
    };
    scrumBoardApp.controller("createTaskController", ["$scope", "$routeParams", "$location", "$window", "taskService", createTaskController]);
})();