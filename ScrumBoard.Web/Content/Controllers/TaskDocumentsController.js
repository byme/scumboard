﻿(function() {
    var taskDocumentsController = function ($scope, $routeParams, $modal, documentService, fileUploader, authService) {

        $scope.searchInput = "";
        var projectId = $routeParams.taskId;

        $scope.uploader = new fileUploader({
            url: $$ServiceUrl + "/Document?ParentId=" + projectId + "&DocumentType=Task",
            headers: {
                "Authorization": "Bearer " + authService.token()
            },
        });

        $scope.uploader.formData = { ParentId: projectId, DocumentType: "Task" };
        $scope.uploader.autoUpload = true;
        $scope.uploader.autoRemove = true;
        $scope.documents = [];

        $scope.uploader.onSuccessItem = function() {
            documentService.getByTask(projectId).success(function(response) {
                $scope.documentPreview = response;
            });
        };

        $scope.search = function (arg) {
            arg = arg.toLowerCase();
            documentService.getByTask(projectId).then(function (responce) {
                $scope.documentPreview = [];
                var data = responce.data;
                if (arg == "") {
                    $scope.documentPreview = data;
                    return;
                }
                for (var i in data) {
                    if (data.hasOwnProperty(i)) {
                        if (data[i].Name.indexOf(arg) > -1) {
                            $scope.documentPreview.push(data[i]);
                        }
                    }
                }
            });
        };

        var getDocuments = function(projectId) {
            documentService.getByTask(projectId).then(function(responce) {
                $scope.documentPreview = responce.data;
            });
        }
        getDocuments(projectId);

        $scope.addDocument = function() {
            $("#file").click();
        };

        $scope.downloadFile = function myfunction(documentId) {
            documentService.getProjectDocument(documentId);
        };

        $scope.animationsEnabled = true;
        $scope.deleteDocument = function(doc) {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: "/Content/Views/Modals/DeleteDocument.html",
                controller: "modalDeleteController",
                resolve: {
                    data: function() {
                        return doc;
                    }
                }

            });

            modalInstance.result.then(function(data) {
                documentService.deleteDocument(doc).success(function() {
                    getDocuments(projectId);
                });
            });
        };

    };
    scrumBoardApp.controller("taskDocumentsController", ["$scope", "$routeParams", "$modal", "documentService", "FileUploader", "authService", taskDocumentsController]);
})();