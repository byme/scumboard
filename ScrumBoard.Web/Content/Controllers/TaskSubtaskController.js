﻿(function() {
    var taskSubtaskController = function($scope, $location, $routeParams, taskService, $rootScope) {
        $scope.taskId = $routeParams.taskId;

        var getTasks = function(taskId) {
            taskService.getTaskByParent(taskId).then(function(responce) {
                $scope.tasksPreview = responce.data;
            });
        }

        var getTask = function (taskId) {
            taskService.getTask(taskId)
                .then(function (response) {
                    $scope.task = response.data;
                });
        }

        $scope.searchInput = "";

        $scope.search = function(arg) {
            arg = arg.toLowerCase();
            taskService.getTaskByParent($routeParams.taskId).then(function (responce) {
                $scope.tasksPreview = [];
                var data = responce.data;
                if (arg == "") {
                    $scope.tasksPreview = data;
                    return;
                }
                for (var i in data) {
                    if (data[i].Name.toLowerCase().indexOf(arg) > -1 || arg == data[i].Id) {
                        $scope.tasksPreview.push(data[i]);
                    }
                }
            });
        };

        $scope.addSubtask = function() {
            $location.path("/Task/Create/" + $scope.task.ProjectId + "/" + $scope.taskId);
        }

        $scope.goToTask = function(taskId) {
            $location.path("/Project/" + $scope.task.ProjectId + "/Task/" + taskId + "/Info");
        }

        $scope.deleteTask = function(taskId) {
            taskService.deleteTask(taskId).then(function() {
                getTasks($scope.taskId);
            });
        }

        getTask($scope.taskId);
        getTasks($scope.taskId);

    };
    scrumBoardApp.controller("taskSubtaskController", ["$scope", "$location", "$routeParams", "taskService", "$rootScope", taskSubtaskController]);
})();