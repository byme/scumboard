﻿(function() {
    var userController = function ($scope, $location, $routeParams, userService, $modal, FileUploader, authService) {
        var userId = $routeParams.userId;
        $scope.detailTitle = "";
        $scope.detailValue = "";
        $scope.showPhotoRow = false;
        $scope.showDocumentRow = false;
        var tempUser;

        var getUser = function() {
            userService.getUserById(userId)
                .then(function(response) {
                    $scope.user = response.data;
                    tempUser = $.extend(true, {}, response.data);
                    $scope.showPhotoRow = false;
                    $scope.showDocumentRow = false;
                    if ($scope.user.Details) {
                        for (var i = 0; i < $scope.user.Details.length; i++) {
                            if ($scope.user.Details[i].DetailType.Type === "Photo") {
                                $scope.showPhotoRow = true;
                            }
                            if ($scope.user.Details[i].DetailType.Type === "Document") {
                                $scope.showDocumentRow = true;
                            }
                            if ($scope.showDocumentRow && $scope.showPhotoRow) break;
                        }
                    }
                });
        }

        var checkEditProfilePermission = function () {
            var currentUser = userService.currentUser();
            if (currentUser.Id == userId || currentUser.IsAdmin) {
                $scope.canEdit = true;
            } else {
                $scope.canEdit = false;
            }
        }

        getUser();
        checkEditProfilePermission();

        $scope.updateUser = function () {
            userService.updateUser($scope.user)
                .then(function() {
                    userService.updateUserDetails($scope.user.Details)
                        .then(function() {
                            getUser();
                            $scope.switchEditMode();
                            $scope.detailTitle = "";
                            $scope.detailValue = "";
                        });
                });
        }

        $scope.deleteDetail = function(id) {
            userService.deleteDetail(id)
                .then(function() {
                    getUser();
                });
        }

        $scope.toChangePassword = function () {
            $location.path("/User/ChangePassword");
        };

        $scope.downloadFile = function(documentId) {
            userService.getUserDocument(documentId);
        };

        $scope.filterTextInfo = function(detail) {
            if (detail.DetailType.Type === "Text")
                return detail;
        }

        $scope.filterPhotoInfo = function(detail) {
            if (detail.DetailType.Type === "Photo")
                return detail;
        }

        $scope.filterDocumentInfo = function(detail) {
            if (detail.DetailType.Type === "Document")
                return detail;
        }


        // Edit Mode Functions
        $scope.editMode = false;

        

        var editModeOn = function () {
                $scope.editMode = !$scope.editMode;
            
        }

        var editModeOff = function() {
            $scope.editMode = !$scope.editMode;
            $scope.user = $.extend(true, {}, tempUser);
        }

        $scope.switchEditMode = function() {
            if ($scope.editMode)
                editModeOff();
            else
                editModeOn();
        }

        // Add detail
        $scope.animationsEnabled = true;
        $scope.addDetail = function() {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: "/Content/Views/Modals/AddUserDetail.html",
                controller: "modalAddUserDetailController"
            });

            modalInstance.result.then(function() {
                getUser();
            });
        };

        // File Uploader
        $scope.photoUploader = new FileUploader({
            url: $$ServiceUrl + "/user/" + userId + "/document",
            headers: {
                "Authorization": "Bearer " + authService.token()
            }
        });
        $scope.photoUploader.autoUpload = true;
        $scope.photoUploader.autoRemove = true;
        $scope.photoUploader.removeAfterUpload = true;
        $scope.photoUploader.filters.push({
            name: 'imageFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });
        $scope.photoUploader.onCompleteAll = function (item /*{File|FileLikeObject}*/, filter, options) {
            getUser();
        };

        $scope.uploadPhoto = function() {
            window.setTimeout(function() {
                $("#photo").click();
            }, 10);
        };

        // Image styles switch
        $(function () {
            $("#image").children().hover(function () {
                if (!$scope.editMode) return;
                $("#upload-image").show();
            },
            function () {
                if (!$scope.editMode) return;
                $("#upload-image").hide();
            });
            $("#upload-image").hover(function () {
                $(this).prev().addClass("image-hover");
            },
            function () {
                $(this).prev().removeClass("image-hover");
            });
        });
    };
    scrumBoardApp.controller("userController", ["$scope", "$location", "$routeParams", "userService", "$modal", "FileUploader", "authService", userController]);
})();