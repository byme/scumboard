﻿(function () {
    var createSprintController = function ($scope, $location, $routeParams, $window, taskService, sprintService) {
        $scope.startDate = "";
        $scope.endDate = "";

        $scope.createSprint = function () {
            var sprint =
            {
                Name: $scope.name,
                startDate: new Date($scope.startDate),
                endDate: new Date($scope.endDate),
                ProjectId: $routeParams.projectId,
                StatusId: 3
            }

            sprintService.post(sprint).success(function () {
                $location.path("/Project/" + $routeParams.projectId + "/Sprints");
            });
        };

        $scope.cancel = function() {
            $window.history.back();
        };

        $scope.dateEquales = function() {
            if ($scope.startDate != $scope.endDate)
                return true;
            else {
                return false;
            }
        };

    };
    scrumBoardApp.controller("createSprintController", ["$scope", "$location","$routeParams", "$window", "taskService", "sprintService", createSprintController]);
})();