﻿(function() {
    var adminUserController = function($scope, $location, userService) {

        $scope.searchInput = "";

        var getUsers = function() {
            userService.getAll().then(function(responce) {
                $scope.users = responce.data;
            });
        }

        $scope.search = function (searchInput) {
            searchInput = searchInput.toLowerCase();
            userService.getAll().then(function(responce) {
                $scope.users = [];

                if (searchInput == "") {
                    $scope.users = responce.data;
                    return;
                }

                for (var i in responce.data) {
                    var name = responce.data[i].FirstName + " " + responce.data[i].LastName;
                    if (name.indexOf(searchInput) > -1 || responce.data[i].Email.indexOf(searchInput) > -1) {
                        $scope.users.push(responce.data[i]);
                    }
                }
            });
        }

        $scope.goToUser = function(userId) {
            $location.path("/User/" + userId);
        }

        getUsers();

    }

    scrumBoardApp.controller("adminUserController", ["$scope", "$location", "userService", adminUserController]);

})();