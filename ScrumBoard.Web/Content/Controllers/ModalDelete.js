﻿(function() {
    var modalDeleteController = function($scope, $location, $modalInstance, data) {
        $scope.data = data;

        $scope.cancel = function() {
            $modalInstance.dismiss("cancel");
        };
        $scope.ok = function() {
            $modalInstance.close();
        };
    };
    scrumBoardApp.controller("modalDeleteController",
    ["$scope", "$location", "$modalInstance", "data", modalDeleteController]);
})();