﻿(function() {
    var taskController = function($scope, $routeParams, $location, taskService, $rootScope) {

        var pageName = $routeParams.pageName;
        var url = "/Project/" + $routeParams.projectId + "/Task/" + $routeParams.taskId + "/";

        $scope.pages =
        [
            { name: "Info", url: "/Content/Views/TaskInfo.html" },
            { name: "Subtasks", url: "/Content/Views/TaskSubtasks.html" },
            { name: "Documents", url: "/Content/Views/TaskDocuments.html" },
            { name: "Comments", url: "/Content/Views/TaskComments.html" }
        ];

        var initPage = function () {
            for (var i = 0; i < $scope.pages.length; i++) {
                if ($scope.pages[i].name.toUpperCase() == pageName.toUpperCase()) {
                    $scope.currentPage = $scope.pages[i];
                    return;
                }
            }
        }
        initPage();

        $scope.setCurrentPage = function (page) {
            $location.path(url + page.name);
        };

        $scope.isSelected = function (page) {
            return $scope.currentPage === page;
        };

        var getTask = function(taskId) {
            taskService.getTask( taskId).then(function (responce) {
                $scope.task = responce.data;
                $rootScope.projectId = responce.data.ProjectId;
            });
        }
        getTask($routeParams.taskId);

        $scope.backToProject = function() {
            $location.path("/Project/" + $routeParams.projectId + "/Backlog");
        }
    };

    scrumBoardApp.controller("taskController", ["$scope", "$routeParams", "$location", "taskService", "$rootScope", taskController]);
})();