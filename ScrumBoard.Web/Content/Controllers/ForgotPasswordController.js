﻿(function() {
    var forgotPasswordController = function($scope, $http,$location) {
        var $$ServiceUrl = 'http://localhost:54935/api/Account/ForgotPassword';
        $scope.sendLetter = function() {
            var user = {
                Email: $scope.email
            };
            $http.post($$ServiceUrl, user).success(function() {
                $location.path("/Login");
            });
        }
    };
    scrumBoardApp.controller("forgotPasswordController", ["$scope", "$http","$location", forgotPasswordController]);
})();