﻿(function() {
    var taskCommentsController = function($scope, $location, $routeParams, commentService, userService) {
        var userId = userService.currentUser().Id;

        $scope.text = "";

        var getComments = function(taskId) {
            commentService.getComments(taskId).then(function(responce) {
                $scope.comments = responce.data;
            });
        }

        $scope.addComment = function() {
            if ($scope.text != "") {
                var newComment = {
                    Text: $scope.text,
                    Time: new Date(),
                    TaskId: $routeParams.taskId,
                    UserId: userId,
                    UserName: ""
                };
                commentService.addComment(newComment).success(function() {
                    getComments($routeParams.taskId);
                    $scope.text = "";

                });
            }
        }

        $scope.goToUser = function (id) {
            if (!id) return;
            $location.path("/User/" + id);
        }

        getComments($routeParams.taskId);
    }

    scrumBoardApp.controller("taskCommentsController", ["$scope", "$location", "$routeParams", "commentService", "userService", taskCommentsController]);
})();