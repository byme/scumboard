﻿(function() {
    var headerCtrl = function ($scope, $location, $translate, $cookies, projectService, userService) {

        var checkUserRole = function () {
            var user = userService.currentUser();
            if (user) {
                $scope.isAdminPage = user.IsAdmin;
            } else {
                $scope.isAdminPage = false;
            }
        };

        checkUserRole();

        $scope.setCurrentPage = function(page) {
            $scope.currentPage = page;
            if (page == 0) $scope.toHome();
            if (page == 1) $scope.toAbout();
            if (page == 2) $scope.toManageUsersPage();
        }

        $scope.isSelected = function(page) {
            return $scope.currentPage === page;
        }
        $scope.toManageUsersPage = function () {
            $location.path("/ManageUsers");
        }
        $scope.toProjectList = function() {
            $location.path("/ProjectList");
        };
        $scope.toAbout = function() {
            $location.path("/About");
        };
        var projects = [];
        $scope.goToBoard = function () {
            user = userService.currentUser();
            projectService.getByTeammate(user.Id)
                .then(function(responce) {
                    projects = responce.data;
                    if (projects.length !== 1) {
                        $location.path("/ProjectList");
                    } else {
                        $scope.toBoard(projects[0].Id);
                    }
                });

        };

        $scope.$on("$routeChangeStart", function (event, next, current) {
            checkUserRole();
        });

        var languages = [
            "en",
            "ru"
        ];

        var selectedLang = "en";

        var initLang = function() {
            if ($cookies.get("lang")) {
                selectedLang = $cookies.get("lang");
            }
            $translate.use(selectedLang);
            $cookies.put("lang", selectedLang);
        }

        initLang();

        $scope.setLang = function(lang) {
            selectedLang = languages[lang];
            $translate.use(selectedLang);
            $cookies.put("lang", selectedLang);
        }

        $scope.isSelectedLang = function(lang) {
            return selectedLang == languages[lang];
        }
    };
    scrumBoardApp.controller("headerCtrl", ["$scope", "$location", "$translate", "$cookies", "projectService", "userService", headerCtrl]);
})();