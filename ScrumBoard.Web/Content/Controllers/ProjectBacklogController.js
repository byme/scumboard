﻿(function() {
    var projectBacklogController = function($scope, $location, $routeParams, $rootScope, taskService) {

        var getTasks = function(projectId) {
            taskService.getTaskByProject(projectId).then(function(responce) {
                $scope.tasksPreview = responce.data;
            });
        }

        $scope.searchInput = "";

        $scope.search = function(arg) {
            arg = arg.toLowerCase();
            taskService.getTaskByProject($routeParams.projectId).then(function (responce) {
                $scope.tasksPreview = [];
                var data = responce.data;
                if (arg == "") {
                    $scope.tasksPreview = data;
                    return;
                }
                for (var i in data) {
                    if (data[i].Name.toLowerCase().indexOf(arg) > -1 || arg == data[i].Id) {
                        $scope.tasksPreview.push(data[i]);
                    }
                }
            });
        };

        $scope.addFeature = function() {
            $location.path("/Project/" + $routeParams.projectId + "/Task/Create");
        }

        $scope.goToTask = function(taskId) {
            $location.path("/Project/" + $routeParams.projectId + "/Task/" + taskId + "/Info");
        }

        $scope.deleteTask = function(taskId) {
            taskService.deleteTask(taskId).then(function() {
                getTasks($routeParams.projectId);
            });
        }

        getTasks($routeParams.projectId);

    };
    scrumBoardApp.controller("projectBacklogController", ["$scope", "$location", "$routeParams", "$rootScope", "taskService", projectBacklogController]);
})();