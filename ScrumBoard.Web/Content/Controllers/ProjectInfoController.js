﻿(function() {
    var projectInfoController = function ($scope, $location, $routeParams, $modal, projectService, userService) {

        var projectId = $routeParams.projectId;
        $scope.user = userService.currentUser();

        var tempDescription;

        $scope.$on("projectData", function(event, data) {
            $scope.project = data;
            tempDescription = $scope.project.Description;
        });
        $scope.EditMode = false;
        $scope.EditModeOn = function() {
            $scope.EditMode = true;
            tempDescription = $scope.project.Description;
        }

        var editModeOff = function() {
            $scope.EditMode = false;
        }

        $scope.Cancel = function() {
            editModeOff();
            $scope.project.Description = tempDescription;
        }

        $scope.Delete = function() {

        }

        $scope.SaveChanges = function() {
            projectService.put($scope.project);
            editModeOff();
        }

        $scope.animationsEnabled = true;
        $scope.openModal = function(size) {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: "/Content/Views/Modals/ModalConfirmProject.html",
                controller: "modalConfirmProjectController",
                size: size,
                resolve: {
                    data: function () {
                        var data = 
                            {
                                project: $scope.project,
                                title: 'DELETING_PROJECT',
                                message: 'CONFIRM_DELETING_PRJ_MESSAGE'
                        }
                        return data;
                    }
                }
            });

            modalInstance.result.then(function(data) {
                projectService.delete(data.project.Id);
            });
        };
    };
    scrumBoardApp.controller("projectInfoController", ["$scope", "$location", "$routeParams", "$modal", "projectService", "userService",projectInfoController]);
})();