﻿(function() {
    var createProjectController = function ($scope, $location, FileUploader, userService, projectService, $window) {

        var uploader = $scope.uploader = new FileUploader({
            url: 'http://localhost:55482/api/load'
        });

        $scope.name = "";
        $scope.description = "";

        $scope.userPreview = [];

        $scope.searchUserByEmail = function(emailPart) {
            if (emailPart == "") {
                $scope.userPreview = [];
                return;
            }

            $scope.userPreview = userService.searchUser(emailPart);
        };

        $scope.createProject = function() {
            projectService.post($scope.name, $scope.description).success(function() {
                $location.path("/ProjectList");
            });
        };

        $scope.cancel = function() {
            $window.history.back();
        };

    };
    scrumBoardApp.controller("CreateProjectController", ["$scope", "$location", "FileUploader", "userService", "projectService", "$window", createProjectController]);
})();

scrumBoardApp.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter, { 'event': event });
                });

                event.preventDefault();
            }
        });
    };
});