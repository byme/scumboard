﻿(function() {
    var loginWidgetController = function($scope, $rootScope, $location, userService, authService) {
        var user = {};

        $rootScope.$on("$routeChangeStart", function(event, next, current) {
            user = userService.currentUser();
            if (user) {
                $scope.userName = user.Email;
                $scope.isAuth = $rootScope.isAuth;
            }
        });

        $scope.login = function() {
            $location.path("/Login");
        };

        $scope.register = function() {
            $location.path("/Registration");
        };

        $scope.profile = function() {
            $location.path("/User/" + user.Id).hash();
        };

        $scope.logOut = function() {
            authService.logOut();
            $rootScope.isAuth = false;
            $scope.isAuth = false;
            $location.path("/");
        };

    };

    scrumBoardApp.controller("loginWidgetController", ["$scope", "$rootScope", "$location", "userService", "authService", loginWidgetController]);
})();