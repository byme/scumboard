﻿(function () {
    var changePasswordController = function ($scope, $http, $location) {

        $scope.change = function () {
            var user = {
                OldPassword: $scope.oldPassword,
                NewPassword: $scope.password,
                ConfirmPassword: $scope.confirmPassword
            };
            $http.post($$ServiceUrl + '/Account/ChangePassword', user).success(function() {
                $location.back();
            });
        };
        $scope.passwordEquales = function () {
            if ($scope.password === $scope.confirmPassword)
                return true;
            else {
                return false;
            }
        };
    };
    scrumBoardApp.controller("сhangePasswordController", ["$scope", "$http", "$location", changePasswordController]);
})();