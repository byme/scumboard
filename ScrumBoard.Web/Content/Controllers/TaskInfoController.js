﻿(function() {
    var taskInfoController = function ($scope, $location, $routeParams, $filter,taskService, sprintService, teamService, userService) {

        var taskId = $routeParams.taskId;
        var notSet = { Id: null, Name: "Not set", Role: "-" };
        var tempStatus;
        var tempSprint = $.extend(true, {}, notSet);
        var tempTeammate = $.extend(true, {}, notSet);
        $scope.showTeammateSearch = false;
        $scope.showSprintSearch = false;
        $scope.sprintPreview = [];
        $scope.teammatePreview = [];

        var getStatuses = function() {
            taskService.getTaskStatuses()
                .then(function(response) {
                    $scope.statuses = response.data;
                });
        }
        var getTask = function() {
            taskService.getTask(taskId)
                .then(function(response) {
                    $scope.task = response.data;
                    tempStatus = $scope.task.Status;
                    if ($scope.task.Sprint !== null) {
                        tempSprint = $scope.task.Sprint;
                    } else {
                        $scope.task.Sprint = $.extend(true, {}, notSet);
                    }
                    if ($scope.task.Teammate !== null) {
                        tempTeammate = $scope.task.Teammate;
                    } else {
                        $scope.task.Teammate = $.extend(true, {}, notSet);
                    }
                });
        }

        getTask();
        getStatuses();


        $scope.searchSprint = function (sprintName) {
            if (sprintName == null) {
                $scope.sprintPreview = [];
                $scope.sprintPreview.push($.extend(true, {}, notSet));
                return;
            }
            sprintName = sprintName.toLowerCase();
            sprintService.getSprintsByProject($scope.task.ProjectId)
                .then(function (response) {
                    $scope.sprintPreview = [];
                    for (var i = 0; i < response.data.length; i++) {
                        var name = response.data[i].Name.toLowerCase();
                        if (name.indexOf(sprintName) > -1) {
                            $scope.sprintPreview.push(response.data[i]);
                        }
                    }
                    $scope.sprintPreview.push($.extend(true, {}, notSet));
                    if ($scope.sprintPreview.length > 0) {
                        $scope.showSprintSearch = true;
                    }
                });
        }

        $scope.searchTeammate = function (teammateName) {
            if (teammateName == null) {
            $scope.teammatePreview = [];
                $scope.sprintPreview.push($.extend(true, {}, notSet));
                return;
            }
            teammateName = teammateName.toLowerCase();
            teamService.getTeammatesByProject($scope.task.ProjectId)
                .then(function (response) {
                    $scope.teammatePreview = [];
                    for (var i = 0; i < response.data.length; i++) {
                        var name = response.data[i].Name.toLowerCase();
                        teammateName = teammateName.toLowerCase();
                        if (name.indexOf(teammateName) > -1) {
                            $scope.teammatePreview.push(response.data[i]);
                        }
                    }
                    $scope.teammatePreview.push($.extend(true, {}, notSet));
                    if ($scope.teammatePreview.length > 0) {
                        $scope.showTeammateSearch = true;
                    }
                });
        }

        $scope.focusOut = function() {
            $scope.showTeammateSearch = false;
            $scope.showSprintSearch = false;
        }

        $scope.focusOnSprint = function() {
            $scope.showSprintSearch = true;
        }

        $scope.focusOnTeammate = function() {
            $scope.showTeammateSearch = true;
        }

        $scope.chooseSprint = function(id) {
            for (var i = 0; i < $scope.sprintPreview.length; i++) {
                if ($scope.sprintPreview[i].Id !== id) continue;
                $scope.task.Sprint = $scope.sprintPreview[i];
                tempSprint = $.extend(true, {}, $scope.sprintPreview[i]);
                return;
            }
        }

        $scope.chooseTeammate = function(id) {
            for (var i = 0; i < $scope.teammatePreview.length; i++) {
                if ($scope.teammatePreview[i].Id !== id) continue;
                $scope.task.Teammate = $scope.teammatePreview[i];
                tempTeammate = $.extend(true, {}, $scope.teammatePreview[i]);
                return;
            }
        }

        $scope.updateTask = function() {
            taskService.updateTask($scope.task)
                .then(function() {
                    getTask(taskId);
                    $scope.editMode = false;
                });
        }

        $scope.assignTask = function () {
            teamService.getTeammatesByProject($scope.task.ProjectId)
            .then(function (response) {
                $scope.task.Teammate = $filter('filter')(response.data, userService.currentUser().Id)[0];
                $scope.updateTask($scope.task);
            });
        }

        $scope.editMode = false;

        var editModeOn = function() {
            $scope.editMode = !$scope.editMode;
        }

        var editModeOff = function() {
            $scope.sprintPreview = [];
            $scope.teammatePreview = [];
            $scope.task.Sprint = tempSprint;
            $scope.task.Status = tempStatus;
            $scope.task.Teammate = tempTeammate;
            $scope.editMode = !$scope.editMode;
        }

        $scope.switchEditMode = function() {
            if ($scope.editMode)
                editModeOff();
            else
                editModeOn();
        }
    };
    scrumBoardApp.controller("taskInfoController", ["$scope", "$location", "$routeParams", "$filter", "taskService", "sprintService", "teamService", "userService", taskInfoController]);
})();