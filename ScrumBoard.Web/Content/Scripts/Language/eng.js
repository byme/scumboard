﻿
scrumBoardApp.config([
    "$translateProvider", function($translateProvider) {
        $translateProvider.translations("en", {
            "CREATE_PROJECT": "Create Project",
            "NAME": "Name",
            "DESCRIPTION": "Description",
            "INPUT_PROJECT_NAME": "Input project name",
            "INPUT_PROJECT_DESCRIPTION": "Input project descrption",
            "CREATE": "Create",
            "CANCEL": "Cancel",

            //project list page
            "YOUR_PROJECTS": "Your project",
            "ADD_PROJECT": "Add project",
            "PROJECT_NAME_SEARCH": "Search project",

            //Create Sprint
            "CREATE_SPRINT": "Create Sprint",
            "START_DATE": "Start date",
            "END_DATE": "End date",
            "SPRINT_RANGE": "Duration",
            "TASK_SEARCH": "Task Search",
            "INPUT_NAME": "Enter name",
            "INPUT_TASK_NAME": "Enter task name",
            "NAME_IS_REQUIRED": "Name is required",
            "ENTER_A_VALID_NAME": "Enter a valid name",
            "INPUT_ENTER_DATE": "Enter date",
            "TASKS": "Selected tasks",
            "INPUT_ENTER_START_DATE": "Enter start date",
            "INPUT_ENTER_END_DATE": "Enter end date",
            "TO": " to ",

            //404
            "ERROR": "Oops... something go wrong. Please try again.",
            "GO_HOME": "Go home",

            //Board
            "TEAMMATE": "Teammate",
            "ALL": "--All--",
            "TO_DO": "To do",
            "IN_PROGRESS": "In progress",
            "TESTING": "Testing",
            "DONE": "Done",
            "NOT_ASSIGNED": "Not assigned",
            "WITHOUT_SPRINT": "Without sprint",

            //Project backlog
            "TASK_NAME": "Task name",
            "TASK_TYPE": "Task type",
            "TASK_STATUS": "Task status",
            "ORIGINAL_ESTIMATE": "Original estimate",
            "REMAINING": "Remaining",
            "COMPLETED": "Completed",
            "ASSIGNED_TO": "Assigned to",
            "ADD_FEATURE": "Add feature",
            "TASK_NAME_OR_ID_SEARCH": "Task name or id",

            //Subtasks page
            "ADD_SUBTASK": "Add subtask",

            //Project documents
            "DOCUMENT_NAME_SEARCH": "Document name",
            "ADD_DOCUMENT": "Add document",
            "SIZE": "Size",

            //Project sprints
            "SPRINT_NAME_SEARCH": "Sprint name",
            "ADD_SPRINT": "Add sprint",
            "SPRINT_STATUS": "Sprint status",

            //Project team
            "MEMBER_NAME_SEARCH": "Member name",
            "ADD_MEMBER": "Add members",
            "MEMBER_NAME": "Member name",
            "ROLE": "Role",

            //Add teammate modal
            "ADD_TEAMMATE": "Add teammates",
            "TEAMMATE_EMAIL_SEARCH": "Enter teammate email",
            "USERS": "Users:",
            "OK": "OK",

            //Admin user
            "USER_NAME_OR_EMAIL_SEARCH": "User name or email",
            "PHOTO": "Photo",

            //Task comments
            "ENTER_COMMENT": "Enter your comment",
            "SEND": "Send",

            //Delete document modal
            "DELETE_PROMT": "Delete Promt",
            "CONFIRM_DELETING_DOC_MESSAGE": "Do you want delete the document ",

            //Confirm project modal
            "DELETING_PROJECT": "Deleting project",
            "CONFIRM_DELETING_PRJ_MESSAGE": "Do you want delete the project ",
            "RESTORING_PROJECT": "Restoring project",
            "CONFIRM_RESTORE_PRJ_MESSAGE": "Do you want restore the project ",

            //About
            "ABOUT_TITLE": "About ScrumBoard",
            "SCRUM_BOARD_TEAM": "ScrumBoard Team",
            "TEAM_DESCRIPTION": "The developers, who participated in the creation of the ScrumBoard project.",
            "ARTEM_KALIKIN": "Artem Kalikin",
            "KATE_LETAVSKA": "Katerina Letavska",
            "VLAD_YURYEV": "Vlad Yuryev",
            "DMITRY_DEREPOVSKY": "Dmitry Derepovsky",
            "DMITRY_ZEMLJAK": "Dmitry Zemljak",
            "STAS_SILIN": "Stas Silin",

            //Create Task
            "HOURS_PER_TASK": "Original estimate",
            "ENTER_TASK_DESCRIPTION": "Enter task description",
            "ENTER_NAME": "Enter name",
            "CREATE_TASK": "Create Task",
            "ENTER_TIME": "Enter time",
            "SELECT_TASK_TYPE": "Select task type",
            "TIME_IS_REQUIRED": "Time is required",
            "ENTER_A_VALID_TIME": "Enter a valid time",

            //Forgot password & Login & Registration
            "LOGIN": "Login",
            "FORGOT_PASSWORD": "Forgot Password",
            "ENTER_EMAIL_INVALID": "Enter a valid email.",
            "EMAIL": "Email",
            "PASSWORD": "Password",
            "CHANGE_PASSWORD": "Change password",
            "OLD_PASSWORD": "Old password",

            //Project master page
            "INFO": "Info",
            "TEAM": "Team",
            "BACKLOG": "Backlog",
            "SPRINTS": "Sprints",
            "DOCUMENTS": "Documents",
            "BOARD": "Board",

            //Task master page
            "SUBTASKS": "Subtasks",
            "COMMENTS": "Comments",
            "TO_BACKLOG": "Back to backlog",

            //User page
            "LAST_NAME": "Last name",
            "ADDING_FILE_MESSAGE": "Please select a file to add to your profile:",
            "TITLE": "Title",
            "ADDITIONAL_INFO": "Additional info",
            "ALLOWED_FORMATS": "You can add only images of following types: jpg, png, jpeg, bmp, gif.",
            "INPUT_LAST_NAME": "Enter last name",
            "INPUT_FIRST_NAME": "Enter first name",
            "INPUT_PHONE": "Enter phone",
            "INPUT_TITLE": "Enter detail title",
            "INPUT_VALUE": "Enter detail value",
            "ENTER_INCORRECT_LAST_NAME": "Last name field cannot be empty or longer than 40 symbols.",
            "ENTER_INCORRECT_FIRST_NAME": "First name field cannot be empty or longer than 40 symbols.",
            "ENTER_INCORRECT_PHONE": "Phone field cannot be empty or longer than 20 symbols.",
            "ENTER_INCORRECT_TITLE": "Detail title field cannot be empty or longer than 20 symbols.",
            "ENTER_INCORRECT_VALUE": "Detail value field cannot be empty.",
            "ADD_DETAIL": "Add user detail",
            "ADD_DETAIL_TITLE": "Add User Detail",
            "ADD_DOCUMENT_TITLE": "Please click 'Add' button to attach a document.",
            "ADD_DOCUMENT_PHOTO": "Please click 'Add' button to attach a photo.",
            "ADD": "Add",


            //Master page
            "PROJECTS": "Projects",
            "ABOUT": "About",
            "LANGUAGE": "Language",
            "MANAGE_USERS": "Manage users",
            "MANAGE_PROJECTS": "Manage projects",
            "MANAGEMENT": "Management",

            "ENTER_PASSWORD_INVALID": "Enter a correct password.",
            "REMEMBER_ME": "Remember me",
            "SIGN_IN": "Sign in",
            //Registration
            "REGISTRATION": "Registration",
            "REGISTER": "Register",
            "PASSWORDS_MUST_MATCH": "Passwords must match.",
            "CONFIRM_PASSWORD": "Confirm password",
            "ENTER_CORRECT_PASSWORD": "Enter a correct password.",
            "FIRST_NAME": "First name",
            "NAME_IS_REQUIRED_REG": "First name is required.",
            "SECOND_NAME": "Second name",
            "SECONDNAME_IS_REQUIRED": "Second name is required.",
            "PHONE": "Phone",
            "PHONE_INCORRECT": "Phone is required.",
            "ENTER_VALID_EMAIL": "Enter a valid email.",
            //TaskInfo
            "TYPE": "Type",
            "STATUS": "Status",
            "SPRINT": "Sprint",
            "ENTER_INCORRECT_SPRINT": "Start enter sprint name and choose one from the list.",
            "ENTER_INCORRECT_TEAMMATE": "Start enter teammate name and choose one from the list.",
            "SAVE_CHANGES": "Save changes",
            "EDIT": "Edit",
            "ASSIGN_TO_ME": "Assign to me",
            "TO_BOARD": "To board",

            //ProjectInfo
            "DELETE": "Delete",

            //Main Page
            "MAIN_PAGE_CARUSEL_ITEM_HEADER_1": "Welcome!",
            "MAIN_PAGE_CARUSEL_ITEM_BODY_1": "Make your work easier with ScrumBoard",
            "MAIN_PAGE_CARUSEL_ITEM_BUTTON_1": "",

            "MAIN_PAGE_CARUSEL_ITEM_HEADER_2": "Get started",
            "MAIN_PAGE_CARUSEL_ITEM_BODY_2": "Register to start using ScrumBoard",
            "MAIN_PAGE_CARUSEL_ITEM_BUTTON_2": "Register",

            "MAIN_PAGE_CARUSEL_ITEM_HEADER_3": "About us",
            "MAIN_PAGE_CARUSEL_ITEM_BODY_3": "Learn more inforation about ScrumBoard team",
            "MAIN_PAGE_CARUSEL_ITEM_BUTTON_3": "Learn more",

            "LOGOUT": "Logout"
    });

        $translateProvider.preferredLanguage("en");
    }
]);