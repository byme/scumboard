﻿
scrumBoardApp.config([
    "$translateProvider", function($translateProvider) {
        $translateProvider.translations("ru", {
            "CREATE_PROJECT": "Создать проект",
            "NAME": "Имя",
            "DESCRIPTION": "Описание",
            "INPUT_PROJECT_NAME": "Введите имя проекта",
            "INPUT_PROJECT_DESCRIPTION": "Введите описание проекта",
            "CREATE": "Создать",
            "CANCEL": "Отмена",

            //project list page
            "YOUR_PROJECTS": "Ваши проекты",
            "ADD_PROJECT": "Добавить проект",
            "PROJECT_NAME_SEARCH": "Найти проект",

            //Create Sprint
            "CREATE_SPRINT": "Создать спринт",
            "START_DATE": "Дата начала",
            "END_DATE": "Дата окончания",
            "SPRINT_RANGE": "Длительность",
            "TASK_SEARCH": "Поиск задач",
            "INPUT_NAME": "Введите имя",
            "INPUT_TASK_NAME": "Введите имя задачи",
            "NAME_IS_REQUIRED": "Имя это обязательное поле",
            "ENTER_A_VALID_NAME": "Введите правильное имя",
            "INPUT_ENTER_DATE": "Введите дату",
            "TASKS": "Выбранные здачи",
            "INPUT_ENTER_START_DATE": "Введите дату начала",
            "INPUT_ENTER_END_DATE": "Введите дату окончания",
            "TO":" по ",

            //404
            "ERROR": "Ой... что-то пошло не так. Пожалуйста, попробуйте еще раз.",
            "GO_HOME": "На главную",

            //Board
            "TEAMMATE": "Разработчик",
            "ALL": "--Все--",
            "TO_DO": "Сделать",
            "IN_PROGRESS": "Выполняется",
            "TESTING": "Тестируется",
            "DONE": "Сделано",
            "NOT_ASSIGNED": "Не назначен",
            "WITHOUT_SPRINT": "Без спринта",

            //Project backlog
            "TASK_NAME": "Имя задачи",
            "TASK_TYPE": "Тип задачи",
            "TASK_STATUS": "Статус выполнения",
            "ORIGINAL_ESTIMATE": "Предварительная оценка",
            "REMAINING": "Осталось",
            "COMPLETED": "Завершён",
            "ASSIGNED_TO": "Назначен на",
            "ADD_FEATURE": "Добавить фичу",
            "TASK_NAME_OR_ID_SEARCH": "Имя задачи или id",

            //Subtasks page
            "ADD_SUBTASK": "Добавить подзадачу",

            //Project documents
            "DOCUMENT_NAME_SEARCH": "Имя документа",
            "ADD_DOCUMENT": "Добавить документ",
            "SIZE": "Размер",

            //Project sprints
            "SPRINT_NAME_SEARCH": "Имя спринта",
            "ADD_SPRINT": "Добавить спринт",
            "SPRINT_STATUS": "Статус спринта",

            //Project team
            "MEMBER_NAME_SEARCH": "Имя участника",
            "ADD_MEMBER": "Добавить участников",
            "MEMBER_NAME": "Имя участника",
            "ROLE": "Роль",

            //Add teammate modal
            "ADD_TEAMMATE": "Добавить участников",
            "TEAMMATE_EMAIL_SEARCH": "Введите email участника",
            "USERS": "Пользователи:",
            "OK": "OK",

            //Admin user
            "USER_NAME_OR_EMAIL_SEARCH": "Имя пользователя или email",
            "PHOTO": "Фото",

            //Task comments
            "ENTER_COMMENT": "Введите ваш комментарий",
            "SEND": "Отправить",

            //Delete document
            "DELETE_PROMT": "Подтверждение удаления",
            "CONFIRM_DELETING_DOC_MESSAGE": "Вы действительно хотите удалить документ ",

            //Confirm project modal
            "DELETING_PROJECT": "Удаление проекта",
            "CONFIRM_DELETING_PRJ_MESSAGE": "Вы действительно хотите удалить проект?",
            "RESTORING_PROJECT": "Восстановление проекта",
            "CONFIRM_RESTORE_PRJ_MESSAGE": "Вы действительно хотите восстановить проект?",

            //About
            "ABOUT_TITLE": "О проекте ScrumBoard",
            "SCRUM_BOARD_TEAM": "Команда",
            "TEAM_DESCRIPTION": "Разработчики, участвовавшие в создании проекта ScrumBoard.",
            "ARTEM_KALIKIN": "Артём Каликин",
            "KATE_LETAVSKA": "Екатерина Летавская",
            "VLAD_YURYEV": "Влад Юрьев",
            "DMITRY_DEREPOVSKY": "Дмитрий Дереповский",
            "DMITRY_ZEMLJAK": "Дмитрий Земляк",
            "STAS_SILIN": "Стас Силин",

            //Forgot password & Login

           
            "DESCRIPTION_IS_REQUIRED": "Введите имя задания",

            //Create Task
            "HOURS_PER_TASK": "Предварительная оценка",
            "ENTER_TASK_DESCRIPTION": "Введите описание",
            "ENTER_NAME": "Введите имя",
            "CREATE_TASK": "Создать задание",
            "ENTER_TIME": "Время не указано",
            "SELECT_TASK_TYPE": "Выберите тип задания",
            "TIME_IS_REQUIRED": "Время не указано",

            //Project master page
            "INFO": "Описание",
            "TEAM": "Команда",
            "BACKLOG": "Список задач",
            "SPRINTS": "Спринты",
            "DOCUMENTS": "Документы",
            "BOARD": "Доска",

            //Task master page
            "SUBTASKS": "Подзадачи",
            "COMMENTS": "Комментарии",
            "TO_BACKLOG": "К списку задач",

            //User page
            "LAST_NAME": "Фамилия",
            "ADDING_FILE_MESSAGE": "Пожалуйста, выберите файл, чтобы добавить его в свой профиль:",
            "TITLE": "Заголовок",
            "ADDITIONAL_INFO": "Дополнительная информация",
            "ALLOWED_FORMATS": "Можно добавлять фото только со следующими расширениями: jpg, png, jpeg, bmp, gif.",
            "INPUT_LAST_NAME": "Введите фамилию",
            "INPUT_FIRST_NAME": "Введите имя",
            "INPUT_PHONE": "Введите номер телефона",
            "INPUT_TITLE": "Введите заголовок",
            "INPUT_VALUE": "Введите информацию",
            "ENTER_INCORRECT_LAST_NAME": "Поле 'Фвмилия' не может быть пустым или длиннее 40 символов.",
            "ENTER_INCORRECT_FIRST_NAME": "Поле 'Имя' не может быть пустым или длиннее 40 символов.",
            "ENTER_INCORRECT_PHONE": "Поле 'Номер телефона' не может быть пустым или длиннее 20 символов.",
            "ENTER_INCORRECT_TITLE": "Поле 'Заголовок' не может быть пустым или длиннее 20 символов.",
            "ENTER_INCORRECT_VALUE": "Поле 'Информация' не может быть пустым.",
            "ADD_DETAIL": "Добавить детали",
            "ADD_DETAIL_TITLE": "Добавить Детали",
            "ADD_DOCUMENT_TITLE": "Пожалуйста, нажмите на кнопку 'Добавить', чтобы загрузить документ.",
            "ADD_DOCUMENT_PHOTO": "Пожалуйста, нажмите на кнопку 'Добавить', чтобы загрузить фото.",
            "ADD": "Добавить",

            //Master page
            "PROJECTS": "Проекты",
            "ABOUT": "О проекте",
            "ENTER_A_VALID_TIME": "Введено не корректное имя",
            "LANGUAGE": "Язык",
            "MANAGE_USERS": "Управление пользователями",
            "MANAGE_PROJECTS": "Управление проектами",
            "MANAGEMENT": "Управление",

            //Forgot password & Login & Registration
            "LOGIN": "Авторизация",
            "FORGOT_PASSWORD": "Забыл пароль",
            "ENTER_EMAIL_INVALID": "Введите почтовый ящик",
            "EMAIL": "Почтовый ящик",
            "PASSWORD": "Пароль",
            "CONFIRM_PASSWORD": "Подтвердите пароль",
            "ENTER_PASSWORD_INVALID": "Неверный пароль.",
            "REMEMBER_ME": "Запомнить меня",
            "SIGN_IN": "Войти",
            "ENTER_VALID_EMAIL": "Введите корректный почтовый ящик.",
            //Registration
            "REGISTRATION": "Регистрация",
            "REGISTER": "Зарегистрироваться",
            "PASSWORDS_MUST_MATCH": "Пароли должны совпадать.",
            "ENTER_CORRECT_PASSWORD": "Введите корректный пароль.",
            "FIRST_NAME": "Имя",
            "FIRSTNAME_IS_REQUIRED": "Имя является обязательным.",
            "SECOND_NAME": "Фамилия",
            "SECONDNAME_IS_REQUIRED": "Фамилия является обязательным.",
            "PHONE": "Телефон",
            "PHONE_INCORRECT": "Номер телефона является обязательным.",
            "CHANGE_PASSWORD": "Изменить пароль",
            "OLD_PASSWORD": "Старый пароль",

            //TaskInfo
            "TYPE": "Тип",
            "STATUS": "Статус",
            "SPRINT": "Спринт",
            "ENTER_INCORRECT_SPRINT": "Сначала введите имя спринта или выберите из списка",
            "ENTER_INCORRECT_TEAMMATE": "Сначала введите имя разработчика или выберите из списка",
            "SAVE_CHANGES": "Сохранить",
            "EDIT": "Редактировать",
            "ASSIGN_TO_ME": "Взять на себя",
            "TO_BOARD": "К доске",

            //ProjectInfo
            "DELETE": "Удалить",
            //Main Page
            "MAIN_PAGE_CARUSEL_ITEM_HEADER_1": "Добро пожаловать!",
            "MAIN_PAGE_CARUSEL_ITEM_BODY_1": "Упростите свою работу вместе с ScrumBoard",
            "MAIN_PAGE_CARUSEL_ITEM_BUTTON_1": "",

            "MAIN_PAGE_CARUSEL_ITEM_HEADER_2": "Начать",
            "MAIN_PAGE_CARUSEL_ITEM_BODY_2": "Зарегестрируйтесь, чтобы начать роботу с ScrumBoard",
            "MAIN_PAGE_CARUSEL_ITEM_BUTTON_2": "Зарегестрироваться",

            "MAIN_PAGE_CARUSEL_ITEM_HEADER_3": "Про нас",
            "MAIN_PAGE_CARUSEL_ITEM_BODY_3": "Узнайте больше информации о команде ScrumBoard",
            "MAIN_PAGE_CARUSEL_ITEM_BUTTON_3": "Узнать больше",

            "LOGOUT": "Выйти"
        });
    }
]);