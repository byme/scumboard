﻿var scrumBoardApp = angular.module("scrumBoardApp", ["pascalprecht.translate", "ngRoute", "ui.bootstrap",
    "LocalStorageModule", "angularFileUpload", "dndLists", "ngCookies", "chieffancypants.loadingBar", "ngAnimate"]);
var $$ServiceUrl = 'http://localhost:54935/api';
var $$ServiceUrlNoApi = 'http://localhost:54935/';

scrumBoardApp
    .factory("errorHandling", [
        '$q', "$location", function($q, $location) {
            var errorHandling = {
                responseError: function(response) {
                    if (response.status == 401) {
                        $location.path("/Login");
                        return $q.reject(response);
                    }

                    if (response.status == 400) {
                        $location.path("/404");
                        return $q.reject(response);
                    }
                    if (response.status >= 400 && response.status <= 500) {
                        $location.path("/404");
                    }
                    return $q.reject(response);
                }
            };
            return errorHandling;
        }
    ])
    .config(function($routeProvider, $compileProvider, $httpProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "Content/Views/Index.html",
                controller: "IndexController"
            })
            .when("/About", {
                templateUrl: "Content/Views/About.html",
                controller: "AboutController"
            })
            .when("/Project/:projectId/Sprint/:sprintId/Board", {
                templateUrl: "Content/Views/Board.html",
                controller: "boardController"
            })
            .when("/Project/:projectId/Board", {
                templateUrl: "Content/Views/Board.html",
                controller: "boardController"
            })
            .when("/ManageUsers", {
                templateUrl: "Content/Views/AdminUser.html",
                controller: "adminUserController"
            })
            .when("/Project/Create", {
                templateUrl: "Content/Views/CreateProject.html",
                controller: "CreateProjectController"
            })
            .when("/Project/:projectId/Sprint/Create", {
                templateUrl: "Content/Views/CreateSprint.html",
                controller: "createSprintController"

            })
            .when("/ProjectList", {
                templateUrl: "Content/Views/ProjectList.html",
                controller: "projectListController"
            })
            .when("/Registration", {
                templateUrl: "Content/Views/Registration.html",
                controller: "RegistrationController"
            })
            .when("/Login", {
                templateUrl: "Content/Views/Login.html",
                controller: "LoginController"
            })
            .when("/Login/ForgotPassword", {
                templateUrl: "Content/Views/ForgotPassword.html",
                controller: "forgotPasswordController"
            })
            .when("/User/ChangePassword", {
                templateUrl: "Content/Views/ChangePassword.html",
                controller: "сhangePasswordController"
            })
            .when("/Task/Create/:projectId/:parentId", {
                templateUrl: "Content/Views/CreateTask.html",
                controller: "createTaskController"
            })
            .when("/Project/:projectId/Task/Create", {
                templateUrl: "Content/Views/CreateTask.html",
                controller: "createTaskController"
            })
            .when("/Project/:projectId/:pageName", {
                templateUrl: "Content/Views/Project.html",
                controller: "projectController"
            })
            .when("/Project/:projectId/Task/:taskId/:pageName", {
                templateUrl: "Content/Views/Task.html",
                controller: "taskController"
            })
            .when("/404", {
                templateUrl: "Content/Views/404.html"
            })
            .when("/User/ChangePassword", {
                templateUrl: "Content/Views/ChangePassword.html",
                controller: "сhangePasswordController"
            })
            .when("/User/:userId", {
                templateUrl: "Content/Views/User.html",
                controller: "userController"
            })
            ;
        $routeProvider.otherwise({ redirectTo: "/404" });

        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);

        $httpProvider.interceptors.push("errorHandling");
    })
    .run(function($rootScope, $location) {
        $rootScope.toBoard = function(projectId, sprintId) {
            if (sprintId != null) {
                $location.path("/Project/" + projectId + "/Sprint/" + sprintId + "/Board");
            }
            else {
                $location.path("/Project/" + projectId + "/Board");
            };
        }
    }).
    run(function($rootScope, $location, authService,userService) {
        $rootScope.$on("$routeChangeStart", function(event, next, current) {
            authService.fillAuthData();
            var user = userService.currentUser();
            if (next.originalPath == "/Registration" || next.originalPath == "/About" || next.originalPath == "/Login/ForgotPassword" || next.originalPath == "/" || next.originalPath == "/Login") {
                return;
            }
            if (!$rootScope.isAuth) {
                $location.path("/");
                return;
            }
            if (next.originalPath == "/ManageUsers" && !user.IsAdmin) {
                $location.path("/404");
                return;
            }
        });
    });