﻿using System.Web.Mvc;

namespace ScrumBoard.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}